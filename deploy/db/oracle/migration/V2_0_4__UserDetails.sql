-----------------------------------------------------------------------------------
-- *******************************************************************************
-- * Copyright (c) 2022 Contributors to the Eclipse Foundation
-- *
-- * See the NOTICE file(s) distributed with this work for additional
-- * information regarding copyright ownership.
-- *
-- * This program and the accompanying materials are made available under the
-- * terms of the Eclipse Public License v. 2.0 which is available at
-- * http://www.eclipse.org/legal/epl-2.0.
-- *
-- * SPDX-License-Identifier: EPL-2.0
-- *******************************************************************************
-----------------------------------------------------------------------------------

-- add phone, fax and initials to table user

ALTER TABLE tbl_user ADD phone character varying(255);
ALTER TABLE tbl_user ADD fax character varying(255);
ALTER TABLE tbl_user ADD initials character varying(255);

