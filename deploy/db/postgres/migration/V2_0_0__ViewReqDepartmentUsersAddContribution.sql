-----------------------------------------------------------------------------------
-- *******************************************************************************
-- * Copyright (c) 2019 Contributors to the Eclipse Foundation
-- *
-- * See the NOTICE file(s) distributed with this work for additional
-- * information regarding copyright ownership.
-- *
-- * This program and the accompanying materials are made available under the
-- * terms of the Eclipse Public License v. 2.0 which is available at
-- * http://www.eclipse.org/legal/epl-2.0.
-- *
-- * SPDX-License-Identifier: EPL-2.0
-- *******************************************************************************
-----------------------------------------------------------------------------------

CREATE OR REPLACE VIEW public.vw_statement_reqdepartment_users
AS SELECT row_number() OVER (ORDER BY u.id) AS id,
    u.id AS user_id,
    u.first_name,
    u.last_name,
    u.username AS user_name,
    u.email_address,
    wd.statement_id,
    wd.id AS workflow_id,
    u.department_id,
    d.name AS department_name,
    d.departmentgroup AS department_group,
    rd.contributed AS department_contributed
   FROM tbl_user u
     JOIN tbl_department d ON u.department_id = d.id
     JOIN tbl_reqdepartment rd ON rd.department_id = d.id
     JOIN tbl_workflowdata wd ON wd.id = rd.workflowdata_id;
