-----------------------------------------------------------------------------------
-- *******************************************************************************
-- * Copyright (c) 2022 Contributors to the Eclipse Foundation
-- *
-- * See the NOTICE file(s) distributed with this work for additional
-- * information regarding copyright ownership.
-- *
-- * This program and the accompanying materials are made available under the
-- * terms of the Eclipse Public License v. 2.0 which is available at
-- * http://www.eclipse.org/legal/epl-2.0.
-- *
-- * SPDX-License-Identifier: EPL-2.0
-- *******************************************************************************
-----------------------------------------------------------------------------------


-- disable tags: add standard flag for tags not allowed to disable and disabled flag

ALTER TABLE public.tbl_tag ADD column standard boolean not null default false;
ALTER TABLE public.tbl_tag ADD column disabled boolean not null default false;

ALTER TABLE public.tbl_tag ADD CONSTRAINT tbl_tag_name_u UNIQUE (name);

UPDATE public.tbl_tag set standard = true WHERE id IN ('email-text', 'email', 'outbox', 'consideration', 'statement', 'cover-letter', 'overview', 'expertise', 'plan', 'explanatory-report');


-- disable statementtype: add disabled flag

alter table public.tbl_statementtype add column disabled boolean not null default false;
ALTER TABLE public.tbl_statementtype ADD CONSTRAINT tbl_statementtype_name_u UNIQUE (name);



CREATE OR REPLACE VIEW vw_deletable_tag AS
SELECT * from public.tbl_tag where id not in (select distinct t.id from public.tbl_attachment2tag tat join public.tbl_tag t on t.id = tat.tag_id) and standard != true and disabled = true;


CREATE OR REPLACE VIEW vw_deletable_statementtype AS
SELECT * from public.tbl_statementtype ts where id not in (select distinct st.id from public.tbl_statement s join public.tbl_statementtype st  on st.id = s.type_id) and disabled = true;


-- increment id sequence by 5 to ensure valid nextval

SELECT nextval('tbl_statementtype_id_seq');
SELECT nextval('tbl_statementtype_id_seq');
SELECT nextval('tbl_statementtype_id_seq');
SELECT nextval('tbl_statementtype_id_seq');
SELECT nextval('tbl_statementtype_id_seq');
