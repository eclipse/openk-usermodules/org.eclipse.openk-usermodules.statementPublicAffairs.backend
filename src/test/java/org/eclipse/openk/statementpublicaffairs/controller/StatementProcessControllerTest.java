/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.controller;

import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.ByteArrayInputStream;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.openk.statementpublicaffairs.StatementPublicAffairsApplication;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ConflictException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ConflictServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalServerErrorException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.service.StatementProcessService;
import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;
import org.eclipse.openk.statementpublicaffairs.viewmodel.AttachmentModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ClaimDetailsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.MailSendReport;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ProcessActivityHistoryViewModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ProcessHistoryModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementDetailsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementTaskModel;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@SpringBootTest(classes = StatementPublicAffairsApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")

class StatementProcessControllerTest {

	@MockBean
	private StatementProcessService statementProcessService;

	@Autowired
	private MockMvc mockMvc;

	@Test
	void getStatementTaskWithValidStatementIdShouldRespondWithListOfStatementTaskModel() throws Exception {
		Long statementId = 1234L;
		String assignee = "assignee";
		String processDefinitionKey = "processDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		String var1Type = "Boolean";
		requiredVariables.put("var1", var1Type);

		String taskDefinitionKey = "taskDefinitionKey";
		String taskId = "taskId";
		List<StatementTaskModel> statementTasks = new ArrayList<>();

		StatementTaskModel stm = new StatementTaskModel();

		stm.setAssignee(assignee);
		stm.setProcessDefinitionKey(processDefinitionKey);
		stm.setRequiredVariables(requiredVariables);
		stm.setStatementId(statementId);
		stm.setTaskDefinitionKey(taskDefinitionKey);
		stm.setTaskId(taskId);
		statementTasks.add(stm);

		Mockito.when(statementProcessService.getCurrentStatementTasks(statementId))
				.thenReturn(Optional.of(statementTasks));

		mockMvc.perform(get("/process/statements/" + statementId + "/task")).andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("[0].statementId", is(statementId.intValue())))
				.andExpect(jsonPath("[0].assignee", is(assignee)))
				.andExpect(jsonPath("[0].processDefinitionKey", is(processDefinitionKey)))
				.andExpect(jsonPath("[0].taskDefinitionKey", is(taskDefinitionKey)))
				.andExpect(jsonPath("[0].taskId", is(taskId)))
				.andExpect(jsonPath("[0].requiredVariables.var1", is(var1Type)));
	}

	@Test
	void getStatementTaskWithInvalidStatementIdShouldRespondWithNotFound() throws Exception {

		Long statementId = 404L;

		Mockito.when(statementProcessService.getCurrentStatementTasks(statementId)).thenReturn(Optional.empty());

		mockMvc.perform(get("/process/statements/" + statementId + "/task")).andExpect(status().is(404));

	}

	@Test
	void getStatementTaskWithValidStatementIdForbiddenShouldRespondWithForbidden() throws Exception {

		Long statementId = 1234L;

		Mockito.when(statementProcessService.getCurrentStatementTasks(statementId))
				.thenThrow(new ForbiddenServiceException());

		mockMvc.perform(get("/process/statements/" + statementId + "/task")).andExpect(status().is(403));

	}

	@Test
	void getStatementTaskWithValidStatementIdInternalErrorInStatementProcessServiceShouldRespondWithInternalServerError()
			throws Exception {

		Long statementId = 1234L;

		Mockito.when(statementProcessService.getCurrentStatementTasks(statementId))
				.thenThrow(new InternalErrorServiceException());

		mockMvc.perform(get("/process/statements/" + statementId + "/task")).andExpect(status().is(500));
	}

	@Test
	void getStatementProcessHistoryWithValidStatementIdShouldRespondWithProcessHistoryModel() throws Exception {

		Long statementId = 1234L;

		String processName = "processName";
		Long processVersion = 10L;

		List<ProcessActivityHistoryViewModel> currentProcessActivities = new ArrayList<>();

		ProcessActivityHistoryViewModel currentActivity = new ProcessActivityHistoryViewModel();

		String cAId = "currentActivityId";
		currentActivity.setActivityId(cAId);
		String activityName = "activityName";
		currentActivity.setActivityName(activityName);
		String activityType = "activityType";
		currentActivity.setActivityType(activityType);
		String assignee = "assignee";
		currentActivity.setAssignee(assignee);
		Boolean canceled = true;
		currentActivity.setCanceled(canceled);
		Boolean completeScope = true;
		currentActivity.setCompleteScope(completeScope);
		Long durationInMillis = 1000L;
		currentActivity.setDurationInMillis(durationInMillis);
		String endTime = TypeConversion.camundaDateStringOfZDT(ZonedDateTime.now()).get();
		currentActivity.setEndTime(endTime);
		String id = "id";
		currentActivity.setId(id);
		String processDefinitionId = "processDefinitionId";
		currentActivity.setProcessDefinitionId(processDefinitionId);
		String processDefinitionKey = "processDefinitionKey";
		currentActivity.setProcessDefinitionKey(processDefinitionKey);
		String startTime = TypeConversion.camundaDateStringOfZDT(ZonedDateTime.now()).get();
		currentActivity.setStartTime(startTime);
		String tenantId = "tenantId";
		currentActivity.setTenantId(tenantId);

		currentProcessActivities.add(currentActivity);

		List<ProcessActivityHistoryViewModel> finishedProcessActivities = new ArrayList<>();

		ProcessActivityHistoryViewModel finishedActivity = new ProcessActivityHistoryViewModel();

		String fAId = "finishedActivityId";
		finishedActivity.setActivityId(fAId);
		finishedActivity.setActivityName(activityName);
		finishedActivity.setActivityType(activityType);
		finishedActivity.setAssignee(assignee);
		finishedActivity.setCanceled(canceled);
		finishedActivity.setCompleteScope(completeScope);
		finishedActivity.setDurationInMillis(durationInMillis);
		finishedActivity.setEndTime(endTime);
		finishedActivity.setId(id);
		finishedActivity.setProcessDefinitionId(processDefinitionId);
		finishedActivity.setProcessDefinitionKey(processDefinitionKey);
		finishedActivity.setStartTime(startTime);
		finishedActivity.setTenantId(tenantId);

		finishedProcessActivities.add(finishedActivity);

		ProcessHistoryModel processHistoryModel = new ProcessHistoryModel();

		processHistoryModel.setCurrentProcessActivities(currentProcessActivities);
		processHistoryModel.setFinishedProcessActivities(finishedProcessActivities);
		processHistoryModel.setProcessName(processName);
		processHistoryModel.setProcessVersion(processVersion);

		Mockito.when(statementProcessService.getStatmentProcessHistory(statementId))
				.thenReturn(Optional.of(processHistoryModel));

		mockMvc.perform(get("/process/statements/" + statementId + "/history")).andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("processName", is(processName)))
				.andExpect(jsonPath("processVersion", is(processVersion.intValue())))

				.andExpect(jsonPath("currentProcessActivities.[0].activityId", is(cAId)))
				.andExpect(jsonPath("currentProcessActivities.[0].activityName", is(activityName)))
				.andExpect(jsonPath("currentProcessActivities.[0].activityType", is(activityType)))
				.andExpect(jsonPath("currentProcessActivities.[0].assignee", is(assignee)))
				.andExpect(jsonPath("currentProcessActivities.[0].canceled", is(canceled)))
				.andExpect(jsonPath("currentProcessActivities.[0].completeScope", is(completeScope)))
				.andExpect(jsonPath("currentProcessActivities.[0].durationInMillis", is(durationInMillis.intValue())))
				.andExpect(jsonPath("currentProcessActivities.[0].endTime", is(endTime)))
				.andExpect(jsonPath("currentProcessActivities.[0].id", is(id)))
				.andExpect(jsonPath("currentProcessActivities.[0].processDefinitionId", is(processDefinitionId)))
				.andExpect(jsonPath("currentProcessActivities.[0].processDefinitionKey", is(processDefinitionKey)))
				.andExpect(jsonPath("currentProcessActivities.[0].startTime", is(startTime)))
				.andExpect(jsonPath("currentProcessActivities.[0].tenantId", is(tenantId)))

				.andExpect(jsonPath("finishedProcessActivities.[0].activityId", is(fAId)))
				.andExpect(jsonPath("finishedProcessActivities.[0].activityName", is(activityName)))
				.andExpect(jsonPath("finishedProcessActivities.[0].activityType", is(activityType)))
				.andExpect(jsonPath("finishedProcessActivities.[0].assignee", is(assignee)))
				.andExpect(jsonPath("finishedProcessActivities.[0].canceled", is(canceled)))
				.andExpect(jsonPath("finishedProcessActivities.[0].completeScope", is(completeScope)))
				.andExpect(jsonPath("finishedProcessActivities.[0].durationInMillis", is(durationInMillis.intValue())))
				.andExpect(jsonPath("finishedProcessActivities.[0].endTime", is(endTime)))
				.andExpect(jsonPath("finishedProcessActivities.[0].id", is(id)))
				.andExpect(jsonPath("finishedProcessActivities.[0].processDefinitionId", is(processDefinitionId)))
				.andExpect(jsonPath("finishedProcessActivities.[0].processDefinitionKey", is(processDefinitionKey)))
				.andExpect(jsonPath("finishedProcessActivities.[0].startTime", is(startTime)))
				.andExpect(jsonPath("finishedProcessActivities.[0].tenantId", is(tenantId)));
	}

	@Test
	void getStatementProcessHistoryWithInvalidStatementIdShouldRespondWithNotFound() throws Exception {

		Long statementId = 404L;

		Mockito.when(statementProcessService.getStatmentProcessHistory(statementId)).thenReturn(Optional.empty());

		mockMvc.perform(get("/process/statements/" + statementId + "/history")).andExpect(status().is(404));
	}

	@Test
	void getStatementProcessHistoryWithValidStatementIdForbiddenShouldRespondWithForbidden() throws Exception {

		Long statementId = 1234L;

		Mockito.when(statementProcessService.getStatmentProcessHistory(statementId))
				.thenThrow(new ForbiddenServiceException());

		mockMvc.perform(get("/process/statements/" + statementId + "/history")).andExpect(status().is(403));

	}

	@Test
	void getStatementProcessHistoryWithValidStatementIdInternalErrorInStatementProcessServiceShouldRespondWithInternalServerError()
			throws Exception {

		Long statementId = 1234L;

		Mockito.when(statementProcessService.getStatmentProcessHistory(statementId))
				.thenThrow(new InternalErrorServiceException());

		mockMvc.perform(get("/process/statements/" + statementId + "/history")).andExpect(status().is(500));

	}

	@Test
	void claimStatementTaskWithValidStatementIdAndValidTaskIdAndUnclaimedShouldRespondWithStatementTaskModel()
			throws Exception {

		Long statementId = 1234L;
		String taskId = "taskId";

		String assignee = "assignee";
		String processDefinitionKey = "processDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		String var1Type = "Boolean";
		requiredVariables.put("var1", var1Type);

		String taskDefinitionKey = "taskDefinitionKey";

		StatementTaskModel stm = new StatementTaskModel();

		stm.setAssignee(assignee);
		stm.setProcessDefinitionKey(processDefinitionKey);
		stm.setRequiredVariables(requiredVariables);
		stm.setStatementId(statementId);
		stm.setTaskDefinitionKey(taskDefinitionKey);
		stm.setTaskId(taskId);

		Mockito.when(statementProcessService.claimStatementTask(statementId, taskId)).thenReturn(Optional.of(stm));

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/claim").with(csrf()))
				.andExpect(status().is2xxSuccessful()).andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("statementId", is(statementId.intValue())))
				.andExpect(jsonPath("assignee", is(assignee)))
				.andExpect(jsonPath("processDefinitionKey", is(processDefinitionKey)))
				.andExpect(jsonPath("taskDefinitionKey", is(taskDefinitionKey)))
				.andExpect(jsonPath("taskId", is(taskId))).andExpect(jsonPath("requiredVariables.var1", is(var1Type)));

	}

	@Test
	void claimStatementTaskWithInvalidStatementIdOrInvalidTaskIdOrUnclaimedShouldRespondWithNotFound()
			throws Exception {

		Long statementId = 404L;
		String taskId = "taskId";

		Mockito.when(statementProcessService.claimStatementTask(statementId, taskId)).thenReturn(Optional.empty());

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/claim").with(csrf()))
				.andExpect(status().is(404));
	}

	@Test
	void claimStatementTaskWithValidStatementIdAndValidTaskIdAndUnclaimedForbiddenShouldRespondWithForbidden()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";

		Mockito.when(statementProcessService.claimStatementTask(statementId, taskId))
				.thenThrow(new ForbiddenServiceException());

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/claim"))
				.andExpect(status().is(403));
	}

	@Test
	void claimStatementTaskWithValidStatementIdAndValidTaskIdAndUnclaimedInternalErrorInStatementProcessServiceShouldRespondWithInternalServerError()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";

		Mockito.when(statementProcessService.claimStatementTask(statementId, taskId))
				.thenThrow(new InternalServerErrorException());

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/claim").with(csrf()))
				.andExpect(status().is(500));
	}

	@Test
	void claimStatementTaskWithMalformedStatementIdorMalformedTaskIdAndUnclaimedShouldRespondWithBadRequest()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";

		Mockito.when(statementProcessService.claimStatementTask(statementId, taskId))
				.thenThrow(new BadRequestServiceException());

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/claim").with(csrf()))
				.andExpect(status().is(400));
	}

	@Test
	void unClaimStatementTaskWithValidStatementIdAndValidTaskIdAndClaimedShouldRespondWithStatementModel()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";

		String processDefinitionKey = "processDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		String var1Type = "Boolean";
		requiredVariables.put("var1", var1Type);

		String taskDefinitionKey = "taskDefinitionKey";

		StatementTaskModel stm = new StatementTaskModel();

		stm.setProcessDefinitionKey(processDefinitionKey);
		stm.setRequiredVariables(requiredVariables);
		stm.setStatementId(statementId);
		stm.setTaskDefinitionKey(taskDefinitionKey);
		stm.setTaskId(taskId);

		Mockito.when(statementProcessService.unClaimStatementTask(statementId, taskId)).thenReturn(Optional.of(stm));

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/unclaim").with(csrf()))
				.andExpect(status().is2xxSuccessful()).andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("statementId", is(statementId.intValue())))
				.andExpect(jsonPath("processDefinitionKey", is(processDefinitionKey)))
				.andExpect(jsonPath("taskDefinitionKey", is(taskDefinitionKey)))
				.andExpect(jsonPath("taskId", is(taskId))).andExpect(jsonPath("requiredVariables.var1", is(var1Type)));

	}

	@Test
	void unClaimStatementTaskWithInvalidStatementIdOrInvalidTaskIdOrClaimedShouldRespondWithNotFound()
			throws Exception {

		Long statementId = 404L;
		String taskId = "taskId";

		Mockito.when(statementProcessService.unClaimStatementTask(statementId, taskId)).thenReturn(Optional.empty());

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/unclaim").with(csrf()))
				.andExpect(status().is(404));
	}

	@Test
	void unClaimStatementTaskWithValidStatementIdAndValidTaskIdAndClaimedForbiddenShouldRespondWithForbidden()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";

		Mockito.when(statementProcessService.unClaimStatementTask(statementId, taskId))
				.thenThrow(new ForbiddenServiceException());

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/unclaim"))
				.andExpect(status().is(403));
	}

	@Test
	void unClaimStatementTaskWithValidStatementIdAndValidTaskIdAndClaimedInternalErrorInStatementProcessServiceShouldRespondWithInternalServerError()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";

		Mockito.when(statementProcessService.unClaimStatementTask(statementId, taskId))
				.thenThrow(new InternalServerErrorException());

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/unclaim").with(csrf()))
				.andExpect(status().is(500));
	}

	@Test
	void unClaimStatementTaskWithMalformedStatementIdorMalformedTaskIdAndClaimedShouldRespondWithBadRequest()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";

		Mockito.doThrow(new BadRequestServiceException()).when(statementProcessService)
				.unClaimStatementTask(statementId, taskId);
		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/unclaim").with(csrf()))
				.andExpect(status().is(400));
	}

	@Test
	void completeStatementTaskWithValidStatementIdAndValidTaskIdAndValidVariablesShouldRespondWithNoContent()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";

		Mockito.when(statementProcessService.completeStatementTask(Mockito.eq(statementId), Mockito.eq(taskId),
				Mockito.anyMap())).thenReturn(Optional.of(true));

		String variablesJson = "{\"var1\": { \"type\": \"Boolean\", \"value\":true}}";

		mockMvc.perform(
				post("/process/statements/" + statementId + "/task/" + taskId + "/complete").content(variablesJson)
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).with(csrf()))
				.andExpect(status().is2xxSuccessful());

	}

	@Test
	void completeStatementTaskWithInvalidStatementIdAndInvalidTaskIdShouldRespondWithNotFound() throws Exception {

		Long statementId = 404L;
		String taskId = "taskId";

		Mockito.when(statementProcessService.completeStatementTask(Mockito.eq(statementId), Mockito.eq(taskId),
				Mockito.anyMap())).thenReturn(Optional.empty());

		String variablesJson = "{\"var1\": { \"type\": \"Boolean\", \"value\":true}}";

		mockMvc.perform(
				post("/process/statements/" + statementId + "/task/" + taskId + "/complete").content(variablesJson)
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).with(csrf()))
				.andExpect(status().is(404));

	}

	@Test
	void completeStatementTaskWithMalfomedStatementIdOrMalformedTaskIdOrInvalidVariablesShouldRespondWithBadRequest()
			throws Exception {

		Long statementId = 1234L;
		String taskId = "taskId";

		Mockito.when(statementProcessService.completeStatementTask(Mockito.eq(statementId), Mockito.eq(taskId),
				Mockito.anyMap())).thenThrow(new BadRequestServiceException());

		String variablesJson = "{\"var1\": { \"type\": \"Boolean\", \"value\":true}}";

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/complete").with(csrf())
				.content(variablesJson).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400));
	}

	@Test
	void completeStatementTaskWithValidStatementIdAndValidTaskIdAndValidVariablesShouldRespondWithInternalServerError()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";

		Mockito.when(statementProcessService.completeStatementTask(Mockito.eq(statementId), Mockito.eq(taskId),
				Mockito.anyMap())).thenThrow(new InternalErrorServiceException());

		String variablesJson = "{\"var1\": { \"type\": \"Boolean\", \"value\":true}}";

		mockMvc.perform(
				post("/process/statements/" + statementId + "/task/" + taskId + "/complete").content(variablesJson)
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).with(csrf()))
				.andExpect(status().is(500));

	}

	@Test
	void getStatementWorkflowWithValidStatementIdShouldRespondWithDefinitionXML() throws Exception {

		Long statementId = 1234L;
		String xmlValue = "xmlValue";
		Mockito.when(statementProcessService.getStatementWorkflow(statementId)).thenReturn(Optional.of(xmlValue));

		mockMvc.perform(get("/process/statements/" + statementId + "/workflowmodel"))
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_XML))
				.andExpect(content().string(xmlValue));

	}

	@Test
	void getStatementWorkflowWithInvalidStatementIdShouldRespondWithNotFound() throws Exception {

		Long statementId = 404L;
		Mockito.when(statementProcessService.getStatementWorkflow(statementId)).thenReturn(Optional.empty());

		mockMvc.perform(get("/process/statements/" + statementId + "/workflowmodel")).andExpect(status().isNotFound());

	}

	@Test
	void getStatementWorkflowWithVaalidStatementIdExceptionAccessingWorkflowEngineShouldRespondWithInternalServerError()
			throws Exception {

		Long statementId = 1234L;

		Mockito.doThrow(new InternalErrorServiceException()).when(statementProcessService)
				.getStatementWorkflow(statementId);
		mockMvc.perform(get("/process/statements/" + statementId + "/workflowmodel"))
				.andExpect(status().isInternalServerError());

	}

	@Test
	void updateStatementWithValidStatementIdTaskIdStatementModelShouldRespondWithStatementModel() throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		String dueDate = "2020-04-30";
		String departmentsDueDate = "2022-01-04";
		String receiptDate = "2020-04-10";
		String title = "Title Text.";
		String city = "City";
		String district = "District";
		Long statementTypeId = 1L;
		String newstatementjson = "{\"dueDate\":\"" + dueDate + "\", \"departmentsDueDate\":\"" + departmentsDueDate
				+ "\", \"receiptDate\":\"" + receiptDate + "\", \"title\":\"" + title + "\", \"city\":\"" + city
				+ "\" , \"district\":\"" + district + "\" , \"typeId\":\"" + statementTypeId + "\" }";

		Mockito.when(statementProcessService.updateStatement(Mockito.eq(statementId), Mockito.eq(taskId),
				Mockito.any(StatementDetailsModel.class))).thenAnswer(new Answer<Optional<StatementDetailsModel>>() {
					@Override
					public Optional<StatementDetailsModel> answer(InvocationOnMock invocation) throws Throwable {
						Object[] args = invocation.getArguments();
						if (args[2] instanceof StatementDetailsModel model) {
							if (dueDate.equals(model.getDueDate())
									&& departmentsDueDate.equals(model.getDepartmentsDueDate())
									&& title.equals(model.getTitle()) && statementTypeId.equals(model.getTypeId())
									&& district.equals(model.getDistrict())
									&& receiptDate.equals(model.getReceiptDate()) && city.equals(model.getCity())) {
								model.setId(1234L);
								model.setFinished(false);
								return Optional.of(model);
							}
						}
						return Optional.empty();
					}
				});

		mockMvc.perform(
				post("/process/statements/" + statementId + "/task/" + taskId + "/statement").content(newstatementjson)
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).with(csrf()))
				.andExpect(status().is(200)).andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("id", is(1234))).andExpect(jsonPath("finished", is(false)))
				.andExpect(jsonPath("dueDate", is(dueDate)))
				.andExpect(jsonPath("departmentsDueDate", is(departmentsDueDate)))
				.andExpect(jsonPath("receiptDate", is(receiptDate))).andExpect(jsonPath("city", is(city)))
				.andExpect(jsonPath("district", is(district))).andExpect(jsonPath("typeId", is(1)))
				.andExpect(jsonPath("title", is(title)));
	}

	@Test
	void updateStatementWithValidStatemenTdTaskIdUpdateResponseEmptyShouldRespondWithInternalServerError()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		String dueDate = "2020-04-30";
		String receiptDate = "2020-04-10";
		String title = "Title Text.";
		String city = "City";
		String district = "District";
		Long statementTypeId = 1L;
		String newstatementjson = "{\"dueDate\":\"" + dueDate + "\", \"receiptDate\":\"" + receiptDate
				+ "\", \"title\":\"" + title + "\", \"city\":\"" + city + "\" , \"district\":\"" + district
				+ "\" , \"typeId\":\"" + statementTypeId + "\" }";

		Mockito.when(statementProcessService.updateStatement(Mockito.eq(statementId), Mockito.eq(taskId),
				Mockito.any(StatementDetailsModel.class))).thenReturn(Optional.empty());

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/statement").with(csrf())
				.content(newstatementjson).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(500));

	}

	@Test
	void updateStatementWithValidStatemenTdTaskIdBadRequestServiceExceptionShouldRespondWithBadRequest()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		String dueDate = "2020-04-30";
		String receiptDate = "2020-04-10";
		String title = "Title Text.";
		String city = "City";
		String district = "District";
		Long statementTypeId = 1L;
		String newstatementjson = "{\"dueDate\":\"" + dueDate + "\", \"receiptDate\":\"" + receiptDate
				+ "\", \"title\":\"" + title + "\", \"city\":\"" + city + "\" , \"district\":\"" + district
				+ "\" , \"typeId\":\"" + statementTypeId + "\" }";

		Mockito.when(statementProcessService.updateStatement(Mockito.eq(statementId), Mockito.eq(taskId),
				Mockito.any(StatementDetailsModel.class))).thenThrow(new BadRequestServiceException());

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/statement").with(csrf())
				.content(newstatementjson).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400));
	}

	@Test
	void updateStatementWithValidStatemenTdTaskIdInternalErrorServiceExceptionShouldRespondWithInternalServerError()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		String dueDate = "2020-04-30";
		String receiptDate = "2020-04-10";
		String title = "Title Text.";
		String city = "City";
		String district = "District";
		Long statementTypeId = 1L;
		String newstatementjson = "{\"dueDate\":\"" + dueDate + "\", \"receiptDate\":\"" + receiptDate
				+ "\", \"title\":\"" + title + "\", \"city\":\"" + city + "\" , \"district\":\"" + district
				+ "\" , \"typeId\":\"" + statementTypeId + "\" }";

		Mockito.when(statementProcessService.updateStatement(Mockito.eq(statementId), Mockito.eq(taskId),
				Mockito.any(StatementDetailsModel.class))).thenThrow(new InternalErrorServiceException());

		mockMvc.perform(
				post("/process/statements/" + statementId + "/task/" + taskId + "/statement").content(newstatementjson)
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).with(csrf()))
				.andExpect(status().is(500));
	}

	@Test
	void updateStatementWithValidStatemenTdTaskIdForbiddenServiceExceptionShouldRespondWithForbidden()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		String dueDate = "2020-04-30";
		String receiptDate = "2020-04-10";
		String title = "Title Text.";
		String city = "City";
		String district = "District";
		Long statementTypeId = 1L;
		String newstatementjson = "{\"dueDate\":\"" + dueDate + "\", \"receiptDate\":\"" + receiptDate
				+ "\", \"title\":\"" + title + "\", \"city\":\"" + city + "\" , \"district\":\"" + district
				+ "\" , \"typeId\":\"" + statementTypeId + "\" }";

		Mockito.when(statementProcessService.updateStatement(Mockito.eq(statementId), Mockito.eq(taskId),
				Mockito.any(StatementDetailsModel.class))).thenThrow(new ForbiddenServiceException());

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/statement")
				.content(newstatementjson).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(403));
	}

	@Test
	void updateStatementWithValidStatemenTdTaskIdNotFoundServiceExceptionShouldRespondWithNotFound() throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		String dueDate = "2020-04-30";
		String receiptDate = "2020-04-10";
		String title = "Title Text.";
		String city = "City";
		String district = "District";
		Long statementTypeId = 1L;
		String newstatementjson = "{\"dueDate\":\"" + dueDate + "\", \"receiptDate\":\"" + receiptDate
				+ "\", \"title\":\"" + title + "\", \"city\":\"" + city + "\" , \"district\":\"" + district
				+ "\" , \"typeId\":\"" + statementTypeId + "\" }";

		Mockito.when(statementProcessService.updateStatement(Mockito.eq(statementId), Mockito.eq(taskId),
				Mockito.any(StatementDetailsModel.class))).thenThrow(new NotFoundServiceException());

		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/statement").with(csrf())
				.content(newstatementjson).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(404));
	}

	@Test
	void setTagsWithValidStatementIdTaskIdAttachmentIdAndTaglistShouldRespondWithOkNoContent() throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		Long attachmentId = 23L;

		String tagIdList = "[1,2,3]";
		mockMvc.perform(post(
				"/process/statements/" + statementId + "/task/" + taskId + "/attachments/" + attachmentId + "/tags")
						.content(tagIdList).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
						.with(csrf()))
				.andExpect(status().is(200));
	}

	@Test
	void setTagsWithValidStatementIdTaskIdAttachmentIdAndBadRequestServiceExceptionShouldRespondBadRequest()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		Long attachmentId = 23L;

		String tagIdList = "[1,2,3]";
		Mockito.doThrow(new BadRequestServiceException()).when(statementProcessService).setAttachmentTags(
				Mockito.eq(statementId), Mockito.eq(taskId), Mockito.eq(attachmentId), Mockito.anySet());

		mockMvc.perform(post(
				"/process/statements/" + statementId + "/task/" + taskId + "/attachments/" + attachmentId + "/tags")
						.with(csrf()).content(tagIdList).contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400));
	}

	@Test
	void setTagsWithValidStatementIdTaskIdAttachmentIdAndInternalErrorServiceExceptionShouldRespondWithInternalServerError()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		Long attachmentId = 23L;

		String tagIdList = "[1,2,3]";
		Mockito.doThrow(new InternalErrorServiceException()).when(statementProcessService).setAttachmentTags(
				Mockito.eq(statementId), Mockito.eq(taskId), Mockito.eq(attachmentId), Mockito.anySet());

		mockMvc.perform(post(
				"/process/statements/" + statementId + "/task/" + taskId + "/attachments/" + attachmentId + "/tags")
						.with(csrf()).content(tagIdList).contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(500));
	}

	@Test
	void setTagsWithValidStatementIdTaskIdAttachmentIdAndForbiddenServiceExceptionShouldRespondWithForbidden()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		Long attachmentId = 23L;

		String tagIdList = "[1,2,3]";
		Mockito.doThrow(new ForbiddenServiceException()).when(statementProcessService).setAttachmentTags(
				Mockito.eq(statementId), Mockito.eq(taskId), Mockito.eq(attachmentId), Mockito.anySet());

		mockMvc.perform(post(
				"/process/statements/" + statementId + "/task/" + taskId + "/attachments/" + attachmentId + "/tags")
						.content(tagIdList).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(403));
	}

	@Test
	void setTagsWithInvalidStatementIdTaskIdAttachmentIdShouldRespondWithNotFound() throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		Long attachmentId = 23L;

		String tagIdList = "[1,2,3]";
		Mockito.doThrow(new NotFoundServiceException()).when(statementProcessService).setAttachmentTags(
				Mockito.eq(statementId), Mockito.eq(taskId), Mockito.eq(attachmentId), Mockito.anySet());

		mockMvc.perform(post(
				"/process/statements/" + statementId + "/task/" + taskId + "/attachments/" + attachmentId + "/tags")
						.with(csrf()).content(tagIdList).contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(404));

	}

	@Test
	void deleteAttachmentWithValidStatementIdTaskIdAttachmentIdShouldRespondOkNoContent() throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		Long attachmentId = 23L;

		mockMvc.perform(
				delete("/process/statements/" + statementId + "/task/" + taskId + "/attachments/" + attachmentId)
						.with(csrf()))
				.andExpect(status().is(204));
	}

	@Test
	void deleteAttachmentWithInvalidStatementIdTaskIdAttachmentIdShouldRespondNotFound() throws Exception {

		Long statementId = 404L;
		String taskId = "taskId";
		Long attachmentId = 23L;

		Mockito.doThrow(new NotFoundServiceException()).when(statementProcessService)
				.deleteStatementAttachments(statementId, taskId, attachmentId);

		mockMvc.perform(
				delete("/process/statements/" + statementId + "/task/" + taskId + "/attachments/" + attachmentId)
						.with(csrf()))
				.andExpect(status().is(404));

	}

	@Test
	void deleteAttachmentWithValidStatementIdTaskIdAttachmentIdForbiddenServiceExceptionShouldRespondForbidden()
			throws Exception {

		Long statementId = 1234L;
		String taskId = "taskId";
		Long attachmentId = 23L;

		Mockito.doThrow(new ForbiddenServiceException()).when(statementProcessService)
				.deleteStatementAttachments(statementId, taskId, attachmentId);

		mockMvc.perform(
				delete("/process/statements/" + statementId + "/task/" + taskId + "/attachments/" + attachmentId))
				.andExpect(status().is(403));

	}

	@Test
	void deleteAttachmentWithValidStatementIdTaskIdAttachmentIdInternalErrorServiceExceptionShouldRespondInternalServerError()
			throws Exception {

		Long statementId = 1234L;
		String taskId = "taskId";
		Long attachmentId = 23L;

		Mockito.doThrow(new InternalErrorServiceException()).when(statementProcessService)
				.deleteStatementAttachments(statementId, taskId, attachmentId);

		mockMvc.perform(
				delete("/process/statements/" + statementId + "/task/" + taskId + "/attachments/" + attachmentId)
						.with(csrf()))
				.andExpect(status().is(500));

	}

	@Test
	void statementAddAttachmentForStatementWithValidStatementIdTaskIdShouldRespondWithAttachmentModelContainingTheAttachmentId()
			throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		Long attachmentId = 34L;
		AttachmentModel model = new AttachmentModel();
		model.setId(attachmentId);
		model.setName("file.txt");
		model.setType("text/plain");
		byte[] rawFile = "this is some test data.".getBytes();
		Mockito.when(statementProcessService.createStatementAttachmentToModel(Mockito.eq(statementId),
				Mockito.eq(taskId), Mockito.any(String.class), Mockito.any(String.class),
				Mockito.any(ByteArrayInputStream.class), Mockito.eq((long) rawFile.length), Mockito.any()))
				.thenReturn(Optional.of(model));
		MockMultipartFile attachmentFile = new MockMultipartFile("attachment", "file.txt", "text/plain", rawFile);

		mockMvc.perform(MockMvcRequestBuilders
				.multipart("/process/statements/" + statementId + "/task/" + taskId + "/attachments")
				.file(attachmentFile).with(csrf())).andExpect(status().is(200))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("id", is(attachmentId.intValue()))).andExpect(jsonPath("type", is("text/plain")))
				.andExpect(jsonPath("name", is("file.txt")));
	}

	@Test
	void mailDispatch() throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		MailSendReport report = new MailSendReport();
		report.setSuccessful(true);
		String reason = "reason";
		report.setReason(reason);
		Mockito.when(statementProcessService.dispatchStatementResponseAndCompleteTask(statementId, taskId))
				.thenReturn(report);
		mockMvc.perform(
				post("/process/statements/" + statementId + "/task/" + taskId + "/mailandcomplete").with(csrf()))
				.andExpect(status().is(200)).andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("successful", is(true))).andExpect(jsonPath("reason", is(reason)));
	}

	@Test
	void mailDispatchWithInternalErrorServiceExceptionShouldReturnInternalServerError() throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		Mockito.doThrow(new InternalErrorServiceException()).when(statementProcessService)
				.dispatchStatementResponseAndCompleteTask(statementId, taskId);
		mockMvc.perform(
				post("/process/statements/" + statementId + "/task/" + taskId + "/mailandcomplete").with(csrf()))
				.andExpect(status().is(500));
	}

	@Test
	void mailDispatchWithForbiddenServiceExceptionShouldReturnForbidden() throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		Mockito.doThrow(new ForbiddenServiceException()).when(statementProcessService)
				.dispatchStatementResponseAndCompleteTask(statementId, taskId);
		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/mailandcomplete"))
				.andExpect(status().is(403));
	}

	@Test
	void mailDispatchWithNotFoundServiceExceptionShouldReturnNotFound() throws Exception {
		Long statementId = 404L;
		String taskId = "taskId";
		Mockito.doThrow(new NotFoundServiceException()).when(statementProcessService)
				.dispatchStatementResponseAndCompleteTask(statementId, taskId);
		mockMvc.perform(
				post("/process/statements/" + statementId + "/task/" + taskId + "/mailandcomplete").with(csrf()))
				.andExpect(status().is(404));
	}

	@Test
	void transferMailText() throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";

		AttachmentModel attachmentModel = new AttachmentModel();
		Long attachmentId = 123L;
		attachmentModel.setId(attachmentId);
		Mockito.when(statementProcessService.transferMailText(statementId, taskId)).thenReturn(attachmentModel);
		mockMvc.perform(
				post("/process/statements/" + statementId + "/task/" + taskId + "/transfermailtext").with(csrf()))
				.andExpect(status().is(200)).andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("id", is(attachmentId.intValue())));
	}

	@Test
	void transferMailTextWithInternalErrorServiceExceptionShouldReturnInternalServerError() throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		Mockito.doThrow(new InternalErrorServiceException()).when(statementProcessService).transferMailText(statementId,
				taskId);
		mockMvc.perform(
				post("/process/statements/" + statementId + "/task/" + taskId + "/transfermailtext").with(csrf()))
				.andExpect(status().is(500));
	}

	@Test
	void transferMailTextWithForbiddenServiceExceptionShouldReturnForbidden() throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		Mockito.doThrow(new ForbiddenServiceException()).when(statementProcessService).transferMailText(statementId,
				taskId);
		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/transfermailtext"))
				.andExpect(status().is(403));
	}

	@Test
	void transferMailTextWithNotFoundServiceExceptionShouldReturnNotFound() throws Exception {
		Long statementId = 404L;
		String taskId = "taskId";
		Mockito.doThrow(new NotFoundServiceException()).when(statementProcessService).transferMailText(statementId,
				taskId);
		mockMvc.perform(
				post("/process/statements/" + statementId + "/task/" + taskId + "/transfermailtext").with(csrf()))
				.andExpect(status().is(404));
	}

	@Test
	void transferMailAttachments() throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";

		String content = "[{\"name\":\"name\", \"tagIds\":[]}]";

		AttachmentModel attachmentModel = new AttachmentModel();
		Long attachmentId = 123L;
		attachmentModel.setId(attachmentId);
		List<AttachmentModel> attachmentModels = new ArrayList<>();
		attachmentModels.add(attachmentModel);
		Mockito.when(statementProcessService.transferMailAttachments(Mockito.eq(statementId), Mockito.eq(taskId),
				Mockito.anyList())).thenReturn(attachmentModels);
		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/transfermailattachments")
				.with(csrf()).content(content).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is(200))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("[0].id", is(attachmentId.intValue())));
	}

	@Test
	void transferMailAttachmentsWithInternalErrorServiceExceptionShouldReturnInternalServerError() throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";

		String content = "[{\"name\":\"name\", \"tagIds\":[]}]";

		Mockito.doThrow(new InternalErrorServiceException()).when(statementProcessService)
				.transferMailAttachments(Mockito.eq(statementId), Mockito.eq(taskId), Mockito.anyList());
		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/transfermailattachments")
				.with(csrf()).content(content).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is(500));
	}

	@Test
	void transferMailAttachmentsWithForbiddenServiceExceptionShouldReturnForbidden() throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		String content = "[{\"name\":\"name\", \"tagIds\":[]}]";

		Mockito.doThrow(new ForbiddenServiceException()).when(statementProcessService)
				.transferMailAttachments(Mockito.eq(statementId), Mockito.eq(taskId), Mockito.anyList());
		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/transfermailattachments")
				.content(content).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is(403));
	}

	@Test
	void transferMailAttachmentsWithNotFoundServiceExceptionShouldReturnNotFound() throws Exception {
		Long statementId = 404L;
		String taskId = "taskId";
		String content = "[{\"name\":\"name\", \"tagIds\":[]}]";

		Mockito.doThrow(new NotFoundServiceException()).when(statementProcessService)
				.transferMailAttachments(Mockito.eq(statementId), Mockito.eq(taskId), Mockito.anyList());
		mockMvc.perform(post("/process/statements/" + statementId + "/task/" + taskId + "/transfermailattachments")
				.with(csrf()).content(content).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is(404));
	}

	@Test
	void getClaimDetails() throws Exception {

		Long statementId = 1234L;
		String taskId = "taskId";
		String assignee = "assignee";
		String claimedUntilTime = "claimedUntilTime";
		String currentTime = "currentTime";
		Boolean isCurrent = true;

		ClaimDetailsModel details = new ClaimDetailsModel();
		details.setAssignee(assignee);
		details.setClaimedUntilTime(claimedUntilTime);
		details.setCurrent(isCurrent);
		details.setCurrentTime(currentTime);
		details.setStatementId(statementId);
		details.setTaskId(taskId);
		Mockito.when(statementProcessService.getClaimDetails(statementId, taskId)).thenReturn(details);

		mockMvc.perform(get("/process/statements/" + statementId + "/task/" + taskId + "/claim-details"))
				.andExpect(status().is(200)).andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("statementId", is(statementId.intValue())))
				.andExpect(jsonPath("assignee", is(assignee)))
				.andExpect(jsonPath("claimedUntilTime", is(claimedUntilTime)))
				.andExpect(jsonPath("current", is(isCurrent))).andExpect(jsonPath("currentTime", is(currentTime)))
				.andExpect(jsonPath("taskId", is(taskId)));
	}

	@Test
	void getClaimDetailsWithInvalidShouldReturnInternalServerError() throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		Mockito.when(statementProcessService.getClaimDetails(statementId, taskId)).thenReturn(null);
		mockMvc.perform(get("/process/statements/" + statementId + "/task/" + taskId + "/claim-details"))
				.andExpect(status().is(500));
	}

	@Test
	void getClaimDetailsWithInternalErrorServiceExceptionShouldReturnInternalServerError() throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		Mockito.when(statementProcessService.getClaimDetails(statementId, taskId)).thenReturn(null);
		Mockito.doThrow(new InternalErrorServiceException()).when(statementProcessService).getClaimDetails(statementId,
				taskId);

		mockMvc.perform(get("/process/statements/" + statementId + "/task/" + taskId + "/claim-details"))
				.andExpect(status().is(500));
	}

	@Test
	void getClaimDetailsWithForbiddenServiceExceptionShouldReturnForbidden() throws Exception {
		Long statementId = 1234L;
		String taskId = "taskId";
		Mockito.when(statementProcessService.getClaimDetails(statementId, taskId)).thenReturn(null);
		Mockito.doThrow(new ForbiddenServiceException()).when(statementProcessService).getClaimDetails(statementId,
				taskId);

		mockMvc.perform(get("/process/statements/" + statementId + "/task/" + taskId + "/claim-details"))
				.andExpect(status().is(403));
	}
	
	
	@Test
	void cancelStatement() throws Exception {
		Long statementId = 1234L;
		mockMvc.perform(post("/process/statements/" + statementId + "/cancel").with(csrf())).andExpect(status().is(200));
		Mockito.verify(statementProcessService).cancelStatement(statementId);
	}

	@Test
	void cancelStatementConflictServiceException() throws Exception {
		Long statementId = 1234L;
		Mockito.doThrow(new ConflictServiceException()).when(statementProcessService).cancelStatement(statementId);
		mockMvc.perform(post("/process/statements/" + statementId + "/cancel").with(csrf())).andExpect(status().is(409));
	}

	@Test
	void cancelStatementInternalErrorServiceException() throws Exception {
		Long statementId = 1234L;
		Mockito.doThrow(new InternalErrorServiceException()).when(statementProcessService).cancelStatement(statementId);
		mockMvc.perform(post("/process/statements/" + statementId + "/cancel").with(csrf())).andExpect(status().is(500));
	}

	@Test
	void cancelStatementNotFoundServiceException() throws Exception {
		Long statementId = 1234L;
		Mockito.doThrow(new NotFoundServiceException()).when(statementProcessService).cancelStatement(statementId);
		mockMvc.perform(post("/process/statements/" + statementId + "/cancel").with(csrf())).andExpect(status().is(404));
	}

	@Test
	void cancelStatementForbiddenServiceException() throws Exception {
		Long statementId = 1234L;
		Mockito.doThrow(new ForbiddenServiceException()).when(statementProcessService).cancelStatement(statementId);
		mockMvc.perform(post("/process/statements/" + statementId + "/cancel").with(csrf())).andExpect(status().is(403));
	}

	
	
	@Test
	void reviveStatement() throws Exception {
		Long statementId = 1234L;
		mockMvc.perform(post("/process/statements/" + statementId + "/revive").with(csrf())).andExpect(status().is(200));
		Mockito.verify(statementProcessService).reviveStatement(statementId);
	}

	@Test
	void reviveStatementConflictServiceException() throws Exception {
		Long statementId = 1234L;
		Mockito.doThrow(new ConflictServiceException()).when(statementProcessService).reviveStatement(statementId);
		mockMvc.perform(post("/process/statements/" + statementId + "/revive").with(csrf())).andExpect(status().is(409));
	}

	@Test
	void reviveStatementInternalErrorServiceException() throws Exception {
		Long statementId = 1234L;
		Mockito.doThrow(new InternalErrorServiceException()).when(statementProcessService).reviveStatement(statementId);
		mockMvc.perform(post("/process/statements/" + statementId + "/revive").with(csrf())).andExpect(status().is(500));
	}

	@Test
	void reviveStatementNotFoundServiceException() throws Exception {
		Long statementId = 1234L;
		Mockito.doThrow(new NotFoundServiceException()).when(statementProcessService).reviveStatement(statementId);
		mockMvc.perform(post("/process/statements/" + statementId + "/revive").with(csrf())).andExpect(status().is(404));
	}

	@Test
	void reviveStatementForbiddenServiceException() throws Exception {
		Long statementId = 1234L;
		Mockito.doThrow(new ForbiddenServiceException()).when(statementProcessService).reviveStatement(statementId);
		mockMvc.perform(post("/process/statements/" + statementId + "/revive").with(csrf())).andExpect(status().is(403));
	}
	

}
