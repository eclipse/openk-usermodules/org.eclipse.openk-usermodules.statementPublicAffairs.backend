/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.eclipse.openk.statementpublicaffairs.StatementPublicAffairsApplication;
import org.eclipse.openk.statementpublicaffairs.service.SessionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Test VersionController REST endpoints.
 * 
 * @author Tobias Stummer
 *
 */
@SpringBootTest(classes = StatementPublicAffairsApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
class SessionControllerTest {

    @MockBean
    private SessionService sessionService;


    @Autowired
    private MockMvc mockMvc;

    /**
     * Test /keepalive-session GET endpoint responds with HTTP.OK.
     * 
     * @throws Exception
     */
    @Test
    void keepAliveSessionEndpointShouldRespondHTTPOK() throws Exception {
        mockMvc.perform(get("/keepalive-session")).andExpect(status().is2xxSuccessful());
    }

    /**
     * Test /logout GET endpoint responds with the response status code of the
     * sessionService logout response.
     * 
     * @throws Exception
     */
    @Test
    void logoutSessionEndpointShouldRespondWithResponseStatusCodeOfSessionService() throws Exception {
        int responseStatusCode = 201;
        when(sessionService.logout()).thenReturn(responseStatusCode);
        mockMvc.perform(get("/logout")).andExpect(status().is(responseStatusCode));
    }
}
