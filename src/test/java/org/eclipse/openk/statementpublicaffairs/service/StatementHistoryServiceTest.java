/*
 *******************************************************************************
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationStatementHistoryService;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.model.HistoryModel;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement;
import org.eclipse.openk.statementpublicaffairs.model.db.TblTextblockdefinition;
import org.eclipse.openk.statementpublicaffairs.model.db.TblWorkflowdata;
import org.eclipse.openk.statementpublicaffairs.model.db.VwStatementdraftHistory;
import org.eclipse.openk.statementpublicaffairs.repository.StatementRepository;
import org.eclipse.openk.statementpublicaffairs.repository.VwStatementDraftHistoryRepository;
import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest(classes = TestConfigurationStatementHistoryService.class)
@ActiveProfiles("test")
class StatementHistoryServiceTest {

	@Autowired
	public VwStatementDraftHistoryRepository statementDraftHistoryRepository;
	@Autowired
	public StatementRepository statementRepository;
	@Autowired
	public ReplacementStringsService replacementStringsService;
	@Autowired
	public StatementHistoryService statementHistoryService;

	@Test
	void testGolden()
			throws IOException, ForbiddenServiceException, NotFoundServiceException, InternalErrorServiceException {
		List<GoldenTC> testCases = loadTestCases();
		for (GoldenTC testCase : testCases) {

			TblStatement tblStatement = Mockito.mock(TblStatement.class);
			TblWorkflowdata tblWorkflowdata = Mockito.mock(TblWorkflowdata.class);
			Mockito.when(tblStatement.getWorkflowdata()).thenReturn(tblWorkflowdata);
			TblTextblockdefinition tblTextblockDefinition = Mockito.mock(TblTextblockdefinition.class);
			Mockito.when(tblTextblockDefinition.getDefinition()).thenReturn(testCase.getTextBlockDefinition());
			Mockito.when(tblWorkflowdata.getTextBlockDefinition()).thenReturn(tblTextblockDefinition);
			Map<String, String> replacementsMap = new HashMap<>();
			Mockito.when(replacementStringsService.getAllReplacements(tblStatement)).thenReturn(replacementsMap);

			Long statementId = testCase.statementId;

			Mockito.when(statementRepository.findById(statementId)).thenReturn(Optional.of(tblStatement));
			List<VwStatementdraftHistory> sdhs = testCase.getDraftEntries().stream().map(draftEntry -> {
				VwStatementdraftHistory sdh = new VwStatementdraftHistory();
				sdh.setDraft(draftEntry.getDraft());
				sdh.setId(1L);
				sdh.setMsg(draftEntry.getMsg());
				sdh.setStatementId(statementId);
				TypeConversion.localDateTimeUTCofIso8601InstantString(draftEntry.getTimestamp())
						.ifPresent(sdh::setTimestamp);
				sdh.setUserFirstName(draftEntry.getUserFirstName());
				sdh.setUserLastName(draftEntry.getUserLastName());
				sdh.setUsername(draftEntry.getUsername());
				sdh.setVersion(draftEntry.getVersion());
				return sdh;
			}).collect(Collectors.toList());
			Mockito.when(statementDraftHistoryRepository.findByStatementIdOrderByVersionAsc(statementId))
					.thenReturn(sdhs);

			HistoryModel history = statementHistoryService.getHistory(statementId);

			assertNotNull(history);
			int prevVersNo = 0;
			for (String v : history.getVersionOrder()) {
				int versNo = Integer.valueOf(v);
				assertTrue(versNo > prevVersNo);
				prevVersNo = versNo;
			}

			Set<String> versionOrderStrings = new HashSet<>(history.getVersionOrder());
			assertEquals(history.getVersions().keySet(), versionOrderStrings);
			if (history.getVersionOrder().size() > 0) {
				assertEquals(history.getCurrentVersion(),
						history.getVersionOrder().get(history.getVersionOrder().size() - 1));
			} else {
				assertNull(history.getCurrentVersion());
			}

			assertEquals(statementId, history.getStatementId());
			Optional<LocalDateTime> oTs = TypeConversion.localDateTimeUTCofIso8601InstantString(history.getTimestamp());
			assertTrue(oTs.isPresent());
			LocalDateTime ts = oTs.get();
			LocalDateTime now = LocalDateTime.now();
			assertTrue(!now.isBefore(ts) && now.minusMinutes(2).isBefore(ts));

			assertEquals(testCase.getVersions(), history.getVersions());
		}
	}

	private List<GoldenTC> loadTestCases() throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		Path resourceDirectory = Paths.get("src", "test", "resources", "text");
		return Files.list(resourceDirectory).map(path -> {
			GoldenTC res = null;
			try {
				res = objectMapper.readValue(path.toFile(), GoldenTC.class);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return res;
		}).collect(Collectors.toList());
	}

}
