/*
 *******************************************************************************
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationStatementAuthorizationService;
import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.model.conf.AuthorizationRuleActions;
import org.eclipse.openk.statementpublicaffairs.model.conf.EvaluationResult;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = TestConfigurationStatementAuthorizationService.class)
@ActiveProfiles("test")

class StatementAuthorizationServiceTest {

	@Autowired
	private UserInfoService userInfoService;

	@Autowired
	private StatementAuthorizationService authorizationService;

	@Test
	void authorize() throws ForbiddenServiceException {

		String userName = "userName";
		String[] userRoles = { "ROLE_SPA_ACCESS", "B" };
		Mockito.when(userInfoService.getUserName()).thenReturn(userName);
		Mockito.when(userInfoService.getUserRoles()).thenReturn(userRoles);

		authorizationService.authorize("ANY", AuthorizationRuleActions.A_READ_STATEMENT);
		try {
			authorizationService.authorize("ASDFXYZ", AuthorizationRuleActions.A_READ_STATEMENT);
			fail("Should have thrown ForbiddenServiceException");
		} catch (ForbiddenServiceException e) {
			// pass
		}

	}

	@Test
	void authorizeByClaimedUser() throws ForbiddenServiceException {

		String assignee = "assignee";

		String notassignee = "notassignee";

		Mockito.when(userInfoService.isOwnUserName(assignee)).thenReturn(true);
		authorizationService.authorizeByClaimedUser(assignee);

		try {
			authorizationService.authorizeByClaimedUser(notassignee);
			fail("Should have thrown ForbiddenServiceException");
		} catch (ForbiddenServiceException e) {
			// pass
		}

	}

	@Test
	void evaluateAuthorization() {

		String[] userRoles = { "ROLE_SPA_APPROVER", "ROLE_SPA_DIVISION_MEMBER" };
		Mockito.when(userInfoService.getUserRoles()).thenReturn(userRoles);

		assertEquals(EvaluationResult.DENIED, authorizationService.evaluateAuthorization("taskDefinitionKey"));
		assertEquals(EvaluationResult.ONLY_REQ_DEPARTMENT_USER,
				authorizationService.evaluateAuthorization("enrichDraft"));

		String[] userRoles2 = { "ROLE_SPA_OFFICIAL_IN_CHARGE" };
		Mockito.when(userInfoService.getUserRoles()).thenReturn(userRoles2);
		assertEquals(EvaluationResult.ACCEPTED, authorizationService.evaluateAuthorization("ANY"));
	}

	@Test
	void authorizeIsUserOrHasRoleOIC() {
		String assignee = "user";
		String action = "action";
		String[] userRoles = { UserRoles.SPA_OFFICIAL_IN_CHARGE };
		Mockito.when(userInfoService.getUserName()).thenReturn(assignee);
		Mockito.when(userInfoService.getUserRoles()).thenReturn(userRoles);

		try {
			authorizationService.authorizeIsUserOrHasRoleOIC(assignee, action);
			// pass
		} catch (ForbiddenServiceException e) {
			fail("Should not have thrown ForbiddenServiceException");
		}

	}

	@Test
	void authorizeIsUserOrHasRoleOICNotAssignee() throws ForbiddenServiceException {
		String assignee = "user";
		String action = "action";

		String[] userRoles = { UserRoles.SPA_OFFICIAL_IN_CHARGE };
		Mockito.when(userInfoService.getUserName()).thenReturn("not assignee");
		Mockito.when(userInfoService.getUserRoles()).thenReturn(userRoles);
		try {
			authorizationService.authorizeIsUserOrHasRoleOIC(assignee, action);
		} catch (ForbiddenServiceException e) {
			fail("Should not have thrown Exception");
		}

	}

	@Test
	void authorizeIsUserOrHasRoleOICNotOIC() throws ForbiddenServiceException {
		String assignee = "user";
		String action = "action";

		String[] userRoles = {};
		Mockito.when(userInfoService.getUserName()).thenReturn("not assignee");
		Mockito.when(userInfoService.getUserRoles()).thenReturn(userRoles);
		try {
			authorizationService.authorizeIsUserOrHasRoleOIC(assignee, action);
			fail("Should have thrown Exception");
		} catch (ForbiddenServiceException e) {
			// pass
		}
	}

}
