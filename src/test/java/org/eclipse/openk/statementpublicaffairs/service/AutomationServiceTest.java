/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationAutomationService;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.WorkflowTaskModel;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement;
import org.eclipse.openk.statementpublicaffairs.repository.StatementRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest(classes = TestConfigurationAutomationService.class)
@ActiveProfiles("test")
class AutomationServiceTest {

	private static String AUTOMATIONSERVICE_MEMBER_NAME_AUTO_UNCLAIM_ENABLE = "autoUnclaimAllStatementsEnable";
	private static String AUTOMATIONSERVICE_MEMBER_NAME_UNCLAIMTIMEOUTENABLE = "unclaimTimedoutStatementEnable";
	private static String AUTOMATIONSERVICE_MEMBER_NAME_NOTIFYENHANCEDUEDATEENABLE = "notifyEnhanceDueDateEnable";

	@Autowired
	private StatementRepository statementRepository;

	@Autowired
	private WorkflowService workflowService;

	@Autowired
	private AutomationService automationService;

	@Autowired
	private NotifyService notifyService;

	@BeforeEach
	void beforeEach() {
		ReflectionTestUtils.setField(automationService, AUTOMATIONSERVICE_MEMBER_NAME_AUTO_UNCLAIM_ENABLE, false);
		ReflectionTestUtils.setField(automationService, AUTOMATIONSERVICE_MEMBER_NAME_UNCLAIMTIMEOUTENABLE, false);
		ReflectionTestUtils.setField(automationService, AUTOMATIONSERVICE_MEMBER_NAME_NOTIFYENHANCEDUEDATEENABLE,
				false);
	}

	@Test
	void testAutoUnClaimWithEnabledIsFalseDoesNotUnclaimStatementTasks() {

		ReflectionTestUtils.setField(automationService, AUTOMATIONSERVICE_MEMBER_NAME_AUTO_UNCLAIM_ENABLE, false);

		automationService.autoUnClaim();

		Mockito.verifyNoInteractions(statementRepository);
		Mockito.verifyNoInteractions(workflowService);
	}

	@Test
	void testAutoUnClaimWithEnabledIsTrueUnclaimsAllStatementTasks() throws InternalErrorServiceException {

		String bk1 = "bk1";
		String taskId1 = "taskId1";
		String bk2 = "bk2";
		String taskId2 = "taskId2";

		List<TblStatement> statements = new ArrayList<>();

		TblStatement statement1 = Mockito.mock(TblStatement.class);
		Mockito.when(statement1.getBusinessKey()).thenReturn(bk1);
		TblStatement statement2 = Mockito.mock(TblStatement.class);
		Mockito.when(statement2.getBusinessKey()).thenReturn(bk2);
		statements.add(statement1);
		statements.add(statement2);

		Mockito.when(statementRepository.findByFinishedAndCanceled(false, false)).thenReturn(statements);

		List<WorkflowTaskModel> wftm1List = new ArrayList<>();
		WorkflowTaskModel wftm1 = Mockito.mock(WorkflowTaskModel.class);
		wftm1List.add(wftm1);
		Mockito.when(wftm1.getTaskId()).thenReturn(taskId1);
		Mockito.when(wftm1.isAssigned()).thenReturn(true);

		List<WorkflowTaskModel> wftm2List = new ArrayList<>();
		WorkflowTaskModel wftm2 = Mockito.mock(WorkflowTaskModel.class);
		wftm2List.add(wftm2);
		Mockito.when(wftm2.getTaskId()).thenReturn(taskId2);
		Mockito.when(wftm2.isAssigned()).thenReturn(true);

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(bk1)).thenReturn(Optional.of(wftm1List));
		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(bk2)).thenReturn(Optional.of(wftm2List));

		ReflectionTestUtils.setField(automationService, AUTOMATIONSERVICE_MEMBER_NAME_AUTO_UNCLAIM_ENABLE, true);
		automationService.autoUnClaim();

		Mockito.verify(workflowService, Mockito.times(1)).unClaimTask(taskId1);
		Mockito.verify(workflowService, Mockito.times(1)).unClaimTask(taskId2);
		Mockito.verify(workflowService, Mockito.times(2)).unClaimTask(Mockito.anyString());

	}

	@Test
	void testUnclaimTimedoutStatementWithEnabledIsFalseDoesNotUnclaimStatementTasks() {
		ReflectionTestUtils.setField(automationService, AUTOMATIONSERVICE_MEMBER_NAME_UNCLAIMTIMEOUTENABLE, false);

		automationService.unclaimTimedoutStatement();

		Mockito.verifyNoInteractions(statementRepository);
		Mockito.verifyNoInteractions(workflowService);
	}

	@Test
	void testUnclaimTimedoutStatementWithEnabledIsTrueUnclaimsAllStatementTasksWithClaimOlderThanConfiguredTimeoutMinutes()
			throws InternalErrorServiceException {

		String bk1 = "bk1";
		String taskId1 = "taskId1";
		String bk2 = "bk2";
		String taskId2 = "taskId2";

		List<TblStatement> statements = new ArrayList<>();

		TblStatement statement1 = Mockito.mock(TblStatement.class);
		Mockito.when(statement1.getBusinessKey()).thenReturn(bk1);
		TblStatement statement2 = Mockito.mock(TblStatement.class);
		Mockito.when(statement2.getBusinessKey()).thenReturn(bk2);
		statements.add(statement1);
		statements.add(statement2);

		Mockito.when(statementRepository.findByFinishedAndCanceled(false, false)).thenReturn(statements);

		List<WorkflowTaskModel> wftm1List = new ArrayList<>();
		WorkflowTaskModel wftm1 = Mockito.mock(WorkflowTaskModel.class);
		wftm1List.add(wftm1);
		Mockito.when(wftm1.getTaskId()).thenReturn(taskId1);
		Mockito.when(wftm1.isAssigned()).thenReturn(true);

		List<WorkflowTaskModel> wftm2List = new ArrayList<>();
		WorkflowTaskModel wftm2 = Mockito.mock(WorkflowTaskModel.class);
		wftm2List.add(wftm2);
		Mockito.when(wftm2.getTaskId()).thenReturn(taskId2);
		Mockito.when(wftm2.isAssigned()).thenReturn(true);

		Mockito.when(
				workflowService.hasUserOperationAfterTimestamp(Mockito.eq(taskId1), Mockito.any(ZonedDateTime.class)))
				.thenReturn(true);
		Mockito.when(
				workflowService.hasUserOperationAfterTimestamp(Mockito.eq(taskId2), Mockito.any(ZonedDateTime.class)))
				.thenReturn(false);

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(bk1)).thenReturn(Optional.of(wftm1List));
		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(bk2)).thenReturn(Optional.of(wftm2List));

		ReflectionTestUtils.setField(automationService, AUTOMATIONSERVICE_MEMBER_NAME_UNCLAIMTIMEOUTENABLE, true);
		automationService.unclaimTimedoutStatement();

		Mockito.verify(workflowService, Mockito.times(1)).unClaimTask(taskId2);
		Mockito.verify(workflowService, Mockito.times(1)).unClaimTask(Mockito.anyString());

	}

	@Test
	void testNotifyDepartmentsLackingContributionWithEnabledIsFalseDoesNotNotify() {
		ReflectionTestUtils.setField(automationService, AUTOMATIONSERVICE_MEMBER_NAME_NOTIFYENHANCEDUEDATEENABLE,
				false);

		automationService.notifyDepartmentsLackingContribution();

		Mockito.verifyNoInteractions(statementRepository);
		Mockito.verifyNoInteractions(workflowService);
		Mockito.verifyNoInteractions(notifyService);
	}

	@Test
	void testNotifyDepartmentsLackingContributionWithEnabledIsTrueDoesInitiateNotificationForAllDueContributors()
			throws InternalErrorServiceException {

		// Three statements:
		// * first is not due
		// * second is due and in task enrichDraft
		// * third is due but not in task enrichDraft

		// expected behaviour : first only checks duedate, second initiates
		// notification, third only gets to the task name verification.

		ReflectionTestUtils.setField(automationService, AUTOMATIONSERVICE_MEMBER_NAME_NOTIFYENHANCEDUEDATEENABLE, true);

		long statementId1 = 21341L;
		long statementId2 = 21342L;
		long statementId3 = 21343L;
		LocalDate departmentsDueDate1 = LocalDate.now().plusDays(3);
		LocalDate departmentsDueDate2 = LocalDate.now().plusDays(2);
		LocalDate departmentsDueDate3 = LocalDate.now().plusDays(1);
		String businessKey1 = "bk1";
		String businessKey2 = "bk2";
		String businessKey3 = "bk3";

		List<TblStatement> notFinnishedStatements = new ArrayList<>();

		TblStatement statement1 = Mockito.mock(TblStatement.class);
		Mockito.when(statement1.getId()).thenReturn(statementId1);
		Mockito.when(statement1.getDepartmentsDueDate()).thenReturn(departmentsDueDate1);
		Mockito.when(statement1.getBusinessKey()).thenReturn(businessKey1);

		TblStatement statement2 = Mockito.mock(TblStatement.class);
		Mockito.when(statement2.getId()).thenReturn(statementId2);
		Mockito.when(statement2.getDepartmentsDueDate()).thenReturn(departmentsDueDate2);
		Mockito.when(statement2.getBusinessKey()).thenReturn(businessKey2);
		List<WorkflowTaskModel> tasksStatement2 = new ArrayList<>();
		WorkflowTaskModel taskStatement2 = Mockito.mock(WorkflowTaskModel.class);
		tasksStatement2.add(taskStatement2);
		Mockito.when(taskStatement2.getTaskDefinitionKey()).thenReturn("enrichDraft");
		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey2))
				.thenReturn(Optional.of(tasksStatement2));

		TblStatement statement3 = Mockito.mock(TblStatement.class);
		Mockito.when(statement3.getId()).thenReturn(statementId3);
		Mockito.when(statement3.getDepartmentsDueDate()).thenReturn(departmentsDueDate3);
		Mockito.when(statement3.getBusinessKey()).thenReturn(businessKey3);

		List<WorkflowTaskModel> tasksStatement3 = new ArrayList<>();
		WorkflowTaskModel taskStatement3 = Mockito.mock(WorkflowTaskModel.class);
		tasksStatement3.add(taskStatement3);
		Mockito.when(taskStatement3.getName()).thenReturn("otherTask");
		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey3))
				.thenReturn(Optional.of(tasksStatement3));

		notFinnishedStatements.add(statement1);
		notFinnishedStatements.add(statement2);
		notFinnishedStatements.add(statement3);

		Mockito.when(statementRepository.findByFinishedAndCanceled(false, false)).thenReturn(notFinnishedStatements);

		// run notifyDepartment
		automationService.notifyDepartmentsLackingContribution();

		Mockito.verify(workflowService, Mockito.never()).getCurrentTasksOfProcessBusinessKey(businessKey1);
		Mockito.verify(notifyService, Mockito.never()).notifyDepartmentsDueDate(statementId1);
		Mockito.verify(notifyService, Mockito.times(1)).notifyDepartmentsDueDate(statementId2);
		Mockito.verify(notifyService, Mockito.never()).notifyDepartmentsDueDate(statementId3);
	}

}
