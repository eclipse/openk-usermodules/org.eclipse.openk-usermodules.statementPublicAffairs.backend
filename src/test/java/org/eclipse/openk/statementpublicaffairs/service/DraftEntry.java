/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.util.List;

import org.eclipse.openk.statementpublicaffairs.model.Textblock;

import lombok.Data;

@Data
public class DraftEntry {
	private List<Textblock> draft;
	private String msg;
	private String timestamp;
	private Long version;
	private String username;
	private String userFirstName;
	private String userLastName;
}
