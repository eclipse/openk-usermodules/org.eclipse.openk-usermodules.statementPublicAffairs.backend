/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service.compile;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.TextToken;
import org.eclipse.openk.statementpublicaffairs.model.Textblock;
import org.eclipse.openk.statementpublicaffairs.model.Textblock.TextblockType;
import org.eclipse.openk.statementpublicaffairs.model.TextblockDefinition;
import org.eclipse.openk.statementpublicaffairs.model.TextblockGroup;
import org.eclipse.openk.statementpublicaffairs.model.TextblockItem;
import org.eclipse.openk.statementpublicaffairs.viewmodel.TextConfiguration;
import org.junit.jupiter.api.Test;

class TextCompileUtilTest {

	/**
	 * Special Testcase bulletpoints
	 * 
	 * @throws BadRequestServiceException
	 */
	@Test
	void parseTextToTokenWithGoldenTextBulltepoints() throws BadRequestServiceException {

		// *
		String testString1 = "*";
		Token[] tokenarr1 = { Token.STRING };
		assertTrue(compare(testString1, tokenarr1), "Error comparing " + testString1);
		// *a
		String testString2 = "*a";
		Token[] tokenarr2 = { Token.STRING, Token.STRING };
		assertTrue(compare(testString2, tokenarr2), "Error comparing " + testString2);
		// *<SPC>
		String testString3 = "* ";
		Token[] tokenarr3 = { Token.TK_BULLET };
		assertTrue(compare(testString3, tokenarr3), "Error comparing " + testString3);
		// *<SPC><NL>
		String testString4 = "* \n";
		Token[] tokenarr4 = { Token.TK_BULLET, Token.TK_NL };
		assertTrue(compare(testString4, tokenarr4), "Error comparing " + testString4);
		// *<SPC>a
		String testString5 = "* a";
		Token[] tokenarr5 = { Token.TK_BULLET, Token.STRING };
		assertTrue(compare(testString5, tokenarr5), "Error comparing " + testString5);
		// **
		String testString6 = "**";
		Token[] tokenarr6 = { Token.TK_BOLD };
		assertTrue(compare(testString6, tokenarr6), "Error comparing " + testString6);
		// **a
		String testString7 = "**a";
		Token[] tokenarr7 = { Token.TK_BOLD, Token.STRING };
		assertTrue(compare(testString7, tokenarr7), "Error comparing " + testString7);
		// **<NL>
		String testString8 = "**\n";
		Token[] tokenarr8 = { Token.TK_BOLD, Token.TK_NL };
		assertTrue(compare(testString8, tokenarr8), "Error comparing " + testString8);
		// <NL>*
		String testString9 = "\n*";
		Token[] tokenarr9 = { Token.TK_NL, Token.STRING };
		assertTrue(compare(testString9, tokenarr9), "Error comparing " + testString9);
		// <NL>*a
		String testString10 = "\n*a";
		Token[] tokenarr10 = { Token.TK_NL, Token.STRING, Token.STRING };
		assertTrue(compare(testString10, tokenarr10), "Error comparing " + testString10);
		// <NL>*<SPC>
		String testString11 = "\n* ";
		Token[] tokenarr11 = { Token.TK_NL, Token.TK_BULLET };
		assertTrue(compare(testString11, tokenarr11), "Error comparing " + testString11);
		// <NL>*<SPC><NL>
		String testString12 = "\n* \n";
		Token[] tokenarr12 = { Token.TK_NL, Token.TK_BULLET, Token.TK_NL };
		assertTrue(compare(testString12, tokenarr12), "Error comparing " + testString12);
		// <NL>*<SPC>a
		String testString13 = "\n* a";
		Token[] tokenarr13 = { Token.TK_NL, Token.TK_BULLET, Token.STRING };
		assertTrue(compare(testString13, tokenarr13), "Error comparing " + testString13);
		// <NL>**
		String testString14 = "\n**";
		Token[] tokenarr14 = { Token.TK_NL, Token.TK_BOLD };
		assertTrue(compare(testString14, tokenarr14), "Error comparing " + testString14);
		// <NL>**a
		String testString15 = "\n**a";
		Token[] tokenarr15 = { Token.TK_NL, Token.TK_BOLD, Token.STRING };
		assertTrue(compare(testString15, tokenarr15), "Error comparing " + testString15);
		// <NL>**<NL>
		String testString16 = "\n**\n";
		Token[] tokenarr16 = { Token.TK_NL, Token.TK_BOLD, Token.TK_NL };
		assertTrue(compare(testString16, tokenarr16), "Error comparing " + testString16);
		// Das ist * ein Stern
		String testString17 = "Ein * Stern";
		Token[] tokenarr17 = { Token.STRING, Token.TK_SPACE, Token.STRING, Token.TK_SPACE, Token.STRING };
		assertTrue(compare(testString17, tokenarr17), "Error comparing " + testString17);

	}

	private boolean compare(String teststring, Token[] tokenarr) throws BadRequestServiceException {
		List<TextToken> texttokens = TextCompileUtil.parseTextToToken(teststring);
		List<Token> tokens = new ArrayList<>();
		for (int i = 0; i < tokenarr.length; i++) {
			tokens.add(tokenarr[i]);
		}

		boolean result = false;
		if (tokens.size() == texttokens.size()) {
			result = true;
			for (int i = 0; i < tokens.size(); i++) {
				if (tokens.get(i) != texttokens.get(i).getType()) {
					result = false;
					break;
				}
			}
		}
		return result;
	}

	@Test
	void parseTextToTokenWithGoldenText3() throws BadRequestServiceException {

		String goldenString = "Test (7,03m*4,50m); Test";
		List<TextToken> texttokens = TextCompileUtil.parseTextToToken(goldenString);
		List<Token> tokens = new ArrayList<>();
		tokens.add(Token.STRING);
		tokens.add(Token.TK_SPACE);
		tokens.add(Token.STRING);
		tokens.add(Token.STRING);
		tokens.add(Token.STRING);
		tokens.add(Token.TK_SPACE);
		tokens.add(Token.STRING);

		assertEquals(tokens.size(), texttokens.size());

		for (int i = 0; i < tokens.size(); i++) {
			assertEquals(tokens.get(i), texttokens.get(i).getType());
		}

	}

	@Test
	void parseTextToTokenWithGoldenText() throws BadRequestServiceException {

		String goldenString = "string string newline\nstring string newline\n* bullet newline\n* bullet newline\n**bold** __italic__";

		List<TextToken> texttokens = TextCompileUtil.parseTextToToken(goldenString);
		List<Token> tokens = new ArrayList<>();

		tokens.add(Token.STRING);
		tokens.add(Token.TK_SPACE);
		tokens.add(Token.STRING);
		tokens.add(Token.TK_SPACE);
		tokens.add(Token.STRING);
		tokens.add(Token.TK_NL);

		tokens.add(Token.STRING);
		tokens.add(Token.TK_SPACE);
		tokens.add(Token.STRING);
		tokens.add(Token.TK_SPACE);
		tokens.add(Token.STRING);
		tokens.add(Token.TK_NL);

		tokens.add(Token.TK_BULLET);
		tokens.add(Token.STRING);
		tokens.add(Token.TK_SPACE);
		tokens.add(Token.STRING);
		tokens.add(Token.TK_NL);

		tokens.add(Token.TK_BULLET);
		tokens.add(Token.STRING);
		tokens.add(Token.TK_SPACE);
		tokens.add(Token.STRING);
		tokens.add(Token.TK_NL);

		tokens.add(Token.TK_BOLD);
		tokens.add(Token.STRING);
		tokens.add(Token.TK_BOLD);
		tokens.add(Token.TK_SPACE);
		tokens.add(Token.TK_ITALIC);
		tokens.add(Token.STRING);
		tokens.add(Token.TK_ITALIC);

		assertEquals(tokens.size(), texttokens.size());

		for (int i = 0; i < tokens.size(); i++) {
			assertEquals(tokens.get(i), texttokens.get(i).getType());
		}
	}

	@Test
	void parseTextToTokenWithGoldenText2() throws BadRequestServiceException {

		String goldenString = "that's a underscore_test.";

		List<TextToken> texttokens = TextCompileUtil.parseTextToToken(goldenString);
		List<Token> tokens = new ArrayList<>();

		tokens.add(Token.STRING);
		tokens.add(Token.TK_SPACE);
		tokens.add(Token.STRING);
		tokens.add(Token.TK_SPACE);
		tokens.add(Token.STRING);
		tokens.add(Token.STRING);
		tokens.add(Token.STRING);

		assertEquals(tokens.size(), texttokens.size());

		for (int i = 0; i < tokens.size(); i++) {
			assertEquals(tokens.get(i), texttokens.get(i).getType());
		}
	}

	@Test
	void convertToTextTokensWithGoldenTextArrangement()
			throws InternalErrorServiceException, BadRequestServiceException {

		TextConfiguration textConfiguration = new TextConfiguration();

		textConfiguration.setReplacements(new HashMap<>());
		TextblockDefinition configuration = new TextblockDefinition();
		List<TextblockGroup> groups = new ArrayList<>();
		TextblockGroup group1 = new TextblockGroup();
		group1.setGroupName("group1");
		List<TextblockItem> textBlocks = new ArrayList<>();
		group1.setTextBlocks(textBlocks);
		groups.add(group1);
		configuration.setGroups(groups);
		textConfiguration.setConfiguration(configuration);

		List<Textblock> blocks = new ArrayList<>();

		List<List<Token>> tokenTypeSets = new ArrayList<>();

		Textblock block1 = new Textblock();
		List<Token> block1tokentypes = new ArrayList<>();
		block1.setType(TextblockType.text);
		block1.setReplacement("string\nstring\n");
		block1tokentypes.add(Token.STRING);
		block1tokentypes.add(Token.TK_NL);
		block1tokentypes.add(Token.STRING);
		block1tokentypes.add(Token.TK_NL);
		blocks.add(block1);
		tokenTypeSets.add(block1tokentypes);

		Textblock block2 = new Textblock();
		List<Token> block2tokentypes = new ArrayList<>();
		block2.setType(TextblockType.text);
		block2.setReplacement("string\n<f:replacement>\n");
		Map<String, String> block2PlaceholderValues = new HashMap<>();
		block2PlaceholderValues.put("<f:replacement>", "string string");
		block2.setPlaceholderValues(block2PlaceholderValues);
		block2tokentypes.add(Token.STRING);
		block2tokentypes.add(Token.TK_NL);
		block2tokentypes.add(Token.STRING);
		block2tokentypes.add(Token.TK_SPACE);
		block2tokentypes.add(Token.STRING);
		block2tokentypes.add(Token.TK_NL);
		blocks.add(block2);
		tokenTypeSets.add(block2tokentypes);

		Textblock block3 = new Textblock();
		List<Token> block3tokentypes = new ArrayList<>();
		block3.setType(TextblockType.newline);
		block3tokentypes.add(Token.TK_NL);
		blocks.add(block3);
		tokenTypeSets.add(block3tokentypes);

		Textblock block4 = new Textblock();
		List<Token> block4tokentypes = new ArrayList<>();
		block4.setType(TextblockType.pagebreak);
		block4tokentypes.add(Token.TK_PB);
		blocks.add(block4);
		tokenTypeSets.add(block4tokentypes);

		Textblock block5 = new Textblock();
		List<Token> block5tokentypes = new ArrayList<>();
		block5.setType(TextblockType.block);
		block5.setTextblockId("block5");
		Map<String, String> block5PlaceholderValues = new HashMap<>();
		block5PlaceholderValues.put("<f:replacement>", "string string");
		block5.setPlaceholderValues(block5PlaceholderValues);
		block5tokentypes.add(Token.TK_PB);
		blocks.add(block4);
		tokenTypeSets.add(block4tokentypes);
		TextblockItem block5Item = new TextblockItem();
		block5Item.setId("block5");
		block5Item.setText("string\n<f:replacement>\n");
		textBlocks.add(block5Item);

		List<List<TextToken>> tokenSets = TextCompileUtil.convertToTextTokens(textConfiguration, blocks);

		assertEquals(tokenSets.size(), tokenTypeSets.size());

		for (int i = 0; i < tokenTypeSets.size(); i++) {
			List<TextToken> textTokens = tokenSets.get(i);
			List<Token> tokens = tokenTypeSets.get(i);

			for (int j = 0; j < tokens.size(); j++) {
				assertEquals(tokens.get(j), textTokens.get(j).getType());
			}
		}

	}

	@Test
	void getTextBlockDefForTextblockIdWithValidTextBlockIdRespondsWithTextBlockItem() {
		TextblockDefinition configuration = new TextblockDefinition();
		List<TextblockGroup> groups = new ArrayList<>();
		TextblockGroup group1 = new TextblockGroup();
		group1.setGroupName("group1");
		List<TextblockItem> textBlocks = new ArrayList<>();
		group1.setTextBlocks(textBlocks);
		groups.add(group1);
		configuration.setGroups(groups);
		TextblockItem item = new TextblockItem();
		item.setId("item");
		textBlocks.add(item);

		TextblockItem found = TextCompileUtil.getTextblockDefForTextblockId(configuration, "item");
		assertEquals("item", found.getId());
	}

	@Test
	void getTextBlockDefForTextblockIdWithInvalidTextBlockIdRespondsNull() {

		TextblockDefinition configuration = new TextblockDefinition();
		List<TextblockGroup> groups = new ArrayList<>();
		TextblockGroup group1 = new TextblockGroup();
		group1.setGroupName("group1");
		List<TextblockItem> textBlocks = new ArrayList<>();
		group1.setTextBlocks(textBlocks);
		groups.add(group1);
		configuration.setGroups(groups);
		configuration.setNegativeGroups(new ArrayList<>());
		TextblockItem item = new TextblockItem();
		item.setId("item");
		textBlocks.add(item);

		TextblockItem notFound = TextCompileUtil.getTextblockDefForTextblockId(configuration, "notFound");
		assertNull(notFound);
	}

	@Test
	void convertToTextWithInvalidTextBlockDefShouldThrowInternalErrorServiceException() {

		TextConfiguration textConfiguration = new TextConfiguration();
		textConfiguration.setReplacements(new HashMap<>());
		TextblockDefinition configuration = new TextblockDefinition();
		textConfiguration.setConfiguration(configuration);
		List<TextblockGroup> groups = new ArrayList<>();
		TextblockGroup group1 = new TextblockGroup();
		group1.setGroupName("group1");
		List<TextblockItem> textBlocks = new ArrayList<>();
		group1.setTextBlocks(textBlocks);
		groups.add(group1);
		configuration.setGroups(groups);
		configuration.setNegativeGroups(new ArrayList<>());
		TextblockItem item = new TextblockItem();
		item.setId("item");
		textBlocks.add(item);
		Textblock block = new Textblock();
		block.setType(TextblockType.block);
		block.setTextblockId("notFound");
		try {
			TextCompileUtil.convertToText(block, textConfiguration);
			fail("Should have thrown InternalErrorServiceException");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void fillPlaceholderWithGoldenParameters() {

		String text = "A <f:freetext> <d:date> <t:statement> <s:select> B";
		String compare = "A freetext REPLACEMENT 01.01.1970 statement REPLACEMENT second B";

		Map<String, String> textPlaceholder = new HashMap<>();
		textPlaceholder.put("<f:freetext>", "freetext REPLACEMENT");
		textPlaceholder.put("<s:select>", "1");
		textPlaceholder.put("<d:date>", "01.01.1970");

		Map<String, String> replacements = new HashMap<>();
		replacements.put("statement", "statement REPLACEMENT");
		Map<String, List<String>> selects = new HashMap<>();
		List<String> selectEntries = new ArrayList<>();
		selectEntries.add("first");
		selectEntries.add("second");
		selects.put("select", selectEntries);

		String result = TextCompileUtil.fillPlaceholder(text, textPlaceholder, replacements, selects);

		assertEquals(compare, result);

	}

	@Test
	void selectValueForWithValidIdShouldRespondWithValue() {

		String name = "name";
		String id = "1";
		String value = "value";

		Map<String, List<String>> selectValues = new HashMap<>();
		List<String> values = new ArrayList<>();
		values.add("zero");
		values.add(value);
		values.add("two");
		selectValues.put(name, values);

		Optional<String> oValue = TextCompileUtil.selectValueFor(name, id, selectValues);
		assertTrue(oValue.isPresent());
		assertEquals(value, oValue.get());

	}

	@Test
	void selectValueForWithInvalidIdShouldRespondOptionalEmpty() {

		String name = "name";
		String id = "4";
		String value = "value";

		Map<String, List<String>> selectValues = new HashMap<>();
		List<String> values = new ArrayList<>();
		values.add("zero");
		values.add(value);
		values.add("two");
		selectValues.put(name, values);

		assertFalse(TextCompileUtil.selectValueFor(name, id, selectValues).isPresent());

	}

}
