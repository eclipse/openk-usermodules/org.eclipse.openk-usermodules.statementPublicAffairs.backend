/*
 *******************************************************************************
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationTaskAdditionsService;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.TaskInfo;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaVariable;
import org.eclipse.openk.statementpublicaffairs.viewmodel.MailSendReport;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = TestConfigurationTaskAdditionsService.class)
@ActiveProfiles("test")
class TaskAdditionsServiceTest {
	
	@Autowired
	NotifyService notificationService;
	
	@Autowired
	MailService mailService;
	
	@Autowired
	StatementService statementService;
	
	@Autowired
	TaskAdditionsService taskAdditionsService;

	@Test
	void onBeforeCompleteTaskAddSentStatusToVariablesMap() {
		
		Long statementId = 1234L;;
		TaskInfo taskInfo = Mockito.mock(TaskInfo.class);
		Mockito.when(taskInfo.getTaskDefinitionKey()).thenReturn("approveStatement");
		Map<String, CamundaVariable> variablesMap = new HashMap<>();
		CamundaVariable camundaBoolTrue = new CamundaVariable();
		camundaBoolTrue.setType("Boolean");
		camundaBoolTrue.setValue(true);
		variablesMap.put("approved_statement", camundaBoolTrue);
		
		MailSendReport report = new MailSendReport();
		report.setSuccessful(true);
		Mockito.when(statementService.dispatchStatementResponse(statementId)).thenReturn(report);
		taskAdditionsService.onBeforeCompleteTask(statementId, taskInfo, variablesMap);
		
		assertTrue(variablesMap.containsKey("sent"));
		assertEquals("Boolean", variablesMap.get("sent").getType());
		assertEquals(true, variablesMap.get("sent").getValue());
	}
	
	
	@Test
	void prepareAndSendStatementResponse() {
		
		Long statementId = 1234L;
		MailSendReport report = new MailSendReport();
		report.setSuccessful(true);
		Mockito.when(statementService.dispatchStatementResponse(statementId)).thenReturn(report);
		
		assertTrue(taskAdditionsService.prepareAndSendStatementResponse(statementId));
	}
	

	@Test
	void onAfterCompleteTaskCreateNegativeResponse() throws InternalErrorServiceException {
		Long statementId = 1234L;;
		TaskInfo taskInfo = Mockito.mock(TaskInfo.class);
		Mockito.when(taskInfo.getTaskDefinitionKey()).thenReturn("createNegativeResponse");
		Map<String, CamundaVariable> variablesMap = new HashMap<>();
		CamundaVariable camundaBoolTrue = new CamundaVariable();
		camundaBoolTrue.setType("Boolean");
		camundaBoolTrue.setValue(true);
		variablesMap.put("response_created", camundaBoolTrue);
		taskAdditionsService.onAfterCompleteTask(statementId, taskInfo, variablesMap);
		Mockito.verify(notificationService).notifyApprovalRequired(statementId);
		
	}

	@Test
	void onAfterCompleteTaskCheckAndFormulateResponse() throws InternalErrorServiceException {
		Long statementId = 1234L;;
		TaskInfo taskInfo = Mockito.mock(TaskInfo.class);
		Mockito.when(taskInfo.getTaskDefinitionKey()).thenReturn("checkAndFormulateResponse");
		Map<String, CamundaVariable> variablesMap = new HashMap<>();
		CamundaVariable camundaBoolTrue = new CamundaVariable();
		camundaBoolTrue.setType("Boolean");
		camundaBoolTrue.setValue(true);
		variablesMap.put("response_created", camundaBoolTrue);
		variablesMap.put("data_complete", camundaBoolTrue);
		taskAdditionsService.onAfterCompleteTask(statementId, taskInfo, variablesMap);
		Mockito.verify(notificationService).notifyApprovalRequired(statementId);
	}

	@Test
	void onAfterCompleteTaskcreateDraft() throws InternalErrorServiceException {
		
		Long statementId = 1234L;;
		TaskInfo taskInfo = Mockito.mock(TaskInfo.class);
		Mockito.when(taskInfo.getTaskDefinitionKey()).thenReturn("createDraft");
		Map<String, CamundaVariable> variablesMap = new HashMap<>();
		taskAdditionsService.onAfterCompleteTask(statementId, taskInfo, variablesMap);
		Mockito.verify(notificationService).notifyEnrichDraft(statementId);
		
	}

	@Test
	void onAfterCompleteTaskApproveStatement() throws InternalErrorServiceException {
		Long statementId = 1234L;;
		TaskInfo taskInfo = Mockito.mock(TaskInfo.class);
		Mockito.when(taskInfo.getTaskDefinitionKey()).thenReturn("approveStatement");
		Map<String, CamundaVariable> variablesMap = new HashMap<>();
		CamundaVariable camundaBoolTrue = new CamundaVariable();
		camundaBoolTrue.setType("Boolean");
		camundaBoolTrue.setValue(true);
		variablesMap.put("approved_statement", camundaBoolTrue);
		CamundaVariable camundaSentBoolTrue = new CamundaVariable();
		camundaSentBoolTrue.setType("Boolean");
		camundaSentBoolTrue.setValue(true);
		variablesMap.put("sent", camundaSentBoolTrue);
		taskAdditionsService.onAfterCompleteTask(statementId, taskInfo, variablesMap);
		Mockito.verify(notificationService).notifyApproved(statementId, true);
		
	}
	
}
