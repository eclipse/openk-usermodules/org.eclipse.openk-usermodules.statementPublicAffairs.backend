/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.repository;

import java.util.List;

import org.eclipse.openk.statementpublicaffairs.model.db.VwStatementPositionSearch;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.CrudRepository;

public interface VwStatementPositionRepository extends CrudRepository<VwStatementPositionSearch, Long> {

	List<VwStatementPositionSearch> findAll(Specification<VwStatementPositionSearch> specification);

}
