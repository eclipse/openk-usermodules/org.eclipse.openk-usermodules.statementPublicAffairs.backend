/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.repository;

import java.util.List;

import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement2parent;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface Statement2ParentRepository extends CrudRepository<TblStatement2parent, Long> {

    @Query("select s from TblStatement2parent s where s.statement_id = ?1")
	List<TblStatement2parent> searchByStatementId(Long id);

    @Query("select s from TblStatement2parent s where s.parent_id = ?1")
	List<TblStatement2parent> searchByParentId(Long id);

}