/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.repository;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.eclipse.openk.statementpublicaffairs.model.SearchParams;
import org.eclipse.openk.statementpublicaffairs.model.db.VwStatementSearch;
import org.springframework.data.jpa.domain.Specification;

public class VwStatementSearchSpecification implements Specification<VwStatementSearch> {

	private static final long serialVersionUID = 1L;

	private SearchParams searchParams;

	public VwStatementSearchSpecification(SearchParams params) {
		this.searchParams = params;
	}

	@Override
	public Predicate toPredicate(Root<VwStatementSearch> root, CriteriaQuery<?> query,
			CriteriaBuilder criteriaBuilder) {
		Predicate[] predicates = createArrayOfPredicates(root, criteriaBuilder);
		if (predicates == null || predicates.length == 0) {
			return null;
		}
		return criteriaBuilder.and(predicates);
	}

	private Predicate[] createArrayOfPredicates(Root<VwStatementSearch> root, CriteriaBuilder criteriaBuilder) {
		return VwPredicateUtil.createBasicStatementSearchPredicates(root, criteriaBuilder, searchParams).stream()
				.toArray(Predicate[]::new);
	}

}
