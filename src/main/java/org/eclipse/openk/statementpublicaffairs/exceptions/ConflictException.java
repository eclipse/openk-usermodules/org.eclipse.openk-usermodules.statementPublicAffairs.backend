/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Throw exception to create Conflict Http responses.
 * 
 * @author Tobias Stummer
 *
 */
@ResponseStatus(code = HttpStatus.CONFLICT)
public class ConflictException extends RuntimeException {

	private static final String DEFAULT_MESSAGE = "Request conflicts.";

	public ConflictException() {
		super(DEFAULT_MESSAGE);
	}

	/**
	 * Create ConflictException with own message.
	 * 
	 * @param message Message
	 */
	public ConflictException(String message) {
		super(message);
	}

	/**
	 * Create ConflictException with own message and cause.
	 * 
	 * @param message Message
	 * @param cause   Cause
	 */
	public ConflictException(String message, Throwable cause) {
		super(message, cause);
	}
}
