/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.api;

import java.util.List;

import org.eclipse.openk.statementpublicaffairs.model.JwtToken;
import org.eclipse.openk.statementpublicaffairs.model.KeyCloakUser;
import org.eclipse.openk.statementpublicaffairs.model.LoginCredentials;
import org.eclipse.openk.statementpublicaffairs.model.VersionInfo;
import org.eclipse.openk.statementpublicaffairs.viewmodel.UserModule;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Interfaces used to access the AuthNAuth module.
 * 
 * @author Tobias Stummer
 *
 */
@FeignClient(name = "${services.authNAuth.name}", url = "${services.authNAuth.url}")
public interface AuthNAuthApi {

	@PostMapping(value = "/portal/rest/beservice/login")
	JwtToken login(@RequestBody LoginCredentials loginCredentials);

	@GetMapping(value = "/portal/rest/beservice/checkAuth")
	feign.Response isTokenValid(@RequestHeader("Authorization") String token);

	@GetMapping(value = "/portal/rest/beservice/logout")
	feign.Response logout(@RequestHeader("Authorization") String token);

	@GetMapping(value = "/portal/rest/beservice/versionInfo")
	VersionInfo versionInfo();

	@GetMapping(value = "/portal/rest/beservice/userModulesForUser")
	List<UserModule> getUserModulesForUser(@RequestHeader("Authorization") String token);

	@GetMapping(value = "/portal/rest/beservice/usersForRole/{userRole}")
	List<KeyCloakUser> getUsersForRole(@RequestHeader("Authorization") String token,
			@RequestParam String userRole);

	@GetMapping(value = "/portal/rest/beservice/users")
	List<KeyCloakUser> getKeycloakUsers(@RequestHeader("Authorization") String token);

}
