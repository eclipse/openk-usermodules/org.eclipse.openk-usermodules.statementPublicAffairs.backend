/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;
import java.util.stream.Collectors;

import jakarta.persistence.PersistenceException;

import org.eclipse.openk.statementpublicaffairs.api.MailUtil;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ConflictServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.UnprocessableEntityServiceException;
import org.eclipse.openk.statementpublicaffairs.model.AttachmentFile;
import org.eclipse.openk.statementpublicaffairs.model.CompanyContactBlockModel;
import org.eclipse.openk.statementpublicaffairs.model.Tags;
import org.eclipse.openk.statementpublicaffairs.model.conf.AuthorizationRuleActions;
import org.eclipse.openk.statementpublicaffairs.model.conf.Rule;
import org.eclipse.openk.statementpublicaffairs.model.db.TblAttachment;
import org.eclipse.openk.statementpublicaffairs.model.db.TblAttachment2Tag;
import org.eclipse.openk.statementpublicaffairs.model.db.TblAttachmentLob;
import org.eclipse.openk.statementpublicaffairs.model.db.TblDepartmentstructure;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement2parent;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatementEditLog;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatementtype;
import org.eclipse.openk.statementpublicaffairs.model.db.TblTag;
import org.eclipse.openk.statementpublicaffairs.model.db.TblUser;
import org.eclipse.openk.statementpublicaffairs.model.db.VwDeletableStatementtype;
import org.eclipse.openk.statementpublicaffairs.model.db.VwDeletableTag;
import org.eclipse.openk.statementpublicaffairs.model.mail.MailConfiguration;
import org.eclipse.openk.statementpublicaffairs.model.mail.MailEntry;
import org.eclipse.openk.statementpublicaffairs.model.mail.MessageConfiguration;
import org.eclipse.openk.statementpublicaffairs.model.mail.NewMailContext;
import org.eclipse.openk.statementpublicaffairs.repository.Attachment2TagRepository;
import org.eclipse.openk.statementpublicaffairs.repository.AttachmentLobRepository;
import org.eclipse.openk.statementpublicaffairs.repository.AttachmentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.DepartmentstructureRepository;
import org.eclipse.openk.statementpublicaffairs.repository.Statement2ParentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementEditLogRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementtypeRepository;
import org.eclipse.openk.statementpublicaffairs.repository.TagRepository;
import org.eclipse.openk.statementpublicaffairs.repository.VwDeletableStatementtypeRepository;
import org.eclipse.openk.statementpublicaffairs.repository.VwDeletableTagRepository;
import org.eclipse.openk.statementpublicaffairs.util.Time;
import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;
import org.eclipse.openk.statementpublicaffairs.viewmodel.AttachmentModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.DistrictDepartmentsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.MailSendReport;
import org.eclipse.openk.statementpublicaffairs.viewmodel.MailTransferAttachment;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementDetailsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.TagModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import lombok.extern.java.Log;

/**
 * StatementService provides interfaces to access and manipulate statement
 * details.
 * 
 * @author Tobias Stummer
 *
 */
@Log
@Service
public class StatementService {

	public static final String FILE_NAME_MAIL_TEXT = "mailText.txt";

	public static final String FILE_TYPE_MAIL_TEXT = "text/plain";

	@Autowired
	private StatementAuthorizationService authorizationService;

	@Autowired
	private StatementRepository statementRepository;

	@Autowired
	private AttachmentRepository attachmentRepository;

	@Autowired
	private StatementtypeRepository statementTypeRepository;

	@Autowired
	private DepartmentstructureRepository departmentStructureRepository;

	@Autowired
	private TagRepository tagRepository;

	@Autowired
	private Attachment2TagRepository attachment2TagRepository;

	@Autowired
	private StatementEditLogRepository statementEditLogRepository;

	@Autowired
	private Statement2ParentRepository statement2ParentRepository;

	@Autowired
	private AttachmentLobRepository attachmentLobRepository;

	@Autowired
	private VwDeletableTagRepository vwDeletableTagRepository;

	@Autowired
	private VwDeletableStatementtypeRepository vwDeletableStatementtypeRepository;

	@Autowired
	private ContactService contactService;

	@Autowired
	private MailService mailService;

	@Autowired
	private MailUtil mailUtil;

	@Autowired
	private UsersService usersService;

	@Autowired
	private UserInfoService userInfoService;

	@Lazy
	@Autowired
	private ArchiveService archiveService;

	/**
	 * Get statement details for statement with given statementId parameter.
	 * 
	 * @param statementId statement Id
	 * @return Optional(StatementDetailsModel) if statement could be found.
	 *         Otherwise Optional#empty.
	 * @throws InternalErrorServiceException on errors when accessing the statement
	 *                                       repository.
	 * @throws ForbiddenServiceException
	 */
	public Optional<StatementDetailsModel> getStatement(Long statementId)
			throws InternalErrorServiceException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);

		return getStatementInternal(statementId);
	}

	public Optional<StatementDetailsModel> getStatementInternal(Long statementId) throws InternalErrorServiceException {
		if (statementId == null) {
			return Optional.empty();
		}
		try {
			Optional<TblStatement> statement = statementRepository.findById(statementId);
			if (statement.isPresent()) {
				TblStatement s = statement.get();
				StatementDetailsModel model = new StatementDetailsModel();
				TypeConversion.dateStringOfLocalDate(s.getDueDate()).ifPresent(model::setDueDate);
				TypeConversion.dateStringOfLocalDate(s.getDepartmentsDueDate()).ifPresent(model::setDepartmentsDueDate);
				TypeConversion.dateStringOfLocalDate(s.getReceiptDate()).ifPresent(model::setReceiptDate);
				TypeConversion.dateStringOfLocalDate(s.getCreationDate()).ifPresent(model::setCreationDate);
				TypeConversion.dateStringOfLocalDate(s.getFinishedDate()).ifPresent(model::setFinishedDate);
				model.setFinished(s.getFinished());
				model.setId(s.getId());
				model.setTitle(s.getTitle());
				model.setBusinessKey(s.getBusinessKey());
				model.setCity(s.getCity());
				model.setDistrict(s.getDistrict());
				model.setTypeId(s.getType().getId());
				model.setContactId(s.getContactDbId());
				model.setSourceMailId(s.getSourceMailId());
				model.setCustomerReference(s.getCustomerReference());
				model.setCanceled(s.getCanceled());
				return Optional.of(model);
			} else {
				return Optional.empty();
			}
		} catch (IllegalArgumentException e) {
			throw new InternalErrorServiceException(
					"Invalid parameters when browsing statement repository for specific statement.", e);
		} catch (PersistenceException e) {
			throw new InternalErrorServiceException("Exception occurred when accessing the statement repository.", e);
		}
	}

	/**
	 * Finish Statement with given statementId parameter.
	 * 
	 * @param statementId statement Id
	 * @return Optional(True) if statement could be finished successfully.
	 *         Optional(false) if statement already finished. Optional(null) if
	 *         statement with given statementId could not be found.
	 * @throws NotFoundServiceException
	 * @throws InternalErrorServiceException
	 */
	public Optional<Boolean> finishStatement(Long statementId, LocalDate finishedDate)
			throws ForbiddenServiceException, InternalErrorServiceException, NotFoundServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_FINISH_STATEMENT);
		if (statementId == null) {
			return Optional.empty();
		}
		Optional<TblStatement> statement = statementRepository.findById(statementId);
		if (statement.isPresent()) {
			TblStatement entity = statement.get();
			archiveService.archiveStatement(statementId, finishedDate);
			entity.setFinished(true);
			entity.setFinishedDate(finishedDate);
			statementRepository.save(entity);
			return Optional.of(true);
		} else {
			return Optional.empty();
		}
	}

	/**
	 * Create new Statement and add it to the Repository.
	 * 
	 * @param newStatement StatmentDetailsModel to store
	 * @return Statement with additional generated reference IDs from the database.
	 *         Optional#empty if provided statement is null.
	 * @throws InternalErrorServiceException       if error occurred accessing the
	 *                                             repositories
	 * @throws BadRequestServiceException          if provided statement type does
	 *                                             not exist.
	 * @throws ForbiddenServiceException
	 * @throws InterruptedException
	 * @throws NotFoundServiceException
	 * @throws UnprocessableEntityServiceException
	 */
	public Optional<StatementDetailsModel> createStatement(StatementDetailsModel newStatement)
			throws InternalErrorServiceException, BadRequestServiceException, ForbiddenServiceException,
			NotFoundServiceException, InterruptedException, UnprocessableEntityServiceException {
		if (newStatement == null || !newStatement.validate()) {
			return Optional.empty();
		}
		TblStatement statement = new TblStatement();
		setStatementParameters(newStatement, statement);
		statement.setFinished(false);
		statement.setCanceled(false);
		statement.setFinishedDate(null);
		statement.setBusinessKey(newStatement.getBusinessKey());
		statement.setDepartmentStructureId(departmentStructureRepository.getLatestDepartmentStructure().get(0).getId());

		TblStatement cStatement;
		try {
			cStatement = statementRepository.save(statement);
			if (newStatement.getSourceMailId() != null) {
				mailService.moveMailFromInboxToStatements(newStatement.getSourceMailId());
			}
		} catch (IllegalArgumentException e) {
			throw new InternalErrorServiceException("Could not store new statement to the repository", e);
		}
		editLog(cStatement.getId(), "Statement - create");
		return Optional.of(statementModelOfStatement(cStatement));
	}

	protected Set<String> tagIdsOf(Set<TblTag> tags) {
		Set<String> tagIds = new HashSet<>();
		if (tags == null) {
			return tagIds;
		}
		for (TblTag tag : tags) {
			tagIds.add(tag.getId());
		}
		return tagIds;
	}

	/**
	 * Get List of Attachments assigned to Statement of given statementId.
	 * 
	 * @param statementId Identifier of Statement
	 * @return If Statement exists, the response contains a list of all assigned
	 *         attachments. Otherwise Optional.empty.
	 * @throws ForbiddenServiceException
	 */
	public Optional<List<AttachmentModel>> getStatementAttachments(Long statementId) throws ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		if (statementId == null) {
			return Optional.empty();
		}

		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isPresent()) {
			List<AttachmentModel> attachments = new ArrayList<>();
			for (TblAttachment attachment : oStatement.get().getAttachments()) {
				AttachmentModel model = new AttachmentModel();
				model.setName(attachment.getFileName());
				model.setType(attachment.getFileType());
				model.setId(attachment.getId());
				model.setSize(attachment.getSize());
				TypeConversion.iso8601InstantStringOfLocalDateTimeUTC(attachment.getTimestamp())
						.ifPresent(model::setTimestamp);
				List<TagModel> tags = getAttachmentTags(attachment.getId());
				List<String> tagIds = new ArrayList<>();
				for (TagModel t : tags) {
					tagIds.add(t.getId());
				}
				model.setTagIds(tagIds);
				attachments.add(model);
			}
			return Optional.of(attachments);
		}
		return Optional.empty();
	}

	protected List<TagModel> getAttachmentTags(Long attachmentId) {
		List<TagModel> attachmentTags = new ArrayList<>();
		Iterable<TblAttachment2Tag> a2ts = attachment2TagRepository.findByAttachmentId(attachmentId);
		for (TblAttachment2Tag a2t : a2ts) {
			Optional<TblTag> oTag = tagRepository.findById(a2t.getTag_id());
			if (oTag.isPresent()) {
				TblTag tag = oTag.get();
				TagModel tm = new TagModel();
				tm.setId(tag.getId());
				tm.setLabel(tag.getName());
				attachmentTags.add(tm);
			}
		}
		return attachmentTags;
	}

	/**
	 * Create new Attachment for Statement with given statementId,
	 * 
	 * @param statementId Identifier of Statement
	 * @param fileName    Filename
	 * @param fileType    Filetype
	 * @param inputStream inputStream of file content
	 * @param length      inputStream content lenght
	 * @return Attachment Id of created Attachmentd if successsful. Otherwise
	 *         Optional.empty
	 * @throws ForbiddenServiceException
	 */
	public Optional<Long> createStatementAttachment(Long statementId, String fileName, String fileType,
			InputStream inputStream, long length) throws ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_MANIPULATE_ATTACHMENTS);
		Optional<TblAttachment> oAttachment = createStatementTblAttachment(statementId, fileName, fileType, inputStream,
				length);
		if (oAttachment.isPresent()) {
			return Optional.of(oAttachment.get().getId());
		}
		return Optional.empty();
	}

	public Optional<TblAttachment> createStatementTblAttachment(Long statementId, String fileName, String fileType,
			InputStream inputStream, long length) {
		if (statementId == null || fileName == null || inputStream == null) {
			return Optional.empty();
		}
		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isPresent()) {
			TblAttachment tblAttachment = new TblAttachment();
			tblAttachment.setStatement(oStatement.get());
			tblAttachment.setFileName(fileName);
			if (fileType != null) {
				tblAttachment.setFileType(fileType.split(";")[0]);
			}
			tblAttachment.setTimestamp(Time.currentTimeLocalTimeUTC());
			byte[] data = new byte[(int) length];
			try {
				int len = inputStream.read(data);
				if (len != length) {
					log.log(Level.WARNING, "Read input stream length differs from attachment meta data.");
				}
			} catch (IOException e) {
				log.log(Level.WARNING, "Read input stream of attachment caused IOException: - " + e.getMessage() + " - "
						+ e.getStackTrace());
				return Optional.empty();
			}
			tblAttachment.setSize(length);
			TblAttachmentLob lob = new TblAttachmentLob();
			lob.setValue(data);
			TblAttachmentLob rlobRes = attachmentLobRepository.save(lob);
			if (rlobRes.getId() == 0L) {
				return Optional.empty();
			}
			tblAttachment.setAttachmentLobId(rlobRes.getId());
			return Optional.of(attachmentRepository.save(tblAttachment));
		}
		return Optional.empty();
	}

	/**
	 * Get Attachment with given attachmentId of Statement with given statementId.
	 * 
	 * @param statementId  Identifier of Statement
	 * @param attachmentId Identifier of Attachment.
	 * @return AttachmentModel if attachment exists and is assigned to the correct
	 *         statement. Otherwise Optional.empty
	 * @throws ForbiddenServiceException
	 */
	public Optional<AttachmentModel> getStatementAttachment(Long statementId, Long attachmentId)
			throws ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		if (statementId == null || attachmentId == null) {
			return Optional.empty();
		}
		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isPresent()) {
			for (TblAttachment attachment : oStatement.get().getAttachments()) {
				if (attachmentId == attachment.getId()) {
					AttachmentModel model = new AttachmentModel();
					model.setName(attachment.getFileName());
					model.setType(attachment.getFileType());
					model.setId(attachment.getId());
					List<TagModel> tags = getAttachmentTags(attachment.getId());
					List<String> tagIds = new ArrayList<>();
					for (TagModel t : tags) {
						tagIds.add(t.getId());
					}
					model.setTagIds(tagIds);
					model.setSize(attachment.getSize());
					TypeConversion.iso8601InstantStringOfLocalDateTimeUTC(attachment.getTimestamp())
							.ifPresent(model::setTimestamp);
					return Optional.of(model);
				}
			}
		}
		return Optional.empty();
	}

	/**
	 * Delete Attachment with given attachmentId of Statement with fiven
	 * statementId.
	 * 
	 * @param taskDefinitionId
	 * 
	 * @param statementId      Identifier of Statement
	 * @param attachmentId     Identifier of Attachment.
	 * @param constraint
	 * @return True if successfully deleted. False if still in database.
	 *         Optional.empty if attachment could not be found.
	 * @throws InternalErrorServiceException
	 * @throws NotFoundServiceException
	 * @throws ForbiddenServiceException
	 */
	public void deleteStatementAttachments(String taskDefinitionId, Long statementId, Long attachmentId,
			String constraint)
			throws InternalErrorServiceException, NotFoundServiceException, ForbiddenServiceException {
		authorizationService.authorize(taskDefinitionId, AuthorizationRuleActions.A_MANIPULATE_ATTACHMENTS, constraint);
		if (statementId == null || attachmentId == null) {
			throw new NotFoundServiceException("Statement or attachment could not be found");
		}
		boolean found = false;
		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isPresent()) {
			for (TblAttachment attachment : oStatement.get().getAttachments()) {
				if (attachmentId.equals(attachment.getId())) {
					Iterable<TblAttachment2Tag> a2ts = attachment2TagRepository.findByAttachmentId(attachmentId);
					attachment2TagRepository.deleteAll(a2ts);
					found = true;
					attachmentRepository.deleteById(attachmentId);
					if (attachmentRepository.existsById(attachmentId)) {
						throw new InternalErrorServiceException("Error when deleting attachment.");
					}
				}
			}
			cleanupDeletableTags();
		}
		if (!found) {
			throw new NotFoundServiceException("Statement or attachment could not be found");
		}
	}

	/**
	 * Read Attachment file of requested attachmentId and statementId.
	 * 
	 * @param statementId  Identifier of Statement
	 * @param attachmentId Identifier of Attachment
	 * @return AttachmentFile instance containing the file details. Optional.empty
	 *         if attachment could not be found.
	 * @throws ForbiddenServiceException
	 */
	public Optional<AttachmentFile> getStatementAttachmentFile(Long statementId, Long attachmentId)
			throws ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		if (statementId == null || attachmentId == null) {
			return Optional.empty();
		}
		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isPresent()) {
			for (TblAttachment attachment : oStatement.get().getAttachments()) {
				if (attachmentId == attachment.getId()) {
					AttachmentFile attachmentFile = new AttachmentFile();
					attachmentFile.setType(attachment.getFileType());
					attachmentFile.setName(attachment.getFileName());
					Optional<TblAttachmentLob> oLob = attachmentLobRepository.findById(attachment.getAttachmentLobId());
					if (oLob.isEmpty()) {
						log.warning("Could not find Lob for attachment.");
						return Optional.empty();
					}
					byte[] bytes = oLob.get().getValue();
					InputStream ressource = new ByteArrayInputStream(bytes);
					attachmentFile.setLength(bytes.length);
					attachmentFile.setRessource(ressource);
					return Optional.of(attachmentFile);
				}
			}
		}
		return Optional.empty();
	}

	public Optional<CompanyContactBlockModel> getContactBlock(Long statementId)
			throws ForbiddenServiceException, InternalErrorServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isEmpty()) {
			return Optional.empty();
		}
		TblStatement statement = oStatement.get();
		return contactService.getContactDetails(statement.getContactDbId(), true);
	}

	public Optional<StatementDetailsModel> updateStatement(Long statementId, StatementDetailsModel newStatement)
			throws NotFoundServiceException, BadRequestServiceException, InternalErrorServiceException,
			ForbiddenServiceException, UnprocessableEntityServiceException {
		if (!newStatement.validate()) {
			throw new BadRequestServiceException("Invalid statement model");
		}

		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isEmpty()) {
			throw new NotFoundServiceException("Statement with statementId could not be found");
		}

		TblStatement statement = oStatement.get();

		setStatementParameters(newStatement, statement);

		TblStatement cStatement;
		try {
			cStatement = statementRepository.save(statement);
		} catch (IllegalArgumentException e) {
			throw new InternalErrorServiceException("Could not store new statement to the repository", e);
		}
		return Optional.of(statementModelOfStatement(cStatement));
	}

	private StatementDetailsModel statementModelOfStatement(TblStatement cStatement) {
		StatementDetailsModel createdModel = new StatementDetailsModel();
		TypeConversion.dateStringOfLocalDate(cStatement.getDueDate()).ifPresent(createdModel::setDueDate);
		TypeConversion.dateStringOfLocalDate(cStatement.getDepartmentsDueDate())
				.ifPresent(createdModel::setDepartmentsDueDate);
		TypeConversion.dateStringOfLocalDate(cStatement.getReceiptDate()).ifPresent(createdModel::setReceiptDate);
		TypeConversion.dateStringOfLocalDate(cStatement.getCreationDate()).ifPresent(createdModel::setCreationDate);
		TypeConversion.dateStringOfLocalDate(cStatement.getFinishedDate()).ifPresent(createdModel::setFinishedDate);
		createdModel.setFinished(cStatement.getFinished());
		createdModel.setId(cStatement.getId());
		createdModel.setBusinessKey(cStatement.getBusinessKey());
		createdModel.setTitle(cStatement.getTitle());
		createdModel.setCity(cStatement.getCity());
		createdModel.setDistrict(cStatement.getDistrict());
		createdModel.setTypeId(cStatement.getType().getId());
		createdModel.setContactId(cStatement.getContactDbId());
		createdModel.setSourceMailId(cStatement.getSourceMailId());
		createdModel.setCustomerReference(cStatement.getCustomerReference());
		createdModel.setCanceled(cStatement.getCanceled());
		return createdModel;
	}

	private void setStatementParameters(StatementDetailsModel newStatement, TblStatement statement)
			throws BadRequestServiceException, InternalErrorServiceException, ForbiddenServiceException,
			UnprocessableEntityServiceException {
		statement.setCity(newStatement.getCity());
		statement.setContactDbId(newStatement.getContactId());
		statement.setDistrict(newStatement.getDistrict());
		if (statement.getSourceMailId() == null) {
			statement.setSourceMailId(newStatement.getSourceMailId());
		}

		Optional<LocalDate> sDueDate = TypeConversion.dateOfDateString(newStatement.getDueDate());
		if (sDueDate.isEmpty()) {
			throw new BadRequestServiceException("Invalid statement model - dueDate");
		}
		statement.setDueDate(sDueDate.get());

		if (newStatement.getDepartmentsDueDate() == null) {
			newStatement.setDepartmentsDueDate(newStatement.getDueDate());
		}
		Optional<LocalDate> sDepartmentsDueDate = TypeConversion.dateOfDateString(newStatement.getDepartmentsDueDate());
		if (sDepartmentsDueDate.isEmpty()) {
			throw new BadRequestServiceException("Invalid statement model - departmentsDueDate");
		}
		statement.setDepartmentsDueDate(sDepartmentsDueDate.get());

		Optional<LocalDate> sReceiptDate = TypeConversion.dateOfDateString(newStatement.getReceiptDate());
		if (sReceiptDate.isEmpty()) {
			throw new BadRequestServiceException("Invalid statement model - receiptDate");
		}
		statement.setReceiptDate(sReceiptDate.get());

		Optional<LocalDate> sCreationDate = TypeConversion.dateOfDateString(newStatement.getCreationDate());
		if (sCreationDate.isEmpty()) {
			throw new BadRequestServiceException("Invalid statement model - creationDate");
		}
		statement.setCreationDate(sCreationDate.get());

		if (!contactService.getContactDetails(newStatement.getContactId(), false).isPresent()) {
			throw new UnprocessableEntityServiceException("Invalid contact id set in statement");
		}
		statement.setContactDbId(newStatement.getContactId());

		try {
			Optional<TblStatementtype> oStatementType = statementTypeRepository.findById(newStatement.getTypeId());
			if (oStatementType.isPresent()) {
				statement.setType(oStatementType.get());
			} else {
				throw new BadRequestServiceException(
						"Provided statement type of the new statement cannot be found in the statement types repository");
			}
		} catch (IllegalArgumentException e) {
			throw new InternalErrorServiceException("Could not read statement type from repository", e);
		}

		statement.setTitle(newStatement.getTitle());

		statement.setCustomerReference(newStatement.getCustomerReference());

	}

	public void addTag(String label)
			throws ForbiddenServiceException, ConflictServiceException, InternalErrorServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_CREATE_TAG);
		if (tagRepository.findById(label).isPresent()) {
			throw new ConflictServiceException("Tag with label " + label + " already exists");
		}

		if (tagRepository.findByName(label).isPresent()) {
			throw new ConflictServiceException("Tag with label " + label + " already exists");
		}
		try {
			TblTag tag = new TblTag();
			tag.setId(label);
			tag.setName(label);
			tag.setStandard(false);
			tag.setDisabled(false);
			tagRepository.save(tag);
			cleanupDeletableTags();
		} catch (IllegalArgumentException e) {
			throw new InternalErrorServiceException("Error adding new tag to database.");
		}
	}

	public void disableTags(Set<String> labels) throws InternalErrorServiceException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_CREATE_TAG);
		for (String label : labels) {
			Optional<TblTag> oTTag = tagRepository.findById(label);
			if (oTTag.isPresent()) {
				TblTag tTag = oTTag.get();
				if (!Boolean.TRUE.equals(tTag.getStandard())) {
					tTag.setDisabled(true);
					try {
						tagRepository.save(tTag);
					} catch (IllegalArgumentException e) {
						throw new InternalErrorServiceException("Error modifying tag database.");
					}
				}
			}
		}
		cleanupDeletableTags();
	}

	public List<TagModel> getAllTags() throws ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_BASE_DATA);
		Iterable<TblTag> allTags = tagRepository.findAll();
		List<TagModel> tags = new ArrayList<>();
		for (TblTag tTag : allTags) {
			TagModel tag = new TagModel();
			tag.setId(tTag.getId());
			tag.setLabel(tTag.getName());
			tag.setDisabled(tTag.getDisabled());
			tag.setStandard(tTag.getStandard());
			tags.add(tag);
		}
		return tags;
	}

	public Set<String> setTags(Long attachmentId, Set<String> tagIds) throws InternalErrorServiceException {
		Iterable<TblAttachment2Tag> old = attachment2TagRepository.findByAttachmentId(attachmentId);
		Set<String> allTags = new HashSet<>();

		allTags.addAll(clearUnusedTagLinks(tagIds, old));

		List<TblAttachment2Tag> addTags = createAdditionalTagLinks(attachmentId, tagIds, old);

		try {
			Iterable<TblAttachment2Tag> newTags = attachment2TagRepository.saveAll(addTags);
			for (TblAttachment2Tag a2t : newTags) {
				allTags.add(a2t.getTag_id());
			}
		} catch (IllegalArgumentException e) {
			throw new InternalErrorServiceException("Add attachment tag caused exception -" + e.getMessage(), e);
		}
		cleanupDeletableTags();
		return allTags;
	}

	public void setTags(Long statementId, Long attachmentId, Set<String> tagIds)
			throws BadRequestServiceException, NotFoundServiceException, InternalErrorServiceException {

		Optional<TblAttachment> oAttachment = attachmentRepository.findById(attachmentId);
		if (oAttachment.isEmpty()) {
			throw new NotFoundServiceException("Attachment with id " + attachmentId + " not found ");
		}
		TblAttachment attachment = oAttachment.get();
		if (!statementId.equals(attachment.getStatement().getId())) {
			throw new NotFoundServiceException(
					"Statement with id " + statementId + " does not own an attachment with id " + attachmentId);
		}

		tagsExist(tagIds);

		Iterable<TblAttachment2Tag> old = attachment2TagRepository.findByAttachmentId(attachmentId);

		clearUnusedTagLinks(tagIds, old);

		List<TblAttachment2Tag> addTags = createAdditionalTagLinks(attachmentId, tagIds, old);

		try {
			attachment2TagRepository.saveAll(addTags);
		} catch (IllegalArgumentException e) {
			throw new InternalErrorServiceException("Add attachment tag caused exception -" + e.getMessage(), e);
		}
		cleanupDeletableTags();

	}

	private List<TblAttachment2Tag> createAdditionalTagLinks(Long attachmentId, Set<String> tagIds,
			Iterable<TblAttachment2Tag> old) {
		List<TblAttachment2Tag> addTags = new ArrayList<>();
		for (String tagId : tagIds) {
			boolean found = false;
			for (TblAttachment2Tag att : old) {
				if (tagId.equals(att.getTag_id())) {
					found = true;
					break;
				}
			}
			if (!found) {
				TblAttachment2Tag newTag = new TblAttachment2Tag();
				newTag.setAttachment_id(attachmentId);
				newTag.setTag_id(tagId);
				addTags.add(newTag);
			}
		}
		return addTags;
	}

	private Set<String> clearUnusedTagLinks(Set<String> tagIds, Iterable<TblAttachment2Tag> old)
			throws InternalErrorServiceException {

		Set<String> allTags = new HashSet<>();
		List<TblAttachment2Tag> removeList = new ArrayList<>();
		for (TblAttachment2Tag att : old) {
			boolean found = false;
			for (String reqTagId : tagIds) {
				if (att.getTag_id().equals(reqTagId)) {
					found = true;
					break;
				}
			}
			if (!found) {
				removeList.add(att);
			} else {
				allTags.add(att.getTag_id());
			}
		}

		try {
			attachment2TagRepository.deleteAll(removeList);
		} catch (IllegalArgumentException e) {
			throw new InternalErrorServiceException("Deleting attachment tag caused exception -" + e.getMessage(), e);
		}
		return allTags;
	}

	private void tagsExist(Set<String> tagIds) throws BadRequestServiceException {
		// verify all tagIds exist
		Iterable<TblTag> allTags = tagRepository.findAll();
		for (String tagid : tagIds) {
			boolean found = false;
			for (TblTag tag : allTags) {
				if (tagid.equals(tag.getId())) {
					found = true;
					break;
				}
			}
			if (!found) {
				throw new BadRequestServiceException("Could not find tag with tagId " + tagid);
			}
		}
	}

	public Map<String, List<String>> getAllSectors() throws ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_BASE_DATA);
		List<TblDepartmentstructure> departmentStructures = departmentStructureRepository
				.getLatestDepartmentStructure();
		if (departmentStructures.isEmpty()) {
			return new HashMap<>();
		}
		return extractSectors(departmentStructures.get(0));
	}

	private Map<String, List<String>> extractSectors(TblDepartmentstructure departmentStructure) {
		Map<String, List<String>> sectors = new HashMap<>();
		Map<String, DistrictDepartmentsModel> def = departmentStructure.getDefinition();
		for (Entry<String, DistrictDepartmentsModel> entry : def.entrySet()) {
			String key = entry.getKey();
			DistrictDepartmentsModel ddm = entry.getValue();
			List<String> value = ddm.getProvides();
			sectors.put(key, value);
		}
		return sectors;
	}

	public Map<String, List<String>> getAllSectors(Long statementId)
			throws NotFoundServiceException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		return getAllSectorsInternal(statementId);

	}

	public Map<String, List<String>> getAllSectorsInternal(Long statementId) throws NotFoundServiceException {
		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isEmpty()) {
			throw new NotFoundServiceException("Statement not found");
		}
		TblStatement statement = oStatement.get();
		Optional<TblDepartmentstructure> oDepartmentStructure = departmentStructureRepository
				.findById(statement.getDepartmentStructureId());
		if (oDepartmentStructure.isPresent()) {
			return extractSectors(oDepartmentStructure.get());
		}
		return new HashMap<>();
	}

	public MailSendReport dispatchStatementResponse(Long statementId) {
		MailSendReport report;
		try {
			Optional<CompanyContactBlockModel> oContact = getContactBlock(statementId);
			if (oContact.isEmpty()) {
				throw new NotFoundServiceException("Could not find valid contact for statement with id " + statementId);
			}
			CompanyContactBlockModel contact = oContact.get();
			if (!mailUtil.validEmailAddress(contact.getEmail())) {
				throw new BadRequestServiceException(
						"Contact has no email address for statement with id " + statementId);
			}
			Optional<List<AttachmentModel>> oAttachmentModels = getStatementAttachments(statementId);
			if (oAttachmentModels.isEmpty()) {
				throw new NotFoundServiceException("Could not find attachments for statement with id " + statementId);
			}
			List<AttachmentModel> attachmentModels = oAttachmentModels.get();
			List<AttachmentFile> attachments = new ArrayList<>();
			boolean hasStatementResponseAttachment = false;
			for (AttachmentModel model : attachmentModels) {
				if (model.getTagIds().contains(Tags.OUTBOX.tagId())) {
					if (model.getTagIds().contains(Tags.STATEMENT.tagId())) {
						hasStatementResponseAttachment = true;
					}
					Optional<AttachmentFile> oAttachmentFile = getStatementAttachmentFile(statementId, model.getId());
					if (oAttachmentFile.isPresent()) {
						attachments.add(oAttachmentFile.get());
					} else {
						throw new InternalErrorServiceException(
								"Prepare statement response: could not get attachment file for " + statementId + " - "
										+ model.getId());
					}
				}
			}
			if (!hasStatementResponseAttachment) {
				throw new BadRequestServiceException("Statement has no attachment that is tagged both "
						+ Tags.OUTBOX.tagId() + " and " + Tags.STATEMENT.tagId());
			}

			NewMailContext mail = new NewMailContext();
			Set<String> recipients = new HashSet<>();
			recipients.add(contact.getEmail());

			MailConfiguration mailConfig = mailUtil.getMailConfiguration();
			MessageConfiguration messageConfig = mailConfig.getStatementResponse();
			mailUtil.filterRecipients(recipients, mailConfig);

			mail.setRecipients(recipients);
			mail.setSubject(messageConfig.getSubject().replace("<t:id>", "" + statementId));
			mail.setMailText(messageConfig.getBody());
			mail.setAttachments(attachments);
			mail.setBccRecipients(messageConfig.getBccEmailAddresses());

			report = mailService.sendStatementMail(mail);
		} catch (InternalErrorServiceException | NotFoundServiceException | BadRequestServiceException
				| ForbiddenServiceException e) {
			String errorMessage = "Error occurred preparing statement mail. - " + e.getMessage();
			log.warning(errorMessage);
			report = new MailSendReport();
			report.setSuccessful(false);
			report.setReason(errorMessage);
		}
		return report;
	}

	public AttachmentModel transferMailTextToAttachment(Long statementId)
			throws NotFoundServiceException, InternalErrorServiceException, ForbiddenServiceException {
		// get mail

		MailEntry mail = getMail(statementId);

		byte[] mailTextBytes = mail.getTextPlain().getBytes(StandardCharsets.UTF_8);
		InputStream mailTextInputStream = new ByteArrayInputStream(mailTextBytes);
		Optional<Long> oAttachmentId = createStatementAttachment(statementId, FILE_NAME_MAIL_TEXT, FILE_TYPE_MAIL_TEXT,
				mailTextInputStream, mailTextBytes.length);
		if (oAttachmentId.isPresent()) {
			Set<String> tagIds = new HashSet<>();
			tagIds.add(Tags.EMAIL.tagId());
			tagIds.add(Tags.EMAIL_TEXT.tagId());
			try {
				setTags(statementId, oAttachmentId.get(), tagIds);
			} catch (BadRequestServiceException e) {
				throw new InternalErrorServiceException(
						"Could not set tags email and email-text to mail-text attachment", e);
			}

			AttachmentModel model = new AttachmentModel();
			model.setId(oAttachmentId.get());
			model.setTagIds(new ArrayList<>(tagIds));
			model.setSize((long) mailTextBytes.length);
			model.setName(FILE_NAME_MAIL_TEXT);
			model.setType(FILE_TYPE_MAIL_TEXT);
			return model;
		}
		throw new NotFoundException("Attachment successfully uploaded, but now not found in attachment repository.");
	}

	private MailEntry getMail(Long statementId) throws NotFoundServiceException, ForbiddenServiceException {
		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isEmpty()) {
			throw new NotFoundServiceException("Could not find statement with statementId " + statementId);
		}
		TblStatement statement = oStatement.get();

		Optional<MailEntry> oMail = mailService.getMail(statement.getSourceMailId());
		if (oMail.isEmpty()) {
			throw new NotFoundServiceException("Statement with statementId " + statementId + " has no source mail.");
		}
		return oMail.get();
	}

	public List<AttachmentModel> transferMailAttachments(Long statementId,
			List<MailTransferAttachment> transferAttachments)
			throws NotFoundServiceException, InternalErrorServiceException, ForbiddenServiceException {

		List<AttachmentModel> transferredAttachments = new ArrayList<>();
		MailEntry mail = getMail(statementId);

		Map<String, Set<String>> attachmentTagsForName = new HashMap<>();
		for (MailTransferAttachment tA : transferAttachments) {
			attachmentTagsForName.put(tA.getName(), tA.getTagIds());
		}
		Map<String, AttachmentFile> mailAttachments = mailService.getStatementInboxMailAttachment(mail.getIdentifier(),
				attachmentTagsForName.keySet());
		for (AttachmentFile maf : mailAttachments.values()) {
			String fileName = maf.getName();
			Optional<Long> oAttachmentId = createStatementAttachment(statementId, fileName, maf.getType(),
					maf.getRessource(), maf.getLength());
			if (oAttachmentId.isPresent()) {
				Set<String> tagIds = new HashSet<>();
				tagIds.add(Tags.EMAIL.tagId());
				tagIds.addAll(attachmentTagsForName.get(fileName));
				try {
					setTags(statementId, oAttachmentId.get(), tagIds);
				} catch (BadRequestServiceException e) {
					throw new InternalErrorServiceException(
							"Could not set tags email and email-text to mail-text attachment", e);
				}
				AttachmentModel model = new AttachmentModel();
				model.setId(oAttachmentId.get());
				model.setTagIds(new ArrayList<>(tagIds));
				model.setSize(maf.getLength());
				model.setName(fileName);
				model.setType(maf.getType());
				transferredAttachments.add(model);
			}
		}
		return transferredAttachments;
	}

	public void editLog(Long statementId, String accessType) {
		String username = userInfoService.getUserName();
		Optional<TblUser> oUser = usersService.getTblUser(username);
		if (oUser.isEmpty()) {
			return;
		}
		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isEmpty()) {
			return;
		}
		TblUser user = oUser.get();
		TblStatement statement = oStatement.get();
		TblStatementEditLog editLog = new TblStatementEditLog();
		editLog.setStatement(statement);
		editLog.setUser(user);
		editLog.setTimestamp(Time.currentTimeLocalTimeUTC());
		editLog.setAccesstype(accessType);
		statementEditLogRepository.save(editLog);
	}

	public Optional<String> getStatementBusinessKey(Long statementId) {
		Optional<TblStatement> statement = statementRepository.findById(statementId);
		if (statement.isPresent()) {
			TblStatement s = statement.get();
			if (s.getBusinessKey() != null) {
				return Optional.of(s.getBusinessKey());
			}
		}
		return Optional.empty();
	}

	public Optional<AttachmentModel> addConsideration(Long statementId, String originalFilename, String contentType,
			InputStream is, long size) throws NotFoundServiceException, ForbiddenServiceException,
			BadRequestServiceException, InternalErrorServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_ADD_CONSIDERATION,
				AuthorizationRuleActions.C_IS_FINISHED);
		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isEmpty()) {
			throw new NotFoundServiceException("Could not find statement with statementId");
		}

		TblStatement statement = oStatement.get();
		if (!Boolean.TRUE.equals(statement.getFinished())) {
			throw new BadRequestServiceException("Statement is not finished.");
		}

		Optional<TblAttachment> oAttachment = createStatementTblAttachment(statementId, originalFilename, contentType,
				is, size);

		if (oAttachment.isEmpty()) {
			throw new InternalErrorServiceException("Could not store attachment");
		}

		Set<String> tagIds = new HashSet<>();
		tagIds.add(Tags.CONSIDERATION.tagId());
		TblAttachment attachment = oAttachment.get();
		Set<String> allTagIds = setTags(attachment.getId(), tagIds);
		AttachmentModel model = new AttachmentModel();
		model.setId(attachment.getId());
		model.setName(attachment.getFileName());
		model.setSize(attachment.getSize());
		TypeConversion.iso8601InstantStringOfLocalDateTimeUTC(attachment.getTimestamp()).ifPresent(model::setTimestamp);
		model.setType(attachment.getFileType());
		model.setTagIds(new ArrayList<>(allTagIds));

		archiveService.archiveStatement(statementId, statement.getFinishedDate());
		return Optional.of(model);
	}

	public String cancelStatement(Long statementId) throws NotFoundServiceException, ConflictServiceException {
		String oldBusinessKey = null;
		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isPresent()) {
			TblStatement statement = oStatement.get();
			if (Boolean.FALSE.equals(statement.getFinished()) && Boolean.FALSE.equals(statement.getCanceled())) {
				List<TblStatement2parent> statementRefs = statement2ParentRepository.searchByStatementId(statementId);
				statement2ParentRepository.deleteAll(statementRefs);
				List<TblStatement2parent> statementParentRefs = statement2ParentRepository
						.searchByParentId(statementId);
				statement2ParentRepository.deleteAll(statementParentRefs);
				oldBusinessKey = statement.getBusinessKey();
				statement.setBusinessKey("");
				statement.setCanceled(true);
				statementRepository.save(statement);
			} else {
				throw new ConflictServiceException("Statement already finished or already canceled");
			}
		} else {
			throw new NotFoundServiceException("Could not find requested statement.");
		}
		return oldBusinessKey;
	}

	public void reviveStatement(Long statementId, String newBusinessKey)
			throws ConflictServiceException, NotFoundServiceException {
		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isPresent()) {
			TblStatement statement = oStatement.get();
			if (Boolean.TRUE.equals(statement.getCanceled())) {
				statement.setBusinessKey(newBusinessKey);
				statement.setCanceled(false);
				statementRepository.save(statement);
			} else {
				throw new ConflictServiceException("Statement is not canceled");
			}
		} else {
			throw new NotFoundServiceException("Could not find requested statement.");
		}
	}

	public void cleanupDeletableTags() {
		List<String> toDeleteIds = vwDeletableTagRepository.findAll().stream().map(VwDeletableTag::getId)
				.collect(Collectors.toList());
		for (String tagId : toDeleteIds) {
			try {
				tagRepository.deleteById(tagId);
			} catch (DataAccessException e) {
				// ignore if already removed. Could happen if more than one deletion in
				// parallel.
			}
		}
	}

	public void cleanupDeletableStatementTypes() {
		List<Long> toDeleteIds = vwDeletableStatementtypeRepository.findAll().stream()
				.map(VwDeletableStatementtype::getId).collect(Collectors.toList());
		for (Long typeId : toDeleteIds) {
			try {
				statementTypeRepository.deleteById(typeId);
			} catch (DataAccessException e) {
				// ignore if already removed. Could happen if more than one deletion in
				// parallel.
			}
		}
	}

}
