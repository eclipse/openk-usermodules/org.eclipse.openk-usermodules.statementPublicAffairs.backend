/*
 *******************************************************************************
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import org.eclipse.openk.statementpublicaffairs.api.MailUtil;
import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.UserModel;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement;
import org.eclipse.openk.statementpublicaffairs.model.db.VwStatementReqdepartmentUsers;
import org.eclipse.openk.statementpublicaffairs.model.mail.MailConfiguration;
import org.eclipse.openk.statementpublicaffairs.model.mail.MessageConfiguration;
import org.eclipse.openk.statementpublicaffairs.model.mail.NewMailContext;
import org.eclipse.openk.statementpublicaffairs.repository.StatementRepository;
import org.eclipse.openk.statementpublicaffairs.repository.VwStatementReqdepartmentUsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import lombok.extern.java.Log;

/**
 * NotifyService is the central service to notify users about important
 * transitions within a statement workflow that require a user action from them.
 * Most of h
 * 
 * @author Tobias Stummer
 *
 */
@Log
@Service
public class NotifyService {

	private static final String ERROR_COLLECTING_EMAIL_ADDRESSES = "Error collecting email addresses";

	@Lazy
	@Autowired	
	private MailService mailService;

	@Autowired
	private MailUtil mailUtil;

	@Autowired
	private UsersService usersService;

	@Autowired
	private StatementRepository statementRepository;
	
	@Lazy
	@Autowired
	private ReplacementStringsService replacementStringsService;

	@Autowired
	private VwStatementReqdepartmentUsersRepository vwStatementReqDepartmentUsersRepository;

	@Value("${statement.compile.dateFormatPattern}")
	private String dateFormatPattern;

	public void notifyNewStatementMailInboxNotification() {
		try {
			log.info("New mail(s) in mailbox");
			Set<String> mailAddresses = getMailAddressesOfUsersWithRole(UserRoles.SPA_OFFICIAL_IN_CHARGE);
			NewMailContext mailContext = new NewMailContext();
			MailConfiguration mailConfig = mailUtil.getMailConfiguration();
			mailUtil.filterRecipients(mailAddresses, mailConfig);
			MessageConfiguration newStatementConfig = mailConfig.getNotificationInboxNewMail();
			mailContext.setMailText(newStatementConfig.getBody());
			mailContext.setRecipients(mailAddresses);
			mailContext.setSubject(newStatementConfig.getSubject());
			mailContext.setBccRecipients(newStatementConfig.getBccEmailAddresses());
			mailService.sendNotifyMail(mailContext);
		} catch (InternalErrorServiceException e) {
			log.warning(ERROR_COLLECTING_EMAIL_ADDRESSES);
		}
	}

	private Set<String> getMailAddressesOfUsersWithRole(String userRole) throws InternalErrorServiceException {
		List<UserModel> users = usersService.getUsersWithRole(userRole);
		Set<String> mailAddresses = new HashSet<>();
		for (UserModel user : users) {
			String emailAddress = user.getEmailAddress();
			if (emailAddress != null && !emailAddress.isEmpty()) {
				mailAddresses.add(emailAddress);
			}
		}
		return mailAddresses;
	}

	private Map<String, String> getStatementReplacementStrings(long statementId) {
		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		return oStatement.isPresent() ? replacementStringsService.getAllReplacements(oStatement.get())
				: new HashMap<>();
	}

	public void notifyApproved(Long statementId, Object value) {
		log.info("Notify approved");
		if (!(value instanceof Boolean)) {
			log.warning("Notify approved with non boolean value");
			return;
		}
		Map<String, String> replacementStrings = getStatementReplacementStrings(statementId);
		try {
			Set<String> mailAddresses = getMailAddressesOfUsersWithRole(UserRoles.SPA_OFFICIAL_IN_CHARGE);
			NewMailContext mailContext = new NewMailContext();
			MailConfiguration mailConfig = mailUtil.getMailConfiguration();
			mailUtil.filterRecipients(mailAddresses, mailConfig);
			mailContext.setRecipients(mailAddresses);
			if (Boolean.TRUE.equals(value)) {
				MessageConfiguration notificationApprovedAndSent = mailConfig.getNotificationApprovedAndSent();
				mailContext.setSubject(notificationApprovedAndSent.getSubject());
				mailContext.setMailText(notificationApprovedAndSent.getBody());
			} else {
				MessageConfiguration notificationApprovedAndNotSent = mailConfig.getNotificationApprovedAndNotSent();
				mailContext.setMailText(notificationApprovedAndNotSent.getBody());
				mailContext.setSubject(notificationApprovedAndNotSent.getSubject());
			}
			mailContext.setMailText(replaceVariables(mailContext.getMailText(), replacementStrings));
			mailContext.setSubject(replaceVariables(mailContext.getSubject(), replacementStrings));
			mailService.sendNotifyMail(mailContext);
		} catch (InternalErrorServiceException e) {
			log.warning(ERROR_COLLECTING_EMAIL_ADDRESSES);
		}
	}

	public void notifyNotApproved(Long statementId) {
		Map<String, String> replacementStrings = getStatementReplacementStrings(statementId);
		try {
			Set<String> mailAddresses = getMailAddressesOfUsersWithRole(UserRoles.SPA_OFFICIAL_IN_CHARGE);
			NewMailContext mailContext = new NewMailContext();
			MailConfiguration mailConfig = mailUtil.getMailConfiguration();
			mailUtil.filterRecipients(mailAddresses, mailConfig);
			mailContext.setRecipients(mailAddresses);
			MessageConfiguration notificationNotApproved = mailConfig.getNotificationNotApproved();
			mailContext.setSubject(notificationNotApproved.getSubject());
			mailContext.setMailText(notificationNotApproved.getBody());

			mailContext.setMailText(replaceVariables(mailContext.getMailText(), replacementStrings));
			mailContext.setSubject(replaceVariables(mailContext.getSubject(), replacementStrings));

			mailService.sendNotifyMail(mailContext);
		} catch (InternalErrorServiceException e) {
			log.warning(ERROR_COLLECTING_EMAIL_ADDRESSES);
		}
	}

	private String replaceDepartmentName(String text, String departmentName) {
		return text.replace(MailConfiguration.DEPARTMENT_NAME, "" + departmentName);
	}

	private String replaceVariables(String text, Map<String, String> replacementStrings) {
		for (Map.Entry<String, String> replace : replacementStrings.entrySet()) {
			if (replace.getValue() == null) {
				text = text.replace("<" + replace.getKey() + ">", "");
			} else {
				text = text.replace("<" + replace.getKey() + ">", replace.getValue());
			}
		}
		return text;
	}

	private void notifyDepartments(Long statementId, MailConfiguration mailConfig, MessageConfiguration msgConf,
			MessageConfiguration msgConfStandIn) {
		Map<DepartmentRelation, Set<String>> departmentMailadresses = getMailAdressesOfNotYetContributedDivisionMembersOf(
				statementId);
		Map<String, String> replacementStrings = getStatementReplacementStrings(statementId);

		msgConfStandIn = msgConfStandIn == null ? msgConf : msgConfStandIn;
		String baseSubject = replaceVariables(msgConf.getSubject(), replacementStrings);
		String baseBody = replaceVariables(msgConf.getBody(), replacementStrings);

		String baseSubjectStandIn = replaceVariables(msgConfStandIn.getSubject(), replacementStrings);
		String baseBodyStandIn = replaceVariables(msgConfStandIn.getBody(), replacementStrings);

		for (Entry<DepartmentRelation, Set<String>> entry : departmentMailadresses.entrySet()) {
			String subject = entry.getKey().isStandIn() ? baseSubjectStandIn : baseSubject;
			String body = entry.getKey().isStandIn() ? baseBodyStandIn : baseBody;
			String departmentName = entry.getKey().getDepartmentName();

			subject = replaceDepartmentName(subject, departmentName);
			body = replaceDepartmentName(body, departmentName);

			NewMailContext mailContext = new NewMailContext();
			Set<String> mailAddresses = entry.getValue();
			mailUtil.filterRecipients(mailAddresses, mailConfig);
			mailContext.setMailText(body);
			mailContext.setRecipients(mailAddresses);
			mailContext.setSubject(subject);
			mailContext.setBccRecipients(entry.getKey().isStandIn() ? msgConfStandIn.getBccEmailAddresses()
					: msgConf.getBccEmailAddresses());
			mailService.sendNotifyMail(mailContext);
		}
	}

	public void notifyEnrichDraft(Long statementId) {
		try {
			MailConfiguration mailConfig = mailUtil.getMailConfiguration();
			notifyDepartments(statementId, mailConfig, mailConfig.getNotificationEnrichDraft(),
					mailConfig.getNotificationEnrichDraftStandIn());
		} catch (InternalErrorServiceException e) {
			log.warning(ERROR_COLLECTING_EMAIL_ADDRESSES);
		}
	}

	private Map<DepartmentRelation, Set<String>> getMailAdressesOfNotYetContributedDivisionMembersOf(Long statementId) {
		Map<DepartmentRelation, Set<String>> departmentAdresses = new HashMap<>();

		List<VwStatementReqdepartmentUsers> users = vwStatementReqDepartmentUsersRepository
				.findByStatementId(statementId);
		for (VwStatementReqdepartmentUsers user : users) {
			if (user.getDepartmentContributed() == null || Boolean.FALSE.equals(user.getDepartmentContributed())) {
				// add
				String department = user.getDepartmentName();
				DepartmentRelation depRel = new DepartmentRelation(department, user.getStandIn());
				departmentAdresses.computeIfAbsent(depRel, k -> new HashSet<>()).add(user.getEmailAddress());
			}
		}
		return departmentAdresses;
	}

	public void notifyDepartmentsDueDate(Long statementId) {
		try {
			MailConfiguration mailConfig = mailUtil.getMailConfiguration();
			notifyDepartments(statementId, mailConfig, mailConfig.getNotificationDueDepartmentContributions(),
					mailConfig.getNotificationDueDepartmentContributionsStandIn());
		} catch (InternalErrorServiceException e) {
			log.warning(ERROR_COLLECTING_EMAIL_ADDRESSES);
		}
	}

	public void notifyApprovalRequired(Long statementId) {
		Map<String, String> replacementStrings = getStatementReplacementStrings(statementId);
		try {
			Set<String> mailAddresses = getMailAddressesOfUsersWithRole(UserRoles.SPA_APPROVER);
			NewMailContext mailContext = new NewMailContext();

			MailConfiguration mailConfig = mailUtil.getMailConfiguration();
			mailUtil.filterRecipients(mailAddresses, mailConfig);
			MessageConfiguration notifyApprovalRequired = mailConfig.getNotificationApprovalRequired();
			mailContext.setMailText(notifyApprovalRequired.getBody());

			mailContext.setRecipients(mailAddresses);
			mailContext.setSubject(notifyApprovalRequired.getSubject());
			mailContext.setBccRecipients(notifyApprovalRequired.getBccEmailAddresses());

			mailContext.setMailText(replaceVariables(mailContext.getMailText(), replacementStrings));
			mailContext.setSubject(replaceVariables(mailContext.getSubject(), replacementStrings));
			mailService.sendNotifyMail(mailContext);
		} catch (InternalErrorServiceException e) {
			log.warning(ERROR_COLLECTING_EMAIL_ADDRESSES);
		}
	}

	public void notifyOnAllMandatoryContributions(Long statementId) {
		Map<String, String> replacementStrings = getStatementReplacementStrings(statementId);
		try {
			Set<String> mailAddresses = getMailAddressesOfUsersWithRole(UserRoles.SPA_OFFICIAL_IN_CHARGE);
			NewMailContext mailContext = new NewMailContext();

			MailConfiguration mailConfig = mailUtil.getMailConfiguration();
			mailUtil.filterRecipients(mailAddresses, mailConfig);
			MessageConfiguration notifyAllMandatoryContributions = mailConfig
					.getNotificationAllMandatoryContributions();
			mailContext.setMailText(notifyAllMandatoryContributions.getBody());
			mailContext.setRecipients(mailAddresses);
			mailContext.setSubject(notifyAllMandatoryContributions.getSubject());
			mailContext.setBccRecipients(notifyAllMandatoryContributions.getBccEmailAddresses());
			mailContext.setMailText(replaceVariables(mailContext.getMailText(), replacementStrings));
			mailContext.setSubject(replaceVariables(mailContext.getSubject(), replacementStrings));
			mailService.sendNotifyMail(mailContext);
		} catch (InternalErrorServiceException e) {
			log.warning(ERROR_COLLECTING_EMAIL_ADDRESSES);
		}
	}

	private class DepartmentRelation {

		private String departmentName;
		private boolean standIn;

		public DepartmentRelation(String departmentName, boolean standIn) {
			this.departmentName = departmentName;
			this.standIn = standIn;
		}

		public String getDepartmentName() {
			return this.departmentName;
		}

		public boolean isStandIn() {
			return this.standIn;
		}

	}

}
