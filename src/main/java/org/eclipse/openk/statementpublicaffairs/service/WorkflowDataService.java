/*
 *******************************************************************************
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.model.DepartmentKey;
import org.eclipse.openk.statementpublicaffairs.model.TaskInfo;
import org.eclipse.openk.statementpublicaffairs.model.conf.AuthorizationRuleActions;
import org.eclipse.openk.statementpublicaffairs.model.conf.Rule;
import org.eclipse.openk.statementpublicaffairs.model.db.TblComment;
import org.eclipse.openk.statementpublicaffairs.model.db.TblDepartment;
import org.eclipse.openk.statementpublicaffairs.model.db.TblReqdepartment;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement2parent;
import org.eclipse.openk.statementpublicaffairs.model.db.TblTextblockdefinition;
import org.eclipse.openk.statementpublicaffairs.model.db.TblUser;
import org.eclipse.openk.statementpublicaffairs.model.db.TblWorkflowdata;
import org.eclipse.openk.statementpublicaffairs.model.db.VwStatementReqdepartmentUsers;
import org.eclipse.openk.statementpublicaffairs.repository.CommentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.DepartmentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.DepartmentstructureRepository;
import org.eclipse.openk.statementpublicaffairs.repository.ReqDepartmentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.Statement2ParentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementRepository;
import org.eclipse.openk.statementpublicaffairs.repository.TextblockdefinitionRepository;
import org.eclipse.openk.statementpublicaffairs.repository.UserRepository;
import org.eclipse.openk.statementpublicaffairs.repository.VwStatementReqdepartmentUsersRepository;
import org.eclipse.openk.statementpublicaffairs.repository.WorkflowDataRepository;
import org.eclipse.openk.statementpublicaffairs.util.Time;
import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;
import org.eclipse.openk.statementpublicaffairs.viewmodel.CommentModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.DistrictDepartmentsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementDepartmentsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.WorkflowDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class WorkflowDataService {

	@Autowired
	private StatementAuthorizationService authorizationService;

	@Autowired
	private StatementProcessService statementProcessService;

	@Autowired
	private UserInfoService userInfoService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private CommentRepository commentRepository;

	@Autowired
	private DepartmentRepository departmentRepository;

	@Autowired
	private VwStatementReqdepartmentUsersRepository statementReqdepartmentUsersRepository;

	@Autowired
	private DepartmentstructureRepository departmentstructureRepository;

	@Autowired
	private WorkflowDataRepository workflowDataRepository;

	@Autowired
	private ReqDepartmentRepository reqDepartmentRepository;

	@Autowired
	private TextblockdefinitionRepository textblockdefinitionRepository;

	@Autowired
	private StatementRepository statementRepository;

	@Autowired
	private Statement2ParentRepository statement2ParentRepository;

	@Autowired
	private NotifyService notificationService;

	public Optional<List<CommentModel>> getComments(Long statementId) throws ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isEmpty()) {
			return Optional.empty();
		}

		TblStatement statement = oStatement.get();
		TblWorkflowdata tWorkflowdata = statement.getWorkflowdata();
		List<CommentModel> comments = new ArrayList<>();
		if (tWorkflowdata == null) {
			return Optional.of(comments);
		}
		List<TblComment> tComments = tWorkflowdata.getComments();

		if (tComments != null) {
			for (TblComment tComment : tComments) {
				if (Boolean.TRUE.equals(tComment.getDisabled())) {
					continue;
				}
				CommentModel comment = new CommentModel();
				comment.setDisabled(tComment.getDisabled());
				comment.setText(tComment.getText());
				comment.setUsername(tComment.getUser().getUsername());
				comment.setFirstName(tComment.getUser().getFirstName());
				comment.setLastName(tComment.getUser().getLastName());
				comment.setId(tComment.getId());
				comment.setEditable(userInfoService.isOwnUserName(tComment.getUser().getUsername()));
				TypeConversion.iso8601InstantStringOfLocalDateTimeUTC(tComment.getTimestamp())
						.ifPresent(comment::setTimestamp);
				comments.add(comment);
			}
		}
		comments.sort(Comparator.comparing(CommentModel::getId));
		return Optional.of(comments);
	}

	protected TblUser getOrCreateUser(String username) throws InternalErrorServiceException {

		List<TblUser> tUsers = userRepository.findByUsername(username);
		TblUser tUser;
		if (tUsers.isEmpty()) {
			tUser = new TblUser();
			tUser.setUsername(username);
			userRepository.save(tUser);
		} else if (tUsers.size() > 1) {
			throw new InternalErrorServiceException("Could not get or create user");
		} else {
			tUser = tUsers.get(0);
		}
		return tUser;
	}

	public void addComment(Long statementId, String commentText)
			throws NotFoundServiceException, InternalErrorServiceException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_CREATE_COMMENT);
		Optional<TblWorkflowdata> wfd = getOrCreateWorkflowdata(statementId);
		if (wfd.isEmpty()) {
			throw new NotFoundServiceException("Could not get workflowdata for statement.");
		}
		TblComment comment = new TblComment();
		comment.setText(commentText);

		TblUser user = getOrCreateUser(userInfoService.getUserName());
		comment.setUser(user);
		comment.setDisabled(false);
		comment.setTimestamp(Time.currentTimeLocalTimeUTC());
		comment.setWorkflowdata(wfd.get());
		commentRepository.save(comment);
	}

	public void updateComment(Long statementId, Long commentId, String commentText)
			throws InternalErrorServiceException, NotFoundServiceException, ForbiddenServiceException {
		Optional<TblComment> oComment = getStatementCommentWithCommentId(statementId, commentId);
		if (oComment.isPresent()) {
			TblComment comment = oComment.get();
			String constraint = Boolean.TRUE.equals(userInfoService.isOwnUserName(comment.getUser().getUsername()))
					? AuthorizationRuleActions.C_IS_OWN_COMMENT
					: Rule.ANY;
			authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_CREATE_COMMENT, constraint);
			comment.setText(commentText);
			comment.setTimestamp(Time.currentTimeLocalTimeUTC());
			commentRepository.save(comment);
		} else {
			throw new NotFoundServiceException("Could not find comment for id");
		}
	}

	private Optional<TblComment> getStatementCommentWithCommentId(Long statementId, Long commentId)
			throws NotFoundServiceException, InternalErrorServiceException {
		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isEmpty()) {
			throw new NotFoundServiceException("Could not find statement or workflow.");
		}

		Optional<TblWorkflowdata> oWfd = getOrCreateWorkflowdata(statementId);
		if (oWfd.isEmpty()) {
			throw new NotFoundServiceException("Could not find statement or workflow.");
		}
		TblWorkflowdata wfd = oWfd.get();

		return wfd.getComments().stream().filter(c -> commentId.equals(c.getId())).findAny();
	}

	public void disableComment(Long statementId, Long commentId)
			throws InternalErrorServiceException, NotFoundServiceException, ForbiddenServiceException {
		Optional<TblComment> oComment = getStatementCommentWithCommentId(statementId, commentId);
		if (oComment.isPresent()) {
			TblComment comment = oComment.get();
			String constraint = Boolean.TRUE.equals(userInfoService.isOwnUserName(comment.getUser().getUsername()))
					? AuthorizationRuleActions.C_IS_OWN_COMMENT
					: Rule.ANY;
			authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_DELETE_COMMENT, constraint);
			comment.setDisabled(true);
			commentRepository.save(comment);
		} else {
			throw new NotFoundServiceException("Could not find comment for id");
		}
	}

	private void setRequiredDepartments(List<TblReqdepartment> requiredDepartments, WorkflowDataModel workflowData) {
		if (requiredDepartments == null) {
			return;
		}
		Map<String, Set<String>> manDeps = new HashMap<>();
		Map<String, Set<String>> optDeps = new HashMap<>();
		for (TblReqdepartment trd : requiredDepartments) {
			String group = trd.getDepartment().getDepartmentgroup();
			String name = trd.getDepartment().getName();
			if (trd.getOptional() != null && trd.getOptional()) {
				optDeps.computeIfAbsent(group, k -> new HashSet<>()).add(name);
			} else {
				manDeps.computeIfAbsent(group, k -> new HashSet<>()).add(name);
			}
		}
		workflowData.setMandatoryDepartments(manDeps);
		workflowData.setOptionalDepartments(optDeps);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Optional<WorkflowDataModel> getWorkflowData(Long statementId) throws ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isEmpty()) {
			return Optional.empty();
		}

		TblStatement statement = oStatement.get();
		TblWorkflowdata tWorkflowdata = statement.getWorkflowdata();
		if (tWorkflowdata == null) {
			return Optional.of(new WorkflowDataModel());
		}

		WorkflowDataModel workflowData = new WorkflowDataModel();
		workflowData.setGeoPosition(tWorkflowdata.getPosition());

		if (Boolean.FALSE.equals(tWorkflowdata.getInitialState())) {
			setRequiredDepartments(tWorkflowdata.getRequiredDepartments(), workflowData);
		}
		return Optional.of(workflowData);
	}

	protected Optional<TblWorkflowdata> getOrCreateWorkflowdata(Long statementId) throws InternalErrorServiceException {
		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isEmpty()) {
			return Optional.empty();
		}

		TblStatement statement = oStatement.get();
		TblWorkflowdata tWorkflowdata = statement.getWorkflowdata();
		if (tWorkflowdata != null) {
			return Optional.of(tWorkflowdata);
		}
		List<TblTextblockdefinition> defs = textblockdefinitionRepository.getLatestTextblockdefinition();
		if (defs.isEmpty()) {
			throw new InternalErrorServiceException("Environment not set up properly. No Textblockdefinitionavailable");
		}
		TblWorkflowdata wfData = new TblWorkflowdata();
		wfData.setTextBlockDefinition(defs.get(0));
		wfData.setStatement(statement);
		wfData.setInitialState(true);
		workflowDataRepository.save(wfData);
		return Optional.of(wfData);
	}

	private Map<String, Set<String>> parseAllDepartments(Map<String, DistrictDepartmentsModel> ddm) {
		Map<String, Set<String>> all = new HashMap<>();
		for (DistrictDepartmentsModel districts : ddm.values()) {
			for (Entry<String, Set<String>> entrySet : districts.getDepartments().entrySet()) {
				all.computeIfAbsent(entrySet.getKey(), k -> new HashSet<>()).addAll(entrySet.getValue());
			}
		}
		return all;
	}

	private Map<String, Set<String>> parseSuggestedDepartments(Map<String, DistrictDepartmentsModel> ddm, String city,
			String district) {
		String key = city + "#" + district;
		if (ddm.containsKey(key)) {
			return ddm.get(key).getDepartments();
		}
		return new HashMap<>();
	}

	public Optional<StatementDepartmentsModel> getDepartments(Long statementId) throws ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isEmpty()) {
			return Optional.empty();
		}
		TblStatement statement = oStatement.get();
		Map<String, DistrictDepartmentsModel> ddm = new HashMap<>();
		departmentstructureRepository.findById(statement.getDepartmentStructureId())
				.ifPresent(ds -> ddm.putAll(ds.getDefinition()));

		StatementDepartmentsModel sdm = new StatementDepartmentsModel();
		sdm.setAllDepartments(parseAllDepartments(ddm));
		sdm.setSuggestedDepartments(parseSuggestedDepartments(ddm, statement.getCity(), statement.getDistrict()));
		return Optional.of(sdm);
	}

	private TblDepartment getDepartment(String group, String name) throws InternalErrorServiceException {
		List<TblDepartment> departments = departmentRepository.getDepartmentsForGroupAndName(group, name);

		if (departments.isEmpty()) {
			throw new InternalErrorServiceException("Could not find Department " + group + " - " + name);
		}
		return departments.get(0);

	}

	public void setDepartments(Long statementId, String taskId, TblWorkflowdata wfd,
			WorkflowDataModel workflowDataModel)
			throws InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException {
		Optional<TaskInfo> ti = statementProcessService.getTaskInfo(statementId, taskId);
		if (ti.isPresent()) {
			authorizationService.authorizeByClaimedUser(ti.get().getAssignee());
			authorizationService.authorize(ti.get().getTaskDefinitionKey(),
					AuthorizationRuleActions.A_SET_REQ_DEPARTMENTS);
		}

		TblStatement statement = wfd.getStatement();
		wfd.setInitialState(false);

		Map<String, DistrictDepartmentsModel> ddm = new HashMap<>();
		departmentstructureRepository.findById(statement.getDepartmentStructureId())
				.ifPresent(ds -> ddm.putAll(ds.getDefinition()));

		Map<String, Set<String>> all = parseAllDepartments(ddm);

		Map<DepartmentKey, Boolean> requiredDepartmenst = new HashMap<>();
		for (Entry<String, Set<String>> entry : workflowDataModel.getOptionalDepartments().entrySet()) {
			String group = entry.getKey();
			for (String department : entry.getValue()) {
				DepartmentKey key = DepartmentKey.of(group, department);
				requiredDepartmenst.put(key, true);
			}
		}

		for (Entry<String, Set<String>> entry : workflowDataModel.getMandatoryDepartments().entrySet()) {
			String group = entry.getKey();
			for (String department : entry.getValue()) {
				DepartmentKey key = DepartmentKey.of(group, department);
				requiredDepartmenst.put(key, false);
			}
		}

		verifyRequiredDepartements(requiredDepartmenst.keySet(), all);

		List<TblReqdepartment> currentReq = wfd.getRequiredDepartments();

		currentReq = deleteNoLongerRequiredDepartments(currentReq, requiredDepartmenst.keySet());
		filterAlreadyExistingRequiredDepartments(requiredDepartmenst, currentReq);
		createNewRequiredDepartments(wfd, requiredDepartmenst);
	}

	private void createNewRequiredDepartments(TblWorkflowdata wfd, Map<DepartmentKey, Boolean> requiredDepartments)
			throws InternalErrorServiceException {
		// create new
		for (Entry<DepartmentKey, Boolean> entry : requiredDepartments.entrySet()) {
			TblDepartment d = getDepartment(entry.getKey().getGroup(), entry.getKey().getDepartment());
			if (d == null) {
				continue;
			}
			TblReqdepartment n = new TblReqdepartment();
			n.setWorkflowdata(wfd);
			n.setDepartment(d);
			n.setOptional(entry.getValue());
			n.setContributed(false);
			reqDepartmentRepository.save(n);
		}
	}

	private void filterAlreadyExistingRequiredDepartments(Map<DepartmentKey, Boolean> requiredDepartments,
			List<TblReqdepartment> oldReq) {
		// remove already existing
		for (TblReqdepartment r : oldReq) {
			String group = r.getDepartment().getDepartmentgroup();
			String name = r.getDepartment().getName();
			DepartmentKey k = DepartmentKey.of(group, name);

			if (requiredDepartments.containsKey(k)) {
				r.setOptional(Boolean.TRUE.equals(requiredDepartments.get(k)));
				reqDepartmentRepository.save(r);
				requiredDepartments.remove(k);
			}
		}
	}

	private List<TblReqdepartment> deleteNoLongerRequiredDepartments(List<TblReqdepartment> existing,
			Set<DepartmentKey> requiredDepartments) {
		// delete no longer required
		List<TblReqdepartment> oldReq = new ArrayList<>();
		if (existing != null) {
			oldReq.addAll(existing);
		}
		List<TblReqdepartment> toRemove = new ArrayList<>();
		for (TblReqdepartment r : oldReq) {
			TblDepartment t = r.getDepartment();
			DepartmentKey k = DepartmentKey.of(t.getDepartmentgroup(), t.getName());
			if (!requiredDepartments.contains(k)) {
				reqDepartmentRepository.delete(r);
				toRemove.add(r);
			}
		}
		oldReq.removeAll(toRemove);
		return oldReq;
	}

	private void verifyRequiredDepartements(Set<DepartmentKey> requiredDepartments, Map<String, Set<String>> all)
			throws BadRequestServiceException {
		// filter for invalid parameter
		for (DepartmentKey depKey : requiredDepartments) {
			if (!all.containsKey(depKey.getGroup()) || !all.get(depKey.getGroup()).contains(depKey.getDepartment())) {
				throw new BadRequestServiceException("Required department " + depKey.getDepartment()
						+ " does not exist in group " + depKey.getGroup());
			}
		}
	}

	@Transactional
	public void setWorkflowData(Long statementId, String taskId, WorkflowDataModel workflowDataModel)
			throws ForbiddenServiceException, InternalErrorServiceException, NotFoundServiceException,
			BadRequestServiceException {
		Optional<TaskInfo> ti = statementProcessService.getTaskInfo(statementId, taskId);
		if (ti.isPresent()) {
			authorizationService.authorizeByClaimedUser(ti.get().getAssignee());
			authorizationService.authorize(ti.get().getTaskDefinitionKey(),
					AuthorizationRuleActions.A_SET_WORKFLOW_DATA);
		}
		if (workflowDataModel == null) {
			throw new BadRequestServiceException();
		}

		Optional<TblWorkflowdata> oWfd = getOrCreateWorkflowdata(statementId);
		if (oWfd.isEmpty()) {
			throw new NotFoundServiceException("Could not get workflowdata");
		}
		TblWorkflowdata wfd = oWfd.get();
		setPosition(workflowDataModel, wfd);
		wfd = workflowDataRepository.save(wfd);
		setDepartments(statementId, taskId, wfd, workflowDataModel);
		statementProcessService.touchNewUserOperation(statementId, taskId);
	}

	public void setPosition(WorkflowDataModel workflowDataModel, TblWorkflowdata wfd) {
		wfd.setPosition(workflowDataModel.getGeoPosition());
	}

	public Optional<List<Long>> getStatementParentIds(Long statementId) throws ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isEmpty()) {
			return Optional.empty();
		}
		List<TblStatement2parent> s2pL = statement2ParentRepository.searchByStatementId(statementId);
		Set<Long> parentIds = new HashSet<>();
		for (TblStatement2parent s2p : s2pL) {
			parentIds.add(s2p.getParent_id());
		}
		return Optional.of(new ArrayList<Long>(parentIds));
	}

	public Optional<List<Long>> getStatementChildrenIds(Long statementId) throws ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isEmpty()) {
			return Optional.empty();
		}
		List<TblStatement2parent> s2pL = statement2ParentRepository.searchByParentId(statementId);
		Set<Long> childrenIds = new HashSet<>();
		for (TblStatement2parent s2p : s2pL) {
			childrenIds.add(s2p.getStatement_id());
		}
		return Optional.of(new ArrayList<Long>(childrenIds));
	}

	public void setStatementParents(Long statementId, String taskId, Set<Long> parentIds)
			throws NotFoundServiceException, ForbiddenServiceException, InternalErrorServiceException {
		Optional<TaskInfo> ti = statementProcessService.getTaskInfo(statementId, taskId);
		if (ti.isPresent()) {
			authorizationService.authorizeByClaimedUser(ti.get().getAssignee());
			authorizationService.authorize(ti.get().getTaskDefinitionKey(),
					AuthorizationRuleActions.A_SET_PARENT_STATEMENTS);
		}

		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (oStatement.isEmpty()) {
			throw new NotFoundServiceException("Statement with Id could not be found");
		}

		List<TblStatement2parent> s2pL = statement2ParentRepository.searchByStatementId(statementId);

		List<TblStatement> reqParentIds = statementRepository.findByIdIn(parentIds);

		TblStatement statement = oStatement.get();

		// clean no longer
		List<TblStatement2parent> s2pDeleteL = new ArrayList<>();
		for (TblStatement2parent s2p : s2pL) {
			Long s2pPId = s2p.getParent_id();
			if (!parentIds.contains(s2pPId)) {
				s2pDeleteL.add(s2p);
			}
		}

		statement2ParentRepository.deleteAll(s2pDeleteL);

		// add new
		List<TblStatement2parent> s2pCreateL = new ArrayList<>();
		for (TblStatement parentStatement : reqParentIds) {
			Long parentId = parentStatement.getId();
			if (statementId.equals(parentId)) {
				// no self referencing
				continue;
			}
			boolean found = false;
			for (TblStatement2parent s2p : s2pL) {
				if (parentId.equals(s2p.getParent_id())) {
					found = true;
					break;
				}
			}
			if (!found) {
				TblStatement2parent ns2p = new TblStatement2parent();
				ns2p.setStatement_id(statement.getId());
				ns2p.setParent_id(parentStatement.getId());
				s2pCreateL.add(ns2p);
			}
		}
		statement2ParentRepository.saveAll(s2pCreateL);
		statementProcessService.touchNewUserOperation(statementId, taskId);
	}

	public Optional<Map<String, Set<String>>> getDepartmentContributions(Long statementId)
			throws InternalErrorServiceException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		Optional<TblWorkflowdata> oWfd = getOrCreateWorkflowdata(statementId);
		if (oWfd.isEmpty()) {
			return Optional.empty();
		}
		TblWorkflowdata wfd = oWfd.get();
		Map<String, Set<String>> contributions = new HashMap<>();
		for (TblReqdepartment reqdep : wfd.getRequiredDepartments()) {
			Boolean contributed = reqdep.getContributed();
			if (contributed != null && contributed) {
				String department = reqdep.getDepartment().getName();
				String group = reqdep.getDepartment().getDepartmentgroup();
				contributions.computeIfAbsent(group, k -> new HashSet<>()).add(department);
			}
		}
		return Optional.of(contributions);
	}

	public void setDepartmentContributions(Long statementId, String taskId, Map<String, Set<String>> contributed)
			throws InternalErrorServiceException, ForbiddenServiceException, NotFoundServiceException {
		Optional<TaskInfo> ti = statementProcessService.getTaskInfo(statementId, taskId);
		if (ti.isPresent()) {
			authorizationService.authorizeByClaimedUser(ti.get().getAssignee());
			authorizationService.authorize(ti.get().getTaskDefinitionKey(),
					AuthorizationRuleActions.A_SET_DEPARTMENT_CONTRIBUTIONS);
		}
		Optional<TblWorkflowdata> oWfd = getOrCreateWorkflowdata(statementId);
		if (oWfd.isEmpty()) {
			throw new NotFoundServiceException("Could not get WorkflowData");
		}
		TblWorkflowdata wfd = oWfd.get();

		for (TblReqdepartment rDep : wfd.getRequiredDepartments()) {
			String group = rDep.getDepartment().getDepartmentgroup();
			String department = rDep.getDepartment().getName();
			rDep.setContributed(contributed.containsKey(group) && contributed.get(group).contains(department));
			reqDepartmentRepository.save(rDep);
		}
		statementProcessService.touchNewUserOperation(statementId, taskId);

	}

	private boolean nullOrFalse(Boolean bool) {
		return bool == null || !bool;
	}

	// set contributions of own user for statement and task
	public void setDepartmentUserContribute(Long statementId, String taskId) throws InternalErrorServiceException,
			ForbiddenServiceException, NotFoundServiceException, BadRequestServiceException {

		// is allowed to set contribution
		Optional<TaskInfo> ti = statementProcessService.getTaskInfo(statementId, taskId);
		if (ti.isPresent()) {
			authorizationService.authorizeByClaimedUser(ti.get().getAssignee());
			authorizationService.authorize(ti.get().getTaskDefinitionKey(),
					AuthorizationRuleActions.A_USER_CONTRIBUTED);
		}

		// for all own user departments set contribution for reqdepartments
		Optional<TblWorkflowdata> oWfd = getOrCreateWorkflowdata(statementId);
		if (oWfd.isEmpty()) {
			throw new NotFoundServiceException("Could not get WorkflowData");
		}

		// get user department
		List<VwStatementReqdepartmentUsers> ownReqDepartments = statementReqdepartmentUsersRepository
				.findByStatementIdAndUserName(statementId, userInfoService.getUserName());

		if (ownReqDepartments.isEmpty()) {
			throw new BadRequestServiceException("User not responsible for a required department");
		}

		Set<DepartmentKey> userDepartments = new HashSet<>();

		for (VwStatementReqdepartmentUsers statementReqDepartmentUser : ownReqDepartments) {
			userDepartments.add(DepartmentKey.of(statementReqDepartmentUser.getDepartmentGroup(),
					statementReqDepartmentUser.getDepartmentName()));
		}

		TblWorkflowdata wfd = oWfd.get();
		if (addDepartmentContributions(userDepartments, wfd)) {
			notificationService.notifyOnAllMandatoryContributions(statementId);
		}
	}

	private boolean addDepartmentContributions(Set<DepartmentKey> userDepartments, TblWorkflowdata wfd) {
		boolean allMandatoryContributed = true;
		boolean contributionSetsMandatory = false;
		for (TblReqdepartment rDep : wfd.getRequiredDepartments()) {
			String group = rDep.getDepartment().getDepartmentgroup();
			String department = rDep.getDepartment().getName();
			DepartmentKey k = DepartmentKey.of(group, department);
			if (userDepartments.contains(k) && nullOrFalse(rDep.getContributed())) {
				rDep.setContributed(true);
				if (nullOrFalse(rDep.getOptional())) {
					contributionSetsMandatory = true;
				}
			}
			// is mandatory and not contributed
			if (nullOrFalse(rDep.getOptional()) && nullOrFalse(rDep.getContributed())) {
				allMandatoryContributed = false;
			}
			reqDepartmentRepository.save(rDep);
		}
		return contributionSetsMandatory && allMandatoryContributed;
	}

}
