/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.model.AttachmentFile;
import org.eclipse.openk.statementpublicaffairs.model.Tags;
import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;
import org.eclipse.openk.statementpublicaffairs.viewmodel.AttachmentModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementDetailsModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import lombok.extern.java.Log;

@Service
@Log
public class ArchiveService {

	@Autowired
	private StatementService statementService;

	@Value("${archive.basefolder}")
	private String archiveBaseFolderPath;

	@Value("${archive.statementArchiveFolderName}")
	private String archiveFolderNameTemplate;

	/**
	 * Archive statement to configured folder.
	 * 
	 * @param statementId statement identifier
	 * @param today 
	 * @throws InternalErrorServiceException in case an error occurred preparing the
	 *                                       destination folders or storing the
	 *                                       attachment files to the folder.
	 * @throws ForbiddenServiceException     in case user is not allowed to read the
	 *                                       attachments
	 * @throws NotFoundServiceException      in case the statement could not be
	 *                                       found.
	 */
	public void archiveStatement(Long statementId, LocalDate finishedDate)
			throws InternalErrorServiceException, ForbiddenServiceException, NotFoundServiceException {

		File baseFolder = new File(archiveBaseFolderPath);
		if (!baseFolder.isDirectory()) {
			createFolder(archiveBaseFolderPath);
		}
		Optional<StatementDetailsModel> oStatement = statementService.getStatement(statementId);
		if (oStatement.isEmpty()) {
			throw new NotFoundServiceException("Statement to archive could not be found - " + statementId);
		}
		Optional<String> oFinishedDateString = TypeConversion.dateStringOfLocalDate(finishedDate);
		if (oFinishedDateString.isEmpty()) {
			throw new InternalErrorServiceException("Archive statement: Could not convert finshedDate to String");
		}
		String finishedDateString = oFinishedDateString.get();
		String folderName = archiveFolderNameTemplate.replace("<t:id>", "" + statementId).replace("<t:finishedDate>",
				finishedDateString);
		String statementArchiveFolderPath = archiveBaseFolderPath + File.separator + folderName;
		createFolder(statementArchiveFolderPath);
		String infoDataFolderPath = statementArchiveFolderPath + File.separator + "info-data";
		createFolder(infoDataFolderPath);
		String sentDataFolderPath = statementArchiveFolderPath + File.separator + "sent-data";
		createFolder(sentDataFolderPath);

		Optional<List<AttachmentModel>> oStatementAttachments = statementService.getStatementAttachments(statementId);
		if (oStatementAttachments.isEmpty()) {
			throw new InternalErrorServiceException("Could not get statement attachments for statement " + statementId);
		}
		for (AttachmentModel attachment : oStatementAttachments.get()) {
			if (attachment.getTagIds().contains(Tags.OUTBOX.tagId())) {
				storeFile(sentDataFolderPath, statementId, attachment.getId());
			} else {
				storeFile(infoDataFolderPath, statementId, attachment.getId());
			}
		}
	}

	/**
	 * Helper to store attachment file to the baseFolder.
	 * 
	 * @param baseFolderPath folder path to store the file to
	 * @param statementId    statement identifier
	 * @param attachmentId   attachment identifier
	 * @throws ForbiddenServiceException     in case reading the attachment file is
	 *                                       not allowed for the user.
	 * @throws InternalErrorServiceException in case an error occurred when writing
	 *                                       the file to the destination.
	 */
	private void storeFile(String baseFolderPath, Long statementId, Long attachmentId)
			throws ForbiddenServiceException, InternalErrorServiceException {
		Optional<AttachmentFile> oAttachment = statementService.getStatementAttachmentFile(statementId, attachmentId);
		if (oAttachment.isEmpty()) {
			throw new InternalErrorServiceException("Could not read attachment file for attachmentId %d of statement %d".formatted(attachmentId, statementId));
		}
		AttachmentFile attachmentFile = oAttachment.get();
		String filePath = baseFolderPath + File.separator + attachmentFile.getName();
		File file = new File(filePath);
		InputStream ressource = attachmentFile.getRessource();
		try {

			byte[] buffer = new byte[ressource.available()];
			int count = ressource.read(buffer);
			try (OutputStream outStream = new FileOutputStream(file)) {
				outStream.write(buffer);
				log.info("Successfully archived attachment %d of statement %d with length %d".formatted(attachmentId, statementId, count));
			}
		} catch (IOException e) {
			throw new InternalErrorServiceException("Error writing attachment %d of statement %d to  file %s : %s".formatted(attachmentId, statementId, filePath, e.getMessage()));
		}
	}

	/**
	 * Helper for creating a folder.
	 * 
	 * @param folderPath relative or absolute path of the folder to create
	 * @throws InternalErrorServiceException in case an error occurred when creating
	 *                                       the folder.
	 */
	private void createFolder(String folderPath) throws InternalErrorServiceException {
		File folder = new File(folderPath);
		if (!folder.exists() && !folder.mkdirs()) {
			String errorMsg = "Could not create archive folder " + folderPath;
			log.severe(errorMsg);
			throw new InternalErrorServiceException(errorMsg);
		}
	}

}
