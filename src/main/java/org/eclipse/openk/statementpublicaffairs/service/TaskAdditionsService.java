/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.util.Map;

import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.TaskInfo;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaVariable;
import org.eclipse.openk.statementpublicaffairs.model.camunda.WorkflowConstants;
import org.eclipse.openk.statementpublicaffairs.viewmodel.MailSendReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import lombok.extern.java.Log;

@Log
@Service
public class TaskAdditionsService {

	@Autowired
	NotifyService notificationService;

	@Autowired
	MailService mailService;

	@Autowired
	StatementService statementService;
	
	@Autowired
	WorkflowDataService workflowDataService;

	public void onBeforeCompleteTask(Long statementId, TaskInfo taskInfo, Map<String, CamundaVariable> variablesMap) {

		if (WorkflowConstants.TASK_APPROVE_STATEMENT.contentEquals(taskInfo.getTaskDefinitionKey())
				&& Boolean.TRUE.equals(variablesMap.get(WorkflowConstants.VAR_APPROVED_STATEMENT).getValue())) {
			boolean mailSent = prepareAndSendStatementResponse(statementId);
			CamundaVariable sent = new CamundaVariable();
			sent.setType(WorkflowConstants.VARTYPE_BOOLEAN);
			sent.setValue(mailSent);
			variablesMap.put(WorkflowConstants.VAR_SENT, sent);
		}
	}

	protected boolean prepareAndSendStatementResponse(Long statementId) {
		MailSendReport report = statementService.dispatchStatementResponse(statementId);
		return report.getSuccessful();
	}

	public void onAfterCompleteTask(Long statementId, TaskInfo taskInfo, Map<String, CamundaVariable> variablesMap) throws InternalErrorServiceException {

		switch (taskInfo.getTaskDefinitionKey()) {
		case WorkflowConstants.TASK_CREATE_NEGATIVE_RESPONSE:
			if (Boolean.TRUE.equals(variablesMap.get(WorkflowConstants.VAR_RESPONSE_CREATED).getValue())) {
				notificationService.notifyApprovalRequired(statementId);
			}
			break;
		case WorkflowConstants.TASK_CHECK_AND_FORMULATE_RESPONSE:
			
			// approval required
			if (Boolean.TRUE.equals(variablesMap.get(WorkflowConstants.VAR_RESPONSE_CREATED).getValue())
					&& Boolean.TRUE.equals(variablesMap.get(WorkflowConstants.VAR_DATA_COMPLETE).getValue())) {
				notificationService.notifyApprovalRequired(statementId);
			}

			if (Boolean.FALSE.equals(variablesMap.get(WorkflowConstants.VAR_RESPONSE_CREATED).getValue())
					&& Boolean.FALSE.equals(variablesMap.get(WorkflowConstants.VAR_DATA_COMPLETE).getValue())) {
				notificationService.notifyEnrichDraft(statementId);
			}
			break;
		case  WorkflowConstants.TASK_CREATE_DRAFT:
			notificationService.notifyEnrichDraft(statementId);
			break;
		case WorkflowConstants.TASK_APPROVE_STATEMENT:
			if (Boolean.TRUE.equals(variablesMap.get(WorkflowConstants.VAR_APPROVED_STATEMENT).getValue())) {
				notificationService.notifyApproved(statementId, variablesMap.get(WorkflowConstants.VAR_SENT).getValue());
			} else {
				notificationService.notifyNotApproved(statementId);
			}
			break;
		case WorkflowConstants.TASK_ADD_BASIC_INFO_DATA:
			workflowDataService.getOrCreateWorkflowdata(statementId);
			break;
		default:
			log.warning("ON_AFTER_COMPLETE: DID NOT HIT CASE taskDefinitionKey: " + taskInfo.getTaskDefinitionKey());
		}
	}

}
