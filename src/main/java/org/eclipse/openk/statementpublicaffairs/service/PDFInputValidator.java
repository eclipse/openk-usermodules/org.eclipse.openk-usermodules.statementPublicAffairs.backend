/*
 *******************************************************************************
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.eclipse.openk.statementpublicaffairs.model.TextToken;
import org.springframework.stereotype.Service;

import lombok.extern.java.Log;

@Log
@Service
public class PDFInputValidator {

    private static final PDFont font = PDType1Font.HELVETICA;

    private final Map<String, Graphics2D> fontCache = new HashMap<>();


    public void validatePDFInput(List<List<TextToken>> textblockSets) {

        for (List<TextToken> subList : textblockSets) {

            for (TextToken token : subList) {

                String value = token.getValue();

                token.setValue(sanitizeString(value));
            }
        }
    }

    public String sanitizeString(String input) {

        if (input == null) {
            input = "";
        }

        input = input
                .replace('\u00AD', '-')
                .replace('\u00A0', ' ')
                .replace("\t", " ")
                .replace("  ", " ").trim();


        Font lfont = getFont(10).getFont();

        for (int i = 0; i < input.length(); i++) {

            char c = input.charAt(i);
            try {
				if (!lfont.canDisplay(c)) {
					input = input.replace(c, ' ');
				}
            } catch (Exception e) {
            	log.warning("Sanitize String threw exception " + e.getMessage() + " - "+ e);
				input = input.replace(c, ' ');
            }
        }

        return input;
    }

    private Graphics2D getFont(float fontSize) {

        Graphics2D g = fontCache.get(fontSize + "");

        if (g == null) {
            BufferedImage image = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);
            g = image.createGraphics();
            g.setColor(Color.black);
            g.setBackground(Color.white);
            g.setFont(new Font(font.getName(), Font.PLAIN, 10));
            g.setFont(g.getFont().deriveFont(fontSize));

            fontCache.put(fontSize + "", g);
        }

        return g;
    }
}
