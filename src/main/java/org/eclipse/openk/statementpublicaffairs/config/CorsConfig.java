/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import lombok.extern.java.Log;

@Log
@Configuration
public class CorsConfig {

	@Value("${cors.corsEnabled}")
	private boolean corsEnabled;

	@Value("${cors.corsAllowedOrigins:*}")
	private String corsAllowedOrigins;
	
	
	@Value("${cors.corsAllowedHeaders:*}")
	private String corsAllowedHeaders;

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				if (corsEnabled) {
					log.info("Cors enabled");
					registry.addMapping("/**").allowedMethods("GET", "POST", "PUT", "DELETE")
							.allowedOrigins(corsAllowedOrigins).allowedHeaders(corsAllowedHeaders);
				} else {
					log.info("Cors disabled");
				}
			}
		};
	}
}
