/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.statementpublicaffairs.config.auth;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.eclipse.openk.statementpublicaffairs.model.UserDetails;
import org.keycloak.TokenVerifier;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

	@Value("${jwt.useStaticJwt}")
	private boolean useStaticJwt;

	@Value("${jwt.tokenHeader}")
	private String tokenHeader;

	@Value("${jwt.staticJwt}")
	private String staticJwt;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {
		String authenticationHeader = useStaticJwt ? staticJwt : request.getHeader(this.tokenHeader);
		String paramToken = request.getParameter("accessToken");
		if (paramToken != null && !useStaticJwt) {
			authenticationHeader = paramToken;
		}

		try {
			SecurityContext context = SecurityContextHolder.getContext();

			if (authenticationHeader != null) {

				final String bearerTkn = authenticationHeader.replace("Bearer ", "");

				createToken(context, bearerTkn);

			}
			chain.doFilter(request, response);
		} catch (AuthenticationException ex) {
			throw new ServletException("Authentication exception.");
		}

	}

	private void createToken(SecurityContext context, String bearerTkn) throws ServletException {
		try {
			AccessToken token = TokenVerifier.create(bearerTkn,AccessToken.class).getToken();

			List<GrantedAuthority> authorities = new ArrayList<>();
			token.getRealmAccess().getRoles().stream()
					.forEach(x -> authorities.add(new SimpleGrantedAuthority("ROLE_" + x.toUpperCase())));

			UserDetails userDetails = new UserDetails();
			userDetails.setUserName(token.getPreferredUsername());
			userDetails.setName(token.getName());
			userDetails.setFirstName(token.getGivenName());
			userDetails.setLastName(token.getFamilyName());
			userDetails.setToken(bearerTkn);
			UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(userDetails, null,
					authorities);
			auth.setDetails(bearerTkn);

			context.setAuthentication(auth);

		} catch (Exception e) {
			throw new ServletException("Invalid token.");
		}
	}
	
}
