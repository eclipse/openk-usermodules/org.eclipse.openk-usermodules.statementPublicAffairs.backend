/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.config;

import static org.springframework.security.config.Customizer.withDefaults;


import org.eclipse.openk.statementpublicaffairs.config.auth.JwtAuthenticationEntryPoint;
import org.eclipse.openk.statementpublicaffairs.config.auth.JwtAuthenticationTokenFilter;
import org.eclipse.openk.statementpublicaffairs.config.auth.JwtTokenValidationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfTokenRequestAttributeHandler;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity(
		securedEnabled = true,
		jsr250Enabled = true)
public class SecurityConfig {

    @Autowired
    private JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter;

    @Autowired
    private JwtTokenValidationFilter jwtTokenValidationFilter;

    @Autowired
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

	@Bean
	SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		CsrfTokenRequestAttributeHandler requestHandler = new CsrfTokenRequestAttributeHandler();
	 
		http
				.authorizeHttpRequests(requests -> requests
						.requestMatchers("/**").permitAll()
						.anyRequest().authenticated())
				.cors(withDefaults())
				.exceptionHandling(handling -> handling.authenticationEntryPoint(jwtAuthenticationEntryPoint))
				.sessionManagement(management -> management.sessionCreationPolicy(SessionCreationPolicy.ALWAYS))
				.csrf(csrf -> csrf.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
				.csrfTokenRequestHandler(requestHandler))
				.logout(logout -> logout.disable())
				.formLogin(login -> login.disable())
				.anonymous(withDefaults())
				.addFilterAfter(jwtTokenValidationFilter, BasicAuthenticationFilter.class)
				.anonymous(withDefaults())
				.addFilterAfter(jwtAuthenticationTokenFilter, BasicAuthenticationFilter.class);
		return http.build();       
    }

}

