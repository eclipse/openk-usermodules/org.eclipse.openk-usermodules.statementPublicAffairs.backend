/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.controller;

import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.service.UserInfoService;
import org.eclipse.openk.statementpublicaffairs.viewmodel.UserInfoModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * REST controller that contains endpoints to extend the session timeout and
 * logout from the service.
 * 
 * @author Tobias Stummer
 *
 */
@RestController
public class UserInfoController {
    
    
    @Autowired
    private UserInfoService userInfoService;
    
    /**
     * Version GET endpoint provides a version model containing version info about
     * the openKONSEQUENZ module.
     * 
     * @return VersionModel containing the version info
     */
    @Secured({UserRoles.SPA_ACCESS})
    @GetMapping(value = "/userinfo")
    public UserInfoModel getUserInfo() {
        UserInfoModel userInfo = new UserInfoModel();
        userInfo.setFirstName(userInfoService.getFirstName());
        userInfo.setLastName(userInfoService.getLastName());
        userInfo.setRoles(userInfoService.getUserRoles());
        userInfo.setUserName(userInfoService.getUserName());
        return userInfo;
    }


}
