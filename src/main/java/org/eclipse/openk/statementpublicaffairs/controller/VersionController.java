/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.controller;

import org.eclipse.openk.statementpublicaffairs.service.VersionService;
import org.eclipse.openk.statementpublicaffairs.viewmodel.VersionModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller that contains the endpoints to provide the openKONSEQUENZ
 * module version info.
 * 
 * @author Tobias Stummer
 *
 */
@RestController
public class VersionController {

	/**
	 * Version service.
	 */
	@Autowired
	private VersionService versionService;

	/**
	 * Version GET endpoint provides a version model containing version info about
	 * the openKONSEQUENZ module.
	 * 
	 * @return VersionModel containing the version info
	 */
	@GetMapping(value = "/version")
	public VersionModel getVersion() {
		VersionModel version = new VersionModel();
		version.setBuildVersion(versionService.getBuildVersion());
		version.setApplicationName(versionService.getApplicationName());
		return version;
	}

}
