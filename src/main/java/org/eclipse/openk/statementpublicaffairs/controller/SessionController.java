/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.controller;

import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller that contains endpoints to extend the session timeout and
 * logout from the service.
 * 
 * @author Tobias Stummer
 *
 */
@RestController
public class SessionController {

    /**
     * Session service.
     */
    @Autowired
    private SessionService sessionService;

    /**
     * keepalive-session GET endpoint extends the session timeout.
     * 
     * @return HTTP.OK
     */
    @Secured({UserRoles.SPA_ACCESS})
    @GetMapping(value = "/keepalive-session")
    public ResponseEntity<Object> keepaliveSession() {
        // Session is implicitly extended due to role verification
        return ResponseEntity.status(204).build();

    }

    /**
     * Logout GET endpoint uses the SessionService to invalidate the access token.
     * 
     * @return HTTP.OK if successful. Otherwise Response with status indicating the root cause. 
     */
    @Secured({UserRoles.SPA_ACCESS})
    @GetMapping(value = "/logout")
    public ResponseEntity<Object> logout() {
        return ResponseEntity.status(sessionService.logout()).build();
    }

}
