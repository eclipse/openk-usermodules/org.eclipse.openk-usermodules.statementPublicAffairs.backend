/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.controller;

import java.util.Map;

import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.model.Position;
import org.eclipse.openk.statementpublicaffairs.service.GeoPositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller provides endpoints for geo position transform operations.
 * 
 * @author Tobias Stummer
 *
 */
@RestController
public class GeoPositionController {

	@Autowired
	private GeoPositionService geoPositionService;

	/**
	 * Transforms coordinates between geo coordinate formats.
	 * 
	 * @param from      source position format of the coordinates to transform
	 * @param to        target position format of the coordinates to transform
	 * @param positions map of positions
	 * @return map with the transformed positions.
	 */
	@Secured({ UserRoles.SPA_ACCESS })
	@PostMapping(value = "/geo-coordinate-transform")
	public Map<String, Position> transformCoordinates(@RequestParam final String from, @RequestParam final String to,
			@RequestBody Map<String, Position> positions) {
		return geoPositionService.transform(positions, from, to);
	}
}
