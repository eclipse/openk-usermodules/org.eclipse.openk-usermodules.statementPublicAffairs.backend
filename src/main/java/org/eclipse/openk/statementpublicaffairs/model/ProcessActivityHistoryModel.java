/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model;

import java.time.ZonedDateTime;

import lombok.Data;

/**
 * @author Tobias Stummer
 *
 */
@Data
public class ProcessActivityHistoryModel {

    
    private String id;
    private String activityId;
    private String activityName;
    private String activityType;
    private String assignee;
    
    
    private ZonedDateTime startTime;
    private ZonedDateTime endTime;
    private Long durationInMillis;
    private String processDefinitionKey;
    private String processDefinitionId;

    private Boolean canceled;
    private Boolean completeScope;
    private String tenantId;

}
