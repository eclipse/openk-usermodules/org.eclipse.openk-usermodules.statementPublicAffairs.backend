/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model;

import lombok.Data;

@Data
public class DepartmentKey {
	private String group;
	private String department;

	public static DepartmentKey of(String group, String department) {
		if (group == null || department == null) {
			return null;
		}
		DepartmentKey dk = new DepartmentKey();
		dk.setGroup(group);
		dk.setDepartment(department);
		return dk;
	}
}
