/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model;

import java.time.ZonedDateTime;
import java.util.Map;

import lombok.Data;

/**
 * @author Tobias Stummer
 *
 */
@Data
public class WorkflowTaskModel {

    private String taskId;
    private String taskDefinitionKey;
    private String assignee;
    private String authorized;
    private String tenantId;
    private Boolean suspended;
    private String processInstanceId;
    private String processDefinitionKey;
    private String name;
    private String owner;
    private String description;
    private ZonedDateTime created;
    private ZonedDateTime due;
    private String formKey;
    private Map<String,String> requiredVariables;
    
    public boolean isAssigned() {
    	return this.getAssignee() != null && !"".equals(this.getAssignee());
    }

}
