/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model.camunda;

import lombok.Data;

/**
 * @author Tobias Stummer
 *
 */
@Data
public class CamundaTaskInstance {
    private String id;
    private String name;
    private String assignee;
    private String owner;
    private String created;
    private String due;
    private String followUp;
    private String delegationState;
    private String description;
    private String executionId;
    private String parentTaskId;
    private Long priority;
    private String processDefinitionId;
    private String processInstanceId;
    private String caseExecutionId;
    private String caseInstanceId;
    private String taskDefinitionKey;
    private Boolean suspended;
    private String formKey;
    private String tenantId;

}
