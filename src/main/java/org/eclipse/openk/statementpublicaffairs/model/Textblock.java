/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model;

import java.util.Map;

import lombok.Data;

@Data
public class Textblock {
	private TextblockType type;
	private String textblockId;
	private Boolean addNewline;
	private Map<String, String> placeholderValues;
	private String replacement;
	
	public enum TextblockType {
		block,
		text,
		newline,
		pagebreak
	}
}
