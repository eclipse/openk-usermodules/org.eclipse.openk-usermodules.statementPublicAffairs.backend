/*
 *******************************************************************************
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model.contactdatabase;

import lombok.Data;

@Data
public class ContactApiDetailedContact {

    private Long fkContactId;
    private Long id;
    private String uuid;
    private String name;
    private String contactType;
    private String companyName;
    private String companyType;
    private String companyId;
    private String salutationUuid;
    private String personTypeUuid;
    private String firstName;
    private String lastName;
    private String department;
    private String note;
    private String salutationType;
    private String personType;
    private String mainAddress;
    private String email;
    private String searchfield;
    private Boolean anonymized;

}
