/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model;

import java.util.List;

import lombok.Data;

@Data
public class TextblockItem {

	private String id;
	
	private String text;

	private List<String>excludes;
	
	private List<TextblockRequirement>requires;

	public boolean valid() {
		if ( id == null || text == null || excludes == null || requires == null) {
			return false;
		}
		for (String s : excludes) {
			if (s == null) {
				return false;
			}
		}

		for (TextblockRequirement r : requires) {
			if (r == null || !r.valid()) {
				return false;
			}
		}
		return true;
	}

	
}
