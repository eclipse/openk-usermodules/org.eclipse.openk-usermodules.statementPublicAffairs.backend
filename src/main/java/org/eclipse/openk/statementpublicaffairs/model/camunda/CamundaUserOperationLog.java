/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model.camunda;

import lombok.Data;

/**
 * @author Tobias Stummer
 *
 */
@Data
public class CamundaUserOperationLog {

	    private String topicName;
	    private Long lockDuration;
	    private String id; 
	    private String deploymentId;
	    private String processDefinitionId;
	    private String processDefinitionKey;
	    private String processInstanceId;
	    private String executionId;
	    private String caseDefinitionId;
	    private String caseInstanceId;
	    private String caseExecutionId;
	    private String taskId;
	    private String jobId;
	    private String jobDefinitionId;
	    private String batchId;
	    private String userId;
	    private String timestamp;
	    private String operationId;
	    private String operationType;
	    private String entityType;
	    private String property;
	    private String orgValue;
	    private String newValue;
	    
}
