variables:
  FF_USE_FASTZIP: "true" # enable fastzip - a faster zip implementation that also supports level configuration.
  ARTIFACT_COMPRESSION_LEVEL: default
  CACHE_COMPRESSION_LEVEL: default
  TRANSFER_METER_FREQUENCY: 5s # will display transfer progress every 5 seconds for artifacts and remote caches.
  MAVEN_OPTS: "-Djava.awt.headless=true -Dmaven.repo.local=.m2/repository"
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version"
  SONAR_USER_HOME: ${CI_PROJECT_DIR}/.sonar  # Defines the location of the analysis task cache
  GIT_DEPTH: 0  # Tells git to fetch all the branches of the project, required by the analysis task
  FEATURE_BRANCH:
    value: "" # this would be the default value
    description: "This variable makes cakes delicious" # makes this variable appear on the Run Pipeline

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - .m2/repository/

image: maven:3.9-eclipse-temurin-21-alpine
#-----------------------------------------------------------------------------------------------------------------------
stages:
  - Build
  - Test
  - Sonarqube
  - Dockerimage
  - Deploy

#-----------------------------------------------------------------------------------------------------------------------
# Build
#-----------------------------------------------------------------------------------------------------------------------
stage-build:
  image: maven:3.9-eclipse-temurin-21-alpine
  stage: Build
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - .m2/repository/
      - target/
  script:
    - mvn clean install -DskipTests -Dp.type=war && mvn install -DskipTests -Dp.type=jar
  artifacts:
    paths:
      - target/*.jar

#-----------------------------------------------------------------------------------------------------------------------
# Test
#-----------------------------------------------------------------------------------------------------------------------
stage-test:
  stage: Test
  image: maven:3.9-eclipse-temurin-21-alpine
  script:
    - mvn test -Dskip.asciidoc=true
  artifacts:
    paths:
      - target/
      - src/
    reports:
      junit:
        - target/surefire-reports/TEST-*.xml

#-----------------------------------------------------------------------------------------------------------------------
# Sonarqube
#-----------------------------------------------------------------------------------------------------------------------
sonarqube-check:
  stage: Sonarqube
  image:
    name: sonarsource/sonar-scanner-cli:4.6
    entrypoint: [""]
  cache:
    key: ${CI_JOB_NAME}
    paths:
      - .sonar/cache
  script:
    - sonar-scanner -Dsonar.qualitygate.wait=true
  allow_failure: true
  dependencies:
    - stage-test

#-----------------------------------------------------------------------------------------------------------------------
# Dockerimage
#-----------------------------------------------------------------------------------------------------------------------
.docker-build-script:
  image: docker:20.10.7-git
  variables:
    WORKSPACE: ""
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - ls -l
    - |
      if [[ "$CI_COMMIT_TAG" == "" ]]; then
        tag="sha-${CI_COMMIT_SHORT_SHA}"
      else
        tag="$CI_COMMIT_TAG"
      fi
    - echo current tag ${tag}
    - CI_SOURCE_BRANCH=$(git for-each-ref | grep $CI_COMMIT_SHA | grep origin | sed "s/.*\///" | tr '[:upper:]' '[:lower:]')
    - echo CI_SOURCE_BRANCH $CI_SOURCE_BRANCH
    - REGISTRY_IMAGE_BASE="$CI_REGISTRY_IMAGE/$CI_SOURCE_BRANCH/$IMG_NAME"
    - FINAL_REGISTRY_IMAGE="$CI_REGISTRY_IMAGE/$CI_SOURCE_BRANCH/$IMG_NAME:${tag}"
    - echo "REGISTRY_IMAGE_BASE=$REGISTRY_IMAGE_BASE" >> dockerimage.env
    - echo "IMAGE_TAG=$tag" >> dockerimage.env
    - echo "FINAL_REGISTRY_IMAGE=$FINAL_REGISTRY_IMAGE" >> dockerimage.env
    - echo current FINAL_REGISTRY_IMAGE ${FINAL_REGISTRY_IMAGE}
    - echo $WORKSPACE
    - |
      if [[ "$WORKSPACE" != "" ]]; then
        cd $WORKSPACE
      fi
    - docker build --pull -f $DOCKER_FILE -t "$FINAL_REGISTRY_IMAGE" -t "$REGISTRY_IMAGE_BASE" --cache-from "$REGISTRY_IMAGE_BASE"  --build-arg BUILDKIT_INLINE_CACHE=1 .
    - docker push "$REGISTRY_IMAGE_BASE" --all-tags
  artifacts:
    reports:
      dotenv: dockerimage.env

#-----------------------------------------------------------------------------------------------------------------------
docker-build-db-init:
#-----------------------------------------------------------------------------------------------------------------------
  stage: Dockerimage
  extends: .docker-build-script
  variables:
    IMG_NAME: "spa-db-init"
    DOCKER_FILE: "Dockerfile"
    WORKSPACE: "./docker/postgresenv"
  needs: []
  only:
    changes:
      - docker/postgresev/**/*
      - docker/postgresev/*

#-----------------------------------------------------------------------------------------------------------------------
docker-build-camunda:
#-----------------------------------------------------------------------------------------------------------------------
  stage: Dockerimage
  extends: .docker-build-script
  variables:
    IMG_NAME: "spa-camunda"
    DOCKER_FILE: "./docker/camundaenv/Dockerfile"
  needs: []
  only:
    changes:
      - docker/camundaenv/**/*
      - docker/camundaenv/*

#-----------------------------------------------------------------------------------------------------------------------
docker-build-main:
#-----------------------------------------------------------------------------------------------------------------------
  stage: Dockerimage
  extends: .docker-build-script
  variables:
    IMG_NAME: "spa-main"
    DOCKER_FILE: "./docker/runenv/Dockerfile"
  needs:
    - job: stage-build
    - job: stage-test    

#-----------------------------------------------------------------------------------------------------------------------
# Deploy
#-----------------------------------------------------------------------------------------------------------------------
.deploy-script:
  image: alpine:3.14.0
  cache: {}
  variables:
    GIT_STRATEGY: none
    DEPLOYMENT_FILE: fileDefaultVarPlaceholder
    YAML_IMAGE_NAME: image
  before_script:
    - apk add --no-cache git curl bash coreutils
    - wget https://github.com/mikefarah/yq/releases/download/v4.17.2/yq_linux_amd64 -O /usr/bin/yq && chmod +x /usr/bin/yq
    - yq -V
    - ls -l
    - git clone https://${CI_USERNAME}:${CI_PUSH_TOKEN}@gitlab.com/${GITLAB_DEPLOYMENT_REPO_URL}
    - cd *
    - git config --global user.email "gitlab@gitlab.com"
    - git config --global user.name "GitLab CI/CD"
  script:
    - ls -l
    - cat ${DEPLOYMENT_FILE}
    - echo FINAL_REGISTRY_IMAGE ${FINAL_REGISTRY_IMAGE}
    - echo APP_NAME ${APP_NAME}
    - echo IMAGE_TAG ${IMAGE_TAG}
    - echo REGISTRY_IMAGE_BASE ${REGISTRY_IMAGE_BASE}
    - yq e -i '.apps.[env(YAML_APP_NAME)][env(YAML_IMAGE_NAME)].tag = env(IMAGE_TAG)' ${DEPLOYMENT_FILE}
    - yq e -i '.apps.[env(YAML_APP_NAME)][env(YAML_IMAGE_NAME)].repository = env(REGISTRY_IMAGE_BASE)' ${DEPLOYMENT_FILE}
    - cat ${DEPLOYMENT_FILE}
    - git commit -am '[skip ci] Image update'
    - git push origin main


#------------------------------
# Deploy - QA-Environment
#------------------------------
deploy-qa-main:
  stage: Deploy
  extends: .deploy-script
  variables:
    YAML_APP_NAME: spa-be
    DEPLOYMENT_FILE: deployment/applications/values-spa-qa.yaml
  dependencies:
    - "docker-build-main"
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_COMMIT_TAG

deploy-qa-camunda:
  stage: Deploy
  extends: .deploy-script
  variables:
    YAML_APP_NAME: camunda
    DEPLOYMENT_FILE: deployment/applications/values-spa-qa.yaml
  dependencies:
    - "docker-build-camunda"
  rules:
    - if: $CI_COMMIT_BRANCH == "master" || $CI_COMMIT_TAG
      changes:
        - docker/camundaenv/**/*
        - docker/camundaenv/*

deploy-qa-db-init:
  stage: Deploy
  extends: .deploy-script
  variables:
    YAML_APP_NAME: camunda    
    YAML_IMAGE_NAME: imageDatabase    
    DEPLOYMENT_FILE: deployment/applications/values-spa-qa.yaml
  dependencies:
    - "docker-build-db-init"
  rules:
    - if: $CI_COMMIT_BRANCH == "master" || $CI_COMMIT_TAG
      changes:
        - docker/postgresenv/**/*
        - docker/postgresenv/*        

#------------------------------
# Deploy - DEV-Environment
#------------------------------
deploy-dev-main:
  stage: Deploy
  extends: .deploy-script
  variables:
    YAML_APP_NAME: spa-be
    DEPLOYMENT_FILE: deployment/applications/values-spa-dev.yaml
  dependencies:
    - "docker-build-main"
  rules:
    - if: $CI_COMMIT_BRANCH == "develop"

deploy-dev-camunda:
  stage: Deploy
  extends: .deploy-script
  variables:
    YAML_APP_NAME: camunda
    DEPLOYMENT_FILE: deployment/applications/values-spa-dev.yaml
  dependencies:
    - "docker-build-camunda"
  rules:
    - if: $CI_COMMIT_BRANCH == "develop"
      changes:
        - docker/camundaenv/**/*
        - docker/camundaenv/*

deploy-dev-db-init:
  stage: Deploy
  extends: .deploy-script
  variables:
    YAML_APP_NAME: camunda    
    YAML_IMAGE_NAME: imageDatabase    
    DEPLOYMENT_FILE: deployment/applications/values-spa-dev.yaml
  dependencies:
    - "docker-build-db-init"
  rules:
    - if: $CI_COMMIT_BRANCH == "develop" || $CI_COMMIT_TAG
      changes:
        - docker/postgresenv/**/*
        - docker/postgresenv/*         


