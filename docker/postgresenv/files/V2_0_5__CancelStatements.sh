#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "spa_service" --dbname "spa_service" <<-EOSQL

-----------------------------------------------------------------------------------
-- *******************************************************************************
-- * Copyright (c) 2022 Contributors to the Eclipse Foundation
-- *
-- * See the NOTICE file(s) distributed with this work for additional
-- * information regarding copyright ownership.
-- *
-- * This program and the accompanying materials are made available under the
-- * terms of the Eclipse Public License v. 2.0 which is available at
-- * http://www.eclipse.org/legal/epl-2.0.
-- *
-- * SPDX-License-Identifier: EPL-2.0
-- *******************************************************************************
-----------------------------------------------------------------------------------

-- add canceled indicator to tbl_statement

ALTER TABLE public.tbl_statement ADD COLUMN canceled Boolean NOT NULL DEFAULT FALSE;


-- VW_STATEMENT_SEARCH

CREATE OR REPLACE VIEW vw_statement_search AS
 SELECT s.id,
    s.business_key,
    s.city,
    s.district,
    s.due_date,
    s.finished,
    s.receipt_date,
    s.creation_date,
    s.customer_reference,
    s.title,
    s.type_id,
    s.contact_db_id,
    s.source_mail_id,
    upper(s.id || '|@|' || COALESCE(t.name, '') ||  '|@|' || COALESCE (s.title, '') || '|@|' || COALESCE (s.city, '')  || '|@|' || COALESCE (s.district, '') || '|@|' || COALESCE (s.customer_reference, '') || '|@|' ) AS searchfield,
    s.departments_due_date,
    s.canceled
    FROM tbl_statement s,
    tbl_statementtype t
  WHERE (s.type_id = t.id);


-- VW_USER_STATEMENT_SEARCH

CREATE OR REPLACE VIEW vw_user_statement_search AS
 SELECT DISTINCT s.id,
    s.business_key,
    s.city,
    s.district,
    s.due_date,
    s.finished,
    s.receipt_date,
    s.creation_date,
    s.customer_reference,
    s.title,
    s.type_id,
    s.contact_db_id,
    e.user_id,
    s.source_mail_id,
    upper(s.id || '|@|' || COALESCE(t.name, '') ||  '|@|' || COALESCE (s.title, '') || '|@|' || COALESCE (s.city, '')  || '|@|' || COALESCE (s.district, '') || '|@|' || COALESCE (s.customer_reference, '') || '|@|' ) AS searchfield,
  	s.departments_due_date,
  	s.canceled
    FROM tbl_statement s,
    tbl_statementtype t,
    tbl_statement_edit_log e
  WHERE ((s.type_id = t.id) AND (e.statement_id = s.id));
  
 -- VW_STATEMENT_POSITION_SEARCH

CREATE OR REPLACE VIEW vw_statement_position_search AS
 SELECT s.id,
    s.due_date,
    s.finished,
    s.title,
    s.type_id,
    w.pos,
    s.canceled
   FROM tbl_statement s,
    tbl_workflowdata w
  WHERE (w.statement_id = s.id);


EOSQL

