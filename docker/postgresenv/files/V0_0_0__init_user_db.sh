#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
	CREATE USER spa_service;
	CREATE DATABASE spa_service;
    ALTER USER spa_service with encrypted password 'spa_service';
	GRANT ALL PRIVILEGES ON DATABASE spa_service TO spa_service;
EOSQL
